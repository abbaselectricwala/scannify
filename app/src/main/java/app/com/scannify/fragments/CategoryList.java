package app.com.scannify.fragments;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.melnykov.fab.FloatingActionButton;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import app.com.scannify.R;
import app.com.scannify.Utils.JSONParser;
import app.com.scannify.Utils.Urls;
import app.com.scannify.adapter.CategoryListAdapter;
import app.com.scannify.view.ProgressDialog;

public class CategoryList extends Fragment {


    public CategoryList() {
    }

    SharedPreferences                   prefApp, pref;
    SharedPreferences.Editor            edit;
    private String[]                    categoryTitle;
    private String[]                    imagePath;
    private int[]                       categoryId;

    private TextView                    emptyView;
    JSONObject                          parent;
    CircularProgressView                progressView;
    Thread                              updateThread;
    private RecyclerView                recyclerView;
    private RecyclerView.LayoutManager  layoutManager;
    CategoryListAdapter                 adapterCategoryList;
    Bitmap                              bitmap;
    ActionBar actionBar;
    JSONParser                          jsonParser;
    JSONObject                          jsonBr;
    ImageButton                         imgGPS;
    JSONObject                          Position;
    ImageView                           imageError;
    static int                          back = 0;
    ProgressDialog                      pDialog = null;


     public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      //   ScannifyApplication.getInstance().trackScreenView("CategoryList");
        getActivity().setTitle("Category List");
        pref = getActivity().getSharedPreferences("Category", Context.MODE_PRIVATE);
        prefApp = getActivity().getSharedPreferences("AppPref", Context.MODE_PRIVATE);
        jsonParser = new JSONParser(getActivity());


        View rootView = inflater.inflate(R.layout.fragment_category_list, container, false);
        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && KeyEvent.ACTION_DOWN == event.getAction()) {
                    CategoryList fragment = new CategoryList();
                    if (fragment.isVisible()) {
                    } else {
                        if (back == 0) {
                            HomeFragment.count = 1;
                            HomeFragment fragmenthome = new HomeFragment();
                            FragmentManager fragmentManager = getFragmentManager();
                            fragmentManager.beginTransaction().replace(R.id.frame_container, fragmenthome).commit();

                        } else {
                            back = 0;
                        }
                    }
                    return true;
                }
                return false;
            }
        });
        emptyView     = (TextView)              rootView.findViewById(R.id.empty_view);
        imageError    = (ImageView)             rootView.findViewById(R.id.imageError);
        progressView  = (CircularProgressView)  rootView.findViewById(R.id.progress_view);
        recyclerView  = (RecyclerView)          rootView.findViewById(R.id.listBrowseBroadcast);

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        actionBar = getActivity().getActionBar();
        actionBar.removeAllTabs();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);



        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                FragmentCreateCategory createcategory = new FragmentCreateCategory();
                Bundle bundle = new Bundle();
                String finalPageView = "CategoryList";
                bundle.putString("finalPageView", finalPageView);
                createcategory.setArguments(bundle);
                fragmentTransaction.replace(R.id.frame_container, createcategory);
                fragmentTransaction.addToBackStack("myFrag");
                fragmentTransaction.commit();
            }
        });


        final GestureDetector mGestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

        });

        /*recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());

                if (child != null && mGestureDetector.onTouchEvent(motionEvent)) {
                    int pos = recyclerView.getChildPosition(child);
                    int k = 0;
                    try {
                        for (int i = 0; i < Position.length(); i++) {
                            int n = i + 1;
                            JSONObject categoryArray = Position.getJSONObject("Broadcast_" + n);
                            if (k == pos) {
                                edit.putInt("BroadcastPosition", pos + 1);
                                edit.putString("jsonObject", categoryArray.toString());
                                edit.putString("allJsonObjects", Position.toString());
                            }
                            k++;
                        }
                    } catch (JSONException x) {
                        x.printStackTrace();
                    }
                    // edit.commit();
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    ShowContacts showContacts = new ShowContacts();
                    Bundle args = new Bundle();
                    String finalbroadcasts = "CategoryShow";
                    args.putString("finalbroadcast", finalbroadcasts);
                    showContacts.setArguments(args);
                    fragmentTransaction.replace(R.id.frame_container, showContacts);
                    fragmentTransaction.addToBackStack("myFrag");
                    fragmentTransaction.commit();
                    return true;
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
            }
        });*/

        new LoadCategories().execute();
        return rootView;
    }

    class LoadCategories extends AsyncTask<Integer, String, String> {
        boolean failure = false;
        int success = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (null != getActivity()) {
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
            pDialog = new ProgressDialog(getActivity(), getResources().getString(R.string.prog_msg_loading));
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Integer... args) {
            // TODO Auto-generated method stub
//            int cat = args[0];
            String username = prefApp.getString("Username", "noUser");

            List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
            paramsList.add(new BasicNameValuePair("username", username));

            jsonBr = jsonParser.makeHttpRequest(Urls.GetCategories, "POST", paramsList);

            if (null != jsonBr) {
                try {
                       success = jsonBr.getInt("success");
                    if (1 == success){

                        JSONObject categories = jsonBr.getJSONObject("MyCategories");
                        int brNo = categories.length();
                        categoryTitle = new String[brNo];

                        categoryId = new int[brNo];
                        imagePath = new String[brNo];
                        int loop = 0;

                        for (int i = 0; i < brNo; ++i) {
                            int n = i + 1;
                            JSONObject categoryArray = categories.getJSONObject("c_" + n);

                            categoryTitle[loop] = categoryArray.getString("title");
                            categoryId[loop]    = categoryArray.getInt("category_id");
                            imagePath[loop]     = categoryArray.getString("image_path");

                            loop++;
                        }
                        String Title[] = new String[loop];

                        for (int i = 0; i < loop; ++i)
                            Title[i] = categoryTitle[i];


                        adapterCategoryList = new CategoryListAdapter(getActivity(), categoryTitle, imagePath, categoryId);
                        return "success";
                    }
                } catch (JSONException x) {
                    x.printStackTrace();
                }
            } else {
                return "false";
            }
            return "false";
        }

        protected void onPostExecute(String message) {


             if( 1== success){

                 emptyView.setVisibility(View.GONE);
                 imageError.setVisibility(View.GONE);
                 recyclerView.setAdapter(adapterCategoryList);
                 recyclerView.setVisibility(View.VISIBLE);
                 progressView.setVisibility(View.GONE);
             }
            else{
                 emptyView.setVisibility(View.VISIBLE);
                 imageError.setVisibility(View.VISIBLE);
                 recyclerView.setVisibility(View.GONE);
                 progressView.setVisibility(View.GONE);

             }

                pDialog.dismiss();
                if (null != getActivity()) {
                    getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }
            }

    }
}

