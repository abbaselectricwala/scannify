package app.com.scannify.fragments;


import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import app.com.scannify.R;
import app.com.scannify.Utils.JSONParser;
import app.com.scannify.Utils.Urls;
import app.com.scannify.adapter.SpinnerAdapter;
import app.com.scannify.view.ButtonRectangle;
import app.com.scannify.view.ProgressDialog;
import me.drakeet.materialdialog.MaterialDialog;

import com.scanlibrary.ScanActivity;
import com.squareup.picasso.Picasso;

public class FragmentEditCard extends Fragment {

    EditText                    edname, edemail,edphone, edwebsite, edaddress, edcompany, eded1, eded2, edDesignation,edNotes;
    RelativeLayout              main;
    public ButtonRectangle      save;
    RelativeLayout              rel1, rel2;
    TextView                    tv1, tv2;
    ActionBar actionBar;
    JSONObject                  jsonCr;
    JSONParser                  jsonParser;
    SharedPreferences           detail, appPref, repeat_sp;
    SharedPreferences.Editor    appEdit, editDetail, edit_repeat_sp;
    static int                  back = 0;
    MaterialDialog              mMaterialDialog;
    String                      spnSelectedItem;
    ImageView                   imgviewfront, imgviewback;
    Bitmap                      bitmap;
    ProgressDialog              pDialog = null;
    Uri                         imagepathfront, imagepathback;
    private Spinner             spnCat;
    SpinnerAdapter              spnAdapter;
    int                         id;
    String                      categories[];
    String                      arr_images[];
    int                         category_id[];
    Bitmap                      imagePath[];
    ButtonRectangle             btnAdd;
    boolean                     isClicked = false;
    //Validate                    validate;

    public FragmentEditCard() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       // ScannifyApplication.getInstance().trackScreenView("FragmentEditCard");
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_edit_card, container, false);
        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();

        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && KeyEvent.ACTION_DOWN == event.getAction()) {
                    FragmentContactDetail fragment = new FragmentContactDetail();
                    if (fragment.isVisible()) {
                    } else {
                        if (back == 0) {
                            // HomeFragment.count              = 1;
                            HomeFragment fragmenthome = new HomeFragment();
                            FragmentManager fragmentManager = getFragmentManager();
                            fragmentManager.beginTransaction().replace(R.id.frame_container, fragmenthome).commit();

                        } else {
                            back = 0;
                        }
                    }
                    return true;
                }
                return false;
            }
        });

        actionBar = getActivity().getActionBar();
        actionBar.removeAllTabs();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

        /****************************************************************************************/

        repeat_sp = getActivity().getSharedPreferences("Repeat_Scan", 0);
        edit_repeat_sp = repeat_sp.edit();

        appPref = getActivity().getSharedPreferences("AppPref", Context.MODE_PRIVATE);
        appEdit = appPref.edit();

        jsonParser = new JSONParser(getActivity());

        detail = getActivity().getSharedPreferences("Details", 0);
        editDetail = detail.edit();

        String data = getActivity().getIntent().getStringExtra("data");
        System.out.print(data);

        edname      = (EditText)        rootView.findViewById(R.id.edName);
        edphone     = (EditText)        rootView.findViewById(R.id.edPhone);
        edemail     = (EditText)        rootView.findViewById(R.id.edEmail);
        edwebsite   = (EditText)        rootView.findViewById(R.id.edWebsite);
        edaddress   = (EditText)        rootView.findViewById(R.id.edAddress);
        edNotes     = (EditText)        rootView.findViewById(R.id.edNotes);
        main        = (RelativeLayout)  rootView.findViewById(R.id.main);
        edDesignation     = (EditText)        rootView.findViewById(R.id.edDesignatoin);
        edcompany   = (EditText)        rootView.findViewById(R.id.edCmpName);
        save        = (ButtonRectangle) rootView.findViewById(R.id.btsave);
        rel1        = (RelativeLayout)  rootView.findViewById(R.id.rlTitle1);
        tv1         = (TextView)        rootView.findViewById(R.id.tv1);
        eded1       = (EditText)        rootView.findViewById(R.id.ed1);
        rel2        = (RelativeLayout)  rootView.findViewById(R.id.rlTitle2);
        tv2         = (TextView)        rootView.findViewById(R.id.tv2);
        eded2       = (EditText)        rootView.findViewById(R.id.ed2);
        imgviewfront = (ImageView)      rootView.findViewById(R.id.imgviewfront);
        imgviewback = (ImageView)       rootView.findViewById(R.id.imgviewback);
        spnCat      = (Spinner)         rootView.findViewById(R.id.spnCategory);
        btnAdd      = (ButtonRectangle) rootView.findViewById(R.id.btAdd);

        try {
            String str = appPref.getString("Categories","Empty");
            if( !str.equalsIgnoreCase("Empty") ){
                JSONObject categoryObj = new JSONObject(str);
                categories = new String[categoryObj.length()];
                arr_images = new String[categoryObj.length()];
                category_id = new int[categoryObj.length()];
                JSONObject newObj;
                for( int cnt = 0;cnt< categoryObj.length(); ++cnt){
                    newObj = categoryObj.getJSONObject("c_"+(cnt+1));

                    categories[cnt]  = newObj.getString("title");
                    arr_images[cnt]  = newObj.getString("image_path");
                    category_id[cnt] = newObj.getInt("category_id");

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(0 < categories.length){
            spnAdapter = new SpinnerAdapter(getActivity(), R.layout.row, categories,arr_images);
            spnCat.setAdapter(spnAdapter);
        }
        else{
            spnCat.setVisibility(View.GONE);
        }

        spnCat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spnSelectedItem = spnAdapter.getSelectedCategory(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        new LoadCard().execute();
        save.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        showConfirmDialog();
                                    }
                                }
        );

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isClicked == false) {
                    isClicked = true;
                    FragmentContactDetail.scan_count++;
                    Intent i = new Intent(getActivity(), ScanActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
            }
        });

        return rootView;

    }

    public void showConfirmDialog() {
        mMaterialDialog = new MaterialDialog(getActivity())
                .setMessage(getResources().getString(R.string.msg_check))
                .setPositiveButton(getResources().getString(R.string.bt_confirm), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();


                        new EditCard().execute();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.bt_check), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                    }
                });
        mMaterialDialog.show();
    }


    class LoadCard extends AsyncTask<String, String, String> {

        boolean failure = false;
        String  name, email, website, phone, designation, company_name, company_addr,notes;
        int     isBookmarked = 0;
        String  path[];

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        }

        @Override
        protected String doInBackground(String... args) {
            // TODO Auto-generated method stub
            // here Check for success tag
            try {
                String cardObj = appPref.getString("selectedCardObject", "Empty");
                JSONObject jsonCardObj = new JSONObject(cardObj);

                if (null != jsonCardObj) {

                    id              = jsonCardObj.getInt("id");
                    name            = jsonCardObj.getString("contact_name");
                    email           = jsonCardObj.getString("contact_email");
                    isBookmarked    = jsonCardObj.getInt("isBookmarked");
                    website         = jsonCardObj.getString("website");
                    phone           = jsonCardObj.getString("contact_phone");
                    designation     = jsonCardObj.getString("contact_designation");
                    company_name    = jsonCardObj.getString("company_name");
                    company_addr    = jsonCardObj.getString("company_address");
                    notes           = jsonCardObj.getString("notes");


                    if (!jsonCardObj.getJSONArray("image").equals(null)) {

                        JSONArray imageArray = jsonCardObj.getJSONArray("image");
                        int arrayLength = imageArray.length();
                        path = new String[arrayLength];
                        imagePath = new Bitmap[arrayLength];
                        for (int j = 0; j < arrayLength; ++j) {
                            path[j] = imageArray.get(j).toString();
                        }
                     }
                    return "success";
                } else
                    return "failure";

            } catch (JSONException x) {
                x.printStackTrace();
            }
            return "failure";
        }

        protected void onPostExecute(String message) {
            if (message.equalsIgnoreCase("success")) {

                if (path.length == 2) {
                    btnAdd.setEnabled(false);
                    btnAdd.setBackgroundColor(getResources().getColor(R.color.disabled_button_color));
                } else {
                    btnAdd.setEnabled(true);
                    btnAdd.setBackgroundColor(getResources().getColor(R.color.dark_blue));
                }
                edname.setText(name);
                edphone.setText(phone);
                edemail.setText(email);
                edwebsite.setText(website);
                edaddress.setText(company_addr);
                edDesignation.setText(designation);
                edNotes.setText(notes);

                editDetail.putString("name", name);
                editDetail.putString("email", email);
                editDetail.putString("address", company_addr);
                editDetail.putString("website", website);
                editDetail.putString("company", designation);
                editDetail.putString("phone", phone);
                editDetail.putString("notes", notes);

                editDetail.apply();

                if (1 == imagePath.length){
                    Picasso.with(getActivity()).load(path[0]).into(imgviewfront);
                    appEdit.putString("imagepathfrontURI", path[0]);
                    appEdit.putInt("isEdited",1);
            }
                else if(2 == imagePath.length) {
                        Picasso.with(getActivity()).load(path[0]).into(imgviewfront);
                        Picasso.with(getActivity()).load(path[1]).into(imgviewback);
                    appEdit.putString("imagepathfrontURI",  path[0]);
                    appEdit.putString("imagepathbackURI",  path[1]);
                }
                appEdit.putInt("cr_id",id);
                appEdit.apply();
            }
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    class EditCard extends AsyncTask<String, String, String> {
        boolean failure = false;
        int serverResponseCode = 0;
        int[] imgSave =new int[2];

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(null != getActivity()) {
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
            pDialog = new ProgressDialog(getActivity(), getResources().getString(R.string.prog_msg_Editing));
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            // TODO Auto-generated method stub


           /* if(null != imagepathfront)
                imgSave[0] = uploadFile(imagepathfront.toString());

            if(null != imagepathback)
                imgSave[1] = uploadFile(imagepathback.toString());*/

            String username  = appPref.getString("Username", "noUser");
            List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
            paramsList.add(new BasicNameValuePair("username",      username));
            paramsList.add(new BasicNameValuePair("cr_id",                String.valueOf(id)));
            paramsList.add(new BasicNameValuePair("category",             spnSelectedItem));
            paramsList.add(new BasicNameValuePair("contact_name",         edname.getText().toString()));
            paramsList.add(new BasicNameValuePair("contact_email",        edemail.getText().toString()));
            paramsList.add(new BasicNameValuePair("contact_phone",        edphone.getText().toString()));
            paramsList.add(new BasicNameValuePair("contact_designation",  edDesignation.getText().toString()));
            paramsList.add(new BasicNameValuePair("company_name",         edcompany.getText().toString()));
            paramsList.add(new BasicNameValuePair("company_address",      edaddress.getText().toString()));
            paramsList.add(new BasicNameValuePair("website",              edwebsite.getText().toString()));
            paramsList.add(new BasicNameValuePair("notes",               edNotes.getText().toString()));
            paramsList.add(new BasicNameValuePair("isEdited",             String.valueOf(1)));
            paramsList.add(new BasicNameValuePair("cardTag",              "E"));


            JSONArray hList = new JSONArray();

            /*if (null != imagepathfront) {

                if (1 == imgSave[0]) {
                    hList.put(imagepathfront);
                }
                if (1 == imgSave[1]) {
                    hList.put(imagepathback);
                }
            }

            if (0 != hList.length())
                paramsList.add(new BasicNameValuePair("imageUri", hList.toString()));
*/

            jsonCr = jsonParser.makeHttpRequest(Urls.EditCard , "POST", paramsList);

            if(null != jsonCr) {
                try {
                    if (jsonCr.getString("success").equalsIgnoreCase("1"))
                        return "success";

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else
                return "failure";

            return "failure";
        }
        protected void onPostExecute(String message) {
            if (message.equalsIgnoreCase("success")) {
                try {
                    if (null != jsonCr.getJSONObject("updatedCard").toString()) {
                        String cardObj = jsonCr.getJSONObject("updatedCard").toString();
                        appEdit.putString("selectedCardObject", cardObj);

                        Fragment fragment = new Fragment_Detail();
                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();
                    }
                    else{
                        Fragment fragment = new HomeFragment();
                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else {
                Toast.makeText(getActivity(),"Failed",Toast.LENGTH_LONG).show();
            }
            pDialog.dismiss();
            if (null != getActivity()) {
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        }

        public Bitmap scaleDown(Bitmap realImage,
                                boolean filter) {
            float actualHeight       = realImage.getHeight();
            float actualWidth        = realImage.getWidth();
            float maxHeight          = (float)600.00;
            float maxWidth           = (float)800.0;
            float imgRatio           = actualWidth/actualHeight;
            float maxRatio           = maxWidth/maxHeight;

            if (actualHeight > maxHeight || actualWidth > maxWidth){
                if(imgRatio < maxRatio){
                    //adjust width according to maxHeight
                    imgRatio     = maxHeight / actualHeight;
                    actualWidth  = imgRatio * actualWidth;
                    actualHeight = maxHeight;
                }
                else if(imgRatio > maxRatio){
                    //adjust height according to maxWidth
                    imgRatio     = maxWidth / actualWidth;
                    actualHeight = imgRatio * actualHeight;
                    actualWidth  = maxWidth;
                }
                else{
                    actualHeight = maxHeight;
                    actualWidth  = maxWidth;
                }
            }
            int width  = Math.round(actualWidth);
            int height = Math.round(actualHeight);
            Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                    height, filter);
            return newBitmap;
        }

        public int uploadFile(String sourceFileUri) {
            String fileName        = sourceFileUri;
            String upLoadServerUri = Urls.UploadCardImage;
            HttpURLConnection conn;
            DataOutputStream dos;
            String lineEnd         = "\r\n";
            String twoHyphens      = "--";
            String boundary        = "*****";
            File sourceFile        = new File(sourceFileUri);
            int  success = 0;

            try {

                URL url = new URL(upLoadServerUri);
                conn    = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setUseCaches(false);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("uploaded_file", fileName);

                dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\"" + fileName + "\"" + lineEnd);
                //-=----------
                dos.writeBytes(lineEnd);

                //BitmapFactory.Options options = new BitmapFactory.Options ();
                //options.inSampleSize = 4;

                String path = sourceFile.getAbsolutePath();
                Bitmap myBitmap;
                // BitmapFactory.Options options = new BitmapFactory.Options();
                //options.inPreferredConfig = Bitmap.Config.ARGB_8888;

                if(!sourceFileUri.contains("content://")) {
                    Uri uri = Uri.fromFile(new File(sourceFileUri.toString()));
                    myBitmap = BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(uri));
                }
                else
                    myBitmap = BitmapFactory.decodeFile(path);

                //String p =  new File(getFilesDir(), imgFirst).getAbsolutePath();
                // Bitmap myBitmap           = BitmapFactory.decodeFile(path);
                Bitmap scaleBmp           = scaleDown(myBitmap, true);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                scaleBmp.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                byte[] data               = bos.toByteArray();

                dos.write(data, 0, data.length);

                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                serverResponseCode           = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();

                // Read response Json from urlConnection
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line = null;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line + '\n');
                }

                if(null != stringBuilder) {
                    JSONObject json = new JSONObject(stringBuilder.toString());
                    success = json.getInt("success");
                }

                Log.i("uploadFile", "HTTP Response is : " + serverResponseMessage + ": " + serverResponseCode);

                myBitmap.recycle();
                scaleBmp.recycle();
                bos.flush();
                bos.close();
                dos.flush();
                dos.close();
            } catch (MalformedURLException ex) {
                ex.printStackTrace();
                Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return success;
        }



    }



}
