package app.com.scannify.fragments;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import app.com.scannify.R;
import app.com.scannify.Utils.JSONParser;
import app.com.scannify.Utils.Urls;
import app.com.scannify.activity.LoginActivity;
//import app.com.officevikings.activity.MapsActivity;
import app.com.scannify.view.ProgressDialog;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import me.drakeet.materialdialog.MaterialDialog;

public class FragmentSettings extends Fragment {

    public FragmentSettings() {
    }

    ScrollView rlScroll;
    RelativeLayout rlDelete, rlChangePass, rlLogout;//rlFeedback
    ImageView ivPassChange, ivDelete, ivLogut;//ivFeedback
    View view;
    TextView txtChangePass;
    SharedPreferences prefApp;
    SharedPreferences.Editor editApp;
    int success, count = 0;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    ActionBar actionBar;
    MaterialDialog mMaterialDialog;
    private ProgressDialog pDialog;
    JSONParser jsonParser;
    JSONObject jsondelete;
    static int back = 0;
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && KeyEvent.ACTION_DOWN == event.getAction()) {
                    FragmentSettings fragment = new FragmentSettings();
                    if (fragment.isVisible()) {
                    } else {
                        if (back == 0) {
                            // HomeFragment.count = 1;
                            HomeFragment fragmenthome = new HomeFragment();
                            FragmentManager fragmentManager = getFragmentManager();
                            fragmentManager.beginTransaction().replace(R.id.frame_container, fragmenthome).commit();

                        } else {
                            back = 0;
                        }
                    }
                    return true;
                }
                return false;
            }
        });


        prefApp = getActivity().getSharedPreferences("AppPref", Context.MODE_PRIVATE);
        editApp = prefApp.edit();

        pref = getActivity().getSharedPreferences("SettingsPref", Context.MODE_PRIVATE);
        edit = pref.edit();

        jsonParser = new JSONParser(getActivity());

        actionBar = getActivity().getActionBar();
        actionBar.removeAllTabs();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

        rlScroll = (ScrollView) rootView.findViewById(R.id.rlScroll);
        rlDelete = (RelativeLayout) rootView.findViewById(R.id.relativeDelete);
        rlChangePass = (RelativeLayout) rootView.findViewById(R.id.relativeChangePass);
        rlLogout = (RelativeLayout) rootView.findViewById(R.id.relativeLogout);
        ivPassChange = (ImageView) rootView.findViewById(R.id.imArrow4);
        ivDelete = (ImageView) rootView.findViewById(R.id.imArrow3);
        ivLogut = (ImageView) rootView.findViewById(R.id.imArrow5);
        txtChangePass = (TextView) rootView.findViewById(R.id.tvPasswordChange);
        view = (View) rootView.findViewById(R.id.view8);

        rlChangePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                ChangePassword pass = new ChangePassword();
                fragmentTransaction.addToBackStack("myFrag");
                fragmentTransaction.replace(R.id.frame_container, pass);

                fragmentTransaction.commit();
            }
        });

        rlLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Logout log = new Logout();
                fragmentTransaction.replace(R.id.frame_container, log);
                fragmentTransaction.commit();
            }
        });

        rlDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMaterialDialog = new MaterialDialog(getActivity())
                        .setTitle(getResources().getString(R.string.DeleteAccount))
                        .setMessage(getResources().getString(R.string.DeleteAccountMessage))
                        .setPositiveButton(getResources().getString(R.string.prog_msg_OK), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                new AttempDeleteAccount().execute();
                                mMaterialDialog.dismiss();
                            }
                        })
                        .setNegativeButton(getResources().getString(R.string.prog_msg_CANCEL), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mMaterialDialog.dismiss();
                            }
                        });
                mMaterialDialog.show();
            }
        });

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

   class AttempDeleteAccount extends AsyncTask<String, String, String> {
        boolean failure = false;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(null != getActivity()) {
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
            pDialog = new ProgressDialog(getActivity(),getResources().getString(R.string.prog_msg_delete_account));
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {

            String username = prefApp.getString("Username", "UnknownUser");
            try {

                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("username", username));

                Log.d("request!", "updating");

                jsondelete = jsonParser.makeHttpRequest(Urls.DeleteAccount, "POST", params);

                if( null != jsondelete) {

                    // success tag for json
                    success = jsondelete.getInt(TAG_SUCCESS);

                    if (success == 1)
                        return "success";
                    else
                        return "failed";
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return "failed";
        }

        protected void onPostExecute(String message) {
            if(message.equalsIgnoreCase("success")){
                //goto Login Page
                edit.clear();
                editApp.clear();
                edit.commit();
                editApp.commit();
                Intent ii = new Intent(getActivity(), LoginActivity.class);
                // set the new task and clear flags
                ii.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(ii);
                Toast.makeText(getActivity(),"Account deleted successfully...",Toast.LENGTH_SHORT).show();
            }
            pDialog.dismiss();
            if (null != getActivity()) {
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        }
    }
}