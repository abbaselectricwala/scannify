package app.com.scannify.fragments;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.orhanobut.dialogplus.DialogPlus;

import com.orhanobut.dialogplus.ListHolder;
import com.orhanobut.dialogplus.OnClickListener;
import com.orhanobut.dialogplus.Holder;
import com.orhanobut.dialogplus.OnCancelListener;
import com.orhanobut.dialogplus.OnDismissListener;
import com.orhanobut.dialogplus.OnItemClickListener;

import com.viewpagerindicator.CirclePageIndicator;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import app.com.scannify.R;
import app.com.scannify.Utils.JSONParser;
import app.com.scannify.Utils.Urls;
import app.com.scannify.Utils.Utils;
import app.com.scannify.adapter.SimpleMenuAdapter;
import app.com.scannify.view.ButtonRectangle;
import app.com.scannify.view.ProgressDialog;
import me.drakeet.materialdialog.MaterialDialog;

public class Fragment_Detail extends Fragment {


    ActionBar actionBar;
    SharedPreferences           appPref;
    SharedPreferences.Editor    edit;
    TextView                    txtNameInfo, txtPhoneInfo, txtEmailInfo, txtDesgInfo, txtAddrInfo, txtWebsiteInfo, imgTextName,txtCompanyInfo, txtNotesInfo;
    ButtonRectangle             btnContact;
    ImageView                   fside, bside;
    ViewPager                   viewPager;
    ImagePagerAdapter           adapter = null;
    Bitmap                      imagePath[];
    Bitmap                      bm;
    ImageView                   imageOverlap;
    public  int                 actionbar_height,flag,id = 0;
    String                      phoneNo,backPageString;
    CirclePageIndicator         circleIndicator;
    RelativeLayout              rDetail;
    MaterialDialog              mMaterialDialog;
    ProgressDialog              pDialog;
    RelativeLayout              emailLayout, phoneLayout, addrLayout;
    String  name, email, website, phone, designation, company_name, company_addr, notes;
    public double lat,lang;


    private class ImagePagerAdapter extends PagerAdapter {
        Bitmap[] mImages;
        int     imageUriLength;

        ImagePagerAdapter(Bitmap imagePath[]) {
            imageUriLength = imagePath.length;
            mImages = new Bitmap[imageUriLength];
            for (int i = 0; i < imageUriLength; ++i) {
                Bitmap imageUri = imagePath[i];
                mImages[i] = imageUri;
            }
        }

        @Override
        public void destroyItem(final ViewGroup container, final int position, final Object object) {
            container.removeView((ImageView) object);
        }

        @Override
        public int getCount() {
            return this.mImages.length;
        }




        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {
            final Context context = getActivity();
            final ImageView imageView = new ImageView(context);
            imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

            bm = this.mImages[position];
            Drawable drawable = new BitmapDrawable(getResources(), bm);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setImageDrawable(drawable);

            container.addView(imageView, 0);
            return imageView;
        }

        @Override
        public boolean isViewFromObject(final View view, final Object object) {
            return view == object;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // ScannifyApplication.getInstance().trackScreenView("FragmentDetail");
        setHasOptionsMenu(true);

        if(null != getArguments()) {
            backPageString = getArguments().getString("finalPageView");
        }
        if(null == backPageString)
            backPageString = "Empty";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);
        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();

        /*if(null != getArguments()) {
            backPageString = getArguments().getString("finalPageView");
        }
        if(null == backPageString)
            backPageString = "Empty";*/

        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && KeyEvent.ACTION_DOWN == event.getAction()) {
                    Fragment_Detail fragment = new Fragment_Detail();
                    if (fragment.isVisible()) {
                    } else {
                        if(backPageString.equalsIgnoreCase("BookmarkList")){
                        Fragment_Bookmark_List catList       = new Fragment_Bookmark_List();
                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.frame_container, catList).commit();

                        }
                        else {
                            HomeFragment.count              = 1;
                            HomeFragment fragmenthome       = new HomeFragment();
                            FragmentManager fragmentManager = getFragmentManager();
                            fragmentManager.beginTransaction().replace(R.id.frame_container, fragmenthome).commit();
                        }
                    }
                    return true;
                }
                return false;
            }
        });

        getActivity().setTitle("Contact Details");
        actionBar = getActivity().getActionBar();
        actionBar.removeAllTabs();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionbar_height = Utils.pxToDp(actionBar.getHeight());

        /****************************************************************************************/

        appPref = getActivity().getSharedPreferences("AppPref", Context.MODE_PRIVATE);

        txtNameInfo     = (TextView)            rootView.findViewById(R.id.txtNameInfo);
        txtPhoneInfo    = (TextView)            rootView.findViewById(R.id.txtPhoneInfo);
        txtEmailInfo    = (TextView)            rootView.findViewById(R.id.txtEmailInfo);
        txtWebsiteInfo  = (TextView)            rootView.findViewById(R.id.txtWebInfo);
        txtAddrInfo     = (TextView)            rootView.findViewById(R.id.txtAddrInfo);
        txtDesgInfo     = (TextView)            rootView.findViewById(R.id.txtDesgInfo);
        txtNotesInfo    = (TextView)            rootView.findViewById(R.id.txtNotesInfo);
        imageOverlap    = (ImageView)           rootView.findViewById(R.id.overlapImage);
        imgTextName     = (TextView)            rootView.findViewById(R.id.tvName);
        viewPager       = (ViewPager)           rootView.findViewById(R.id.view_pager);
        circleIndicator = (CirclePageIndicator) rootView.findViewById(R.id.indicator);
        rDetail         = (RelativeLayout)      rootView.findViewById(R.id.home);
        txtCompanyInfo  = (TextView)            rootView.findViewById(R.id.txtCompanyInfo);
        emailLayout     = (RelativeLayout)      rootView.findViewById(R.id.emailLayout);
        phoneLayout     = (RelativeLayout)      rootView.findViewById(R.id.phoneLayout);
        addrLayout     = (RelativeLayout)      rootView.findViewById(R.id.addrLayout);

        viewPager.setBackgroundResource(R.drawable.default_bg);

        btnContact = (ButtonRectangle) rootView.findViewById(R.id.btnContact);
        btnContact.setOnClickListener(new View.OnClickListener()

                                      {
                                          @Override
                                          public void onClick(View v) {
                                              Intent intent = new Intent(Intent.ACTION_DIAL);
                                              intent.setData(Uri.parse(phoneNo));
                                              startActivity(intent);
                                          }
                                      }

        );
        imageOverlap.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                switch (flag) {
                    case 0:
                        imageOverlap.setImageResource(R.drawable.ic_bookmark_active);
                        flag = 1;
                        break;
                    case 1:
                        imageOverlap.setImageResource(R.drawable.ic_bookmark_inactive_browsebroadcasts);
                        flag = 0;
                        break;
                }
                new AddBookmarked(flag, id).execute();
            }
        });

        new LoadCard().execute();

        phoneLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("call", "call");

                if (!phone.equalsIgnoreCase("")) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" +phone));
                    startActivity(intent);
                }
            }
        });

        emailLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!email.equalsIgnoreCase("")) {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.putExtra(Intent.EXTRA_EMAIL, email);
                    intent.setType("vnd.android.cursor.item/email");
                    startActivity(Intent.createChooser(intent, "Send Email"));
                }
            }
        });
        return rootView;

    }

    private void showNoHeaderDialog(Holder holder, int gravity, BaseAdapter adapter,
                                    OnClickListener clickListener, OnItemClickListener itemClickListener,
                                    OnDismissListener dismissListener, OnCancelListener cancelListener) {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion > Build.VERSION_CODES.KITKAT) {
            float density = getActivity().getResources().getDisplayMetrics().density;
            if (density >= 4.0) {
                Toast.makeText(getActivity(), "xxxhdpi", Toast.LENGTH_LONG).show();
            } else if (density >= 3.0) {
                final DialogPlus dialog = new DialogPlus.Builder(getActivity())
                        .setContentHolder(holder)
                        .setCancelable(true)
                        .setFooter(R.layout.footer)
                        .setGravity(gravity)
                        .setAdapter(adapter)
                        .setOnClickListener(clickListener)
                        .setOnItemClickListener(itemClickListener)
                        .setOnDismissListener(dismissListener)
                        .setOnCancelListener(cancelListener)
                        .setExpanded(false)
                        .setPadding(0, actionbar_height + 100, 0, 0)
                        .setMargins(0, 0, 0, 100)
                        .create();
                dialog.show();
            } else if (density >= 2.0) {
                final DialogPlus dialog = new DialogPlus.Builder(getActivity())
                        .setContentHolder(holder)
                        .setCancelable(true)
                        .setFooter(R.layout.footer)
                        .setGravity(gravity)
                        .setAdapter(adapter)
                        .setOnClickListener(clickListener)
                        .setOnItemClickListener(itemClickListener)
                        .setOnDismissListener(dismissListener)
                        .setOnCancelListener(cancelListener)
                        .setExpanded(false)
                        .setPadding(0, actionbar_height + 70, 0, 0)
                        .setMargins(0, 0, 0, 100)
                        .create();
                dialog.show();
            } else if (density >= 1.5) {
                final DialogPlus dialog = new DialogPlus.Builder(getActivity())
                        .setContentHolder(holder)
                        .setCancelable(true)
                        .setFooter(R.layout.footer)
                        .setGravity(gravity)
                        .setAdapter(adapter)
                        .setOnClickListener(clickListener)
                        .setOnItemClickListener(itemClickListener)
                        .setOnDismissListener(dismissListener)
                        .setOnCancelListener(cancelListener)
                        .setExpanded(false)
                        .setPadding(0, actionbar_height + 50 , 0, 0)
                        .setMargins(0, 0, 0, 100)
                        .create();
                dialog.show();
            } else {
                final DialogPlus dialog = new DialogPlus.Builder(getActivity())
                        .setContentHolder(holder)
                        .setCancelable(true)
                        .setFooter(R.layout.footer)
                        .setGravity(gravity)
                        .setAdapter(adapter)
                        .setOnClickListener(clickListener)
                        .setOnItemClickListener(itemClickListener)
                        .setOnDismissListener(dismissListener)
                        .setOnCancelListener(cancelListener)
                        .setExpanded(false)
                        .setMargins(0, 0, 0, 100)
                        .create();
                dialog.show();
            }
        } else {
            final DialogPlus dialog = new DialogPlus.Builder(getActivity())
                    .setContentHolder(holder)
                    .setCancelable(true)
                    .setGravity(gravity)
                    .setAdapter(adapter)
                    .setOnClickListener(clickListener)
                    .setOnItemClickListener(itemClickListener)
                    .setOnDismissListener(dismissListener)
                    .setOnCancelListener(cancelListener)
                    .setExpanded(false)
                    .setMargins(0, 0, 0, 100)
                    .create();
            dialog.show();
        }
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.add("Menu 1a").setOnMenuItemClickListener(this.EditButtonClickListener).setIcon(R.drawable.ic_more_vert_white).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
    }

    MenuItem.OnMenuItemClickListener EditButtonClickListener = new MenuItem.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            showMenuDialog(
                    Gravity.TOP
            );
            return true;
        }
    };
    private void showMenuDialog(int gravity) {
        Holder holder;
        holder = new ListHolder();

        OnClickListener clickListener = new OnClickListener() {
            @Override
            public void onClick(DialogPlus dialog, View view) {

            }
        };

        OnItemClickListener itemClickListener = new OnItemClickListener() {
            @Override
            public void onItemClick(final DialogPlus dialog, Object item, View view, int position) {
                if(0==position)
                {
                    FragmentEditCard fragmentCreate = new FragmentEditCard();
                    FragmentManager fragmentManager         = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container, fragmentCreate);
                    fragmentTransaction.addToBackStack("myFrag");
                    fragmentTransaction.commit();
                    dialog.dismiss();
                }
                else
                {
                    mMaterialDialog = new MaterialDialog(getActivity())
                            .setTitle(getResources().getString(R.string.DeleteCard))
                            .setMessage(getResources().getString(R.string.DeleteMessage))
                            .setPositiveButton(getResources().getString(R.string.prog_msg_OK), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    new DeleteCard(id).execute();
                                    mMaterialDialog.dismiss();
                                    dialog.dismiss();
                                }
                            })
                            .setNegativeButton(getResources().getString(R.string.prog_msg_CANCEL), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mMaterialDialog.dismiss();
                                    dialog.dismiss();
                                }
                            });

                    mMaterialDialog.show();
                }
            }
        };

        OnDismissListener dismissListener = new OnDismissListener() {
            @Override
            public void onDismiss(DialogPlus dialog) {
            }
        };

        OnCancelListener cancelListener = new OnCancelListener() {
            @Override
            public void onCancel(DialogPlus dialog) {
            }
        };

        SimpleMenuAdapter adapter = new SimpleMenuAdapter(getActivity());

        showNoHeaderDialog(holder, gravity, adapter, clickListener, itemClickListener, dismissListener, cancelListener
        );
        return;
    }

    class LoadCard extends AsyncTask<String, String, String> {

        boolean failure = false;
       int     isBookmarked = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        }

        @Override
        protected String doInBackground(String... args) {
            // TODO Auto-generated method stub
            // here Check for success tag
            try {
                String cardObj = appPref.getString("selectedCardObject", "Empty");
                JSONObject jsonCardObj = new JSONObject(cardObj);

                if (null != jsonCardObj) {

                    id           = jsonCardObj.getInt("id");
                    name         = jsonCardObj.getString("contact_name");
                    email        = jsonCardObj.getString("contact_email");
                    isBookmarked = jsonCardObj.getInt("isBookmarked");
                    website      = jsonCardObj.getString("website");
                    phone        = jsonCardObj.getString("contact_phone");
                    designation  = jsonCardObj.getString("contact_designation");
                    company_name = jsonCardObj.getString("company_name");
                    company_addr = jsonCardObj.getString("company_address");
                    notes        = jsonCardObj.getString("notes");


                    if (!jsonCardObj.getJSONArray("image").equals(null)) {
                        int k = 0;
                        JSONArray imageArray = jsonCardObj.getJSONArray("image");
                        int arrayLength = imageArray.length();
                        imagePath = new Bitmap[arrayLength];
                        for (int j = 0; j < arrayLength; ++j) {
                            String path = imageArray.get(j).toString();
                            if (null != path) {
                                imagePath[k] = BitmapFactory.decodeStream((InputStream) new URL(path).getContent());
                                k++;
                            }
                        }
                        adapter = new ImagePagerAdapter(imagePath);
                    }
                    return "success";

                } else
                    return "failure";


            } catch (JSONException x) {
                x.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "failure";
        }

        protected void onPostExecute(String message) {
            if (message.equalsIgnoreCase("success")) {
                if (imagePath.length != 0) {
                    if (null != adapter) {
                        circleIndicator.setVisibility(View.VISIBLE);
                        viewPager.setAdapter(adapter);
                        circleIndicator.setViewPager(viewPager);
                    }
                    viewPager.setVisibility(View.VISIBLE);
                } else {
                    viewPager.setAdapter(null);
                    circleIndicator.setVisibility(View.INVISIBLE);
                    viewPager.setBackgroundResource(R.drawable.default_bg);
                    viewPager.setVisibility(View.VISIBLE);
                }
                if(name.equals(""))
                {
                    txtNameInfo.setText("NA");
                    imgTextName.setText("NA");
                }
                else {
                    txtNameInfo.setText(name);
                    imgTextName.setText(name);
                }
                if(phone.equals(""))
                {
                    txtPhoneInfo.setText("NA");
                }
                else {
                    txtPhoneInfo.setText(phone);
                }
                if(email.equals(""))
                {
                    txtEmailInfo.setText("NA");
                }
                else {
                    txtEmailInfo.setText(email);
                }
                if(company_name.equals(""))
                {
                    txtCompanyInfo.setText("NA");
                }
                else {
                    txtCompanyInfo.setText(company_name);
                }
                if(website.equals(""))
                {
                    txtWebsiteInfo.setText("NA");
                }
                else {
                    txtWebsiteInfo.setText(website);
                }
                if(company_addr.equals(""))
                {
                    txtAddrInfo.setText("NA");
                }
                else {
                    txtAddrInfo.setText(company_addr);
                }
                if(designation.equals(""))
                {
                    txtDesgInfo.setText("NA");
                }
                else {
                    txtDesgInfo.setText(designation);
                }

                if(notes.equals(""))
                {
                    txtNotesInfo.setText("NA");
                }
                else {
                    txtNotesInfo.setText(notes);
                }

                imageOverlap.setVisibility(View.VISIBLE);

                if (1 == isBookmarked) {
                    imageOverlap.setImageResource(R.drawable.ic_bookmark_active);
                    flag = 1;
                }
                else {
                    imageOverlap.setImageResource(R.drawable.ic_bookmark_inactive_browsebroadcasts);
                    flag = 0;
                }

                rDetail.setVisibility(View.VISIBLE);
                phoneNo = "tel:" + phone;
            }else {
                rDetail.setVisibility(View.GONE);
            }
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

   public  class AddBookmarked extends AsyncTask<String, String, String>{
        int       flag, crId;
        Context    con = getActivity();
        JSONObject json;
        JSONParser jsonParser = new JSONParser(getActivity());

        public AddBookmarked(int flag, int crId){

            if(flag == 0)
                this.flag = 2;
            else
                this.flag = flag;
            this.crId = crId;
            con = getActivity();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... args) {
            // TODO Auto-generated method stub
            // here Check for success tag
            String username = appPref.getString("Username","nouser");
            List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
            paramsList.add(new BasicNameValuePair("username",   username));
            paramsList.add(new BasicNameValuePair("flag",       String.valueOf(flag)));
            paramsList.add(new BasicNameValuePair("crId",      String.valueOf(crId)));

            json = jsonParser.makeHttpRequest(Urls.AddBookmarked, "POST", paramsList);
            String msg = "Failed";

            if(null != json) {
                try {
                    msg = json.getString("message");
                    return msg;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return msg;
        }

        protected void onPostExecute(String message) {
             Toast.makeText(con, message, Toast.LENGTH_SHORT).show();

        }
    }

    class DeleteCard extends AsyncTask<String, String, String> {
        boolean failure = false;
        int success, cr_id;
        String msg;
        JSONObject json;
        JSONParser jsonParser = new JSONParser(getActivity());

        public DeleteCard(int cr_id){
            this.cr_id = cr_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(null != getActivity()) {
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
            pDialog = new ProgressDialog(getActivity(), getResources().getString(R.string.prog_msg_Editing));
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            // TODO Auto-generated method stub
            try {
                String username = appPref.getString("Username","nouser");
                List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
                paramsList.add(new BasicNameValuePair("username",   username));
                paramsList.add(new BasicNameValuePair("cr_id",      String.valueOf(id)));
                paramsList.add(new BasicNameValuePair("cardTag",    "D"));

                json        = jsonParser.makeHttpRequest(Urls.EditCard, "POST", paramsList);
                if(null != json) {

                    success = json.getInt("success");

                    if (1 == success) {
                        msg = json.getString("message");
                        return msg;
                    } else {
                        msg = json.getString("message");
                        return msg;
                    }
                }
            } catch (JSONException x) {
                x.printStackTrace();
            }
            return msg;
        }

        protected void onPostExecute(String message) {
            if(success == 1) {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                 HomeFragment fragment = new HomeFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();
            }
            else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
            pDialog.dismiss();
            if (null != getActivity()) {
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        }
    }

}
