package app.com.scannify.fragments;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import app.com.scannify.R;
import app.com.scannify.activity.HomeActivity;

public class Category extends Fragment {
    String                   title;
    SharedPreferences        pref;
    SharedPreferences.Editor edit;
    String                   flag;

	public Category() {
	}

	@SuppressLint("CommitPrefEdits")
    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v      = null;
        pref        = getActivity().getSharedPreferences("AppPref", Context.MODE_PRIVATE);
        edit        = pref.edit();
        flag        = pref.getString("Flag","0");
		Bundle args = getArguments();

		 if (args != null) {
             title = args.getString(HomeActivity.CUR_PAGE_TITLE);
             if (title.equalsIgnoreCase("Home")) {
                 v        = inflater.inflate(R.layout.contact_list, container, false);
                 Intent i = new Intent(getActivity(), HomeFragment.class);
                 i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                 startActivity(i);
             }

             else if(title.equalsIgnoreCase("Category List")){
                 v        = inflater.inflate(R.layout.fragment_category_list, container, false);
                 Intent i = new Intent(getActivity(), CategoryList.class);
                 i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                 startActivity(i);
             }
             else if(title.equalsIgnoreCase("Bookmarks")){
                 v        = inflater.inflate(R.layout.bookmark_contact_list, container, false);
                 Intent i = new Intent(getActivity(), Fragment_Bookmark_List.class);
                 i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                 startActivity(i);
             }
             else if(title.equalsIgnoreCase("Settings")){
                 v        = inflater.inflate(R.layout.fragment_settings, container, false);
                 Intent i = new Intent(getActivity(), FragmentSettings.class);
                 i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                 startActivity(i);
             } else if(title.equalsIgnoreCase("About")){
                 v        = inflater.inflate(R.layout.fragment_about, container, false);
                 Intent i = new Intent(getActivity(), About.class);
                 i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                 startActivity(i);
             } /*else if(title.equalsIgnoreCase("Invite Friends")){
                 v        = inflater.inflate(R.layout.fragment_invite_contact, container, false);
                 Intent i = new Intent(getActivity(), About.class);
                 i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                 startActivity(i);
             }*/
             else if(title.equalsIgnoreCase("Invite Friends")){
                 v        = inflater.inflate(R.layout.fragment_invite_contact, container, false);
                 Intent i = new Intent(getActivity(), About.class);
                 i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                 startActivity(i);
             }
         }
		return v;
	}
    @Override
    public void onResume() {
        super.onResume();
    }
}
