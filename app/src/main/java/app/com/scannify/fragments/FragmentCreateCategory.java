package app.com.scannify.fragments;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import app.com.scannify.R;
import app.com.scannify.Utils.JSONParser;
import app.com.scannify.Utils.Urls;
import app.com.scannify.view.Button;
import app.com.scannify.view.ProgressDialog;
import nl.changer.polypicker.ImagePickerActivity;

public class FragmentCreateCategory extends Fragment implements View.OnClickListener {

    public FragmentCreateCategory() {}

    View                        rootview                = null;
    ProgressDialog              pDialog                 = null;
    HashSet<Uri>                mMedia                  = new HashSet<Uri>();
    ImageView                   thumbnail;
    ImageView                   cancelImage;
    Button                      btnForward;
    android.widget.Button       imgAdd;
    EditText                    edAddTitle;
    SharedPreferences           pref,prefApp;
    SharedPreferences.Editor    edit,editApp;
    Intent                      intent;
    ActionBar actionBar;
    int                         id,i =0,j = 0;
    String                      userProfileString,title,username,email,imageUri;
    static int                  MAX_IMAGES = 1;
    int[]                       img_flag = new int[MAX_IMAGES];
    int                         img_set = 0,imageUriLength = 0,add = 0,context;
    JSONObject                  json;
    int                         success, imgSave, serverResponseCode = 0;
    JSONParser                  jsonParser;
    SimpleDateFormat            df     = new SimpleDateFormat("dd MMM yyyy");
    public static int           load   = 0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        img_flag[0] = 0;

        if(null != getArguments()) {
            userProfileString = getArguments().getString("finalPageView");
        }
        if(null == userProfileString)
            userProfileString = "Empty";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //ScannifyApplication.getInstance().trackScreenView("FragmentCreateCategory");
        getActivity().setTitle("Crete Category" + "");
        actionBar = getActivity().getActionBar();
        actionBar.removeAllTabs();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        rootview = inflater.inflate(R.layout.fragment_create_category, container, false);
        rootview.setFocusableInTouchMode(true);
        rootview.requestFocus();
        rootview.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && KeyEvent.ACTION_DOWN == event.getAction()) {
                    FragmentCreateCategory fragment = new FragmentCreateCategory();
                    if (fragment.isVisible())
                    {
                    } else {
                        if(userProfileString.equalsIgnoreCase("CategoryList"))
                        {
                            CategoryList catList       = new CategoryList();
                            FragmentManager fragmentManager = getFragmentManager();
                            fragmentManager.beginTransaction().replace(R.id.frame_container, catList).commit();

                        } else {
                            HomeFragment.count              = 1;
                            HomeFragment fragmenthome       = new HomeFragment();
                            FragmentManager fragmentManager = getFragmentManager();
                            fragmentManager.beginTransaction().replace(R.id.frame_container, fragmenthome).commit();
                        }
                    }
                    return true;
                }
                return false;
            }
        });

        rootview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideSoftKeyboard(getActivity());
                return false;
            }
        });

        init(rootview);
        return rootview;
    }

    @SuppressLint("CommitPrefEdits")
    private void init(View v) {

        edAddTitle       = (EditText)             v.findViewById(R.id.edAddTitle);
        btnForward       = (Button)               v.findViewById(R.id.forwardBtn);
        imgAdd           = (android.widget.Button)v.findViewById(R.id.imgAdd);
        thumbnail        = (ImageView)            v.findViewById(R.id.media_image);
        cancelImage      = (ImageView)            v.findViewById(R.id.inside_image);

        jsonParser = new JSONParser(getActivity());

        prefApp             = getActivity().getSharedPreferences("AppPref", Context.MODE_PRIVATE);
        editApp             = prefApp.edit();



        btnForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String new_title = edAddTitle.getText().toString();

                if (new_title.isEmpty())
                    edAddTitle.setError("Title can not be blank");

                else {
                    title = new_title;
                    new CreateCategory().execute();
                }
            }
        });

        imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMedia.clear();
                intent = new Intent(getActivity(), ImagePickerActivity.class);
                intent.putExtra(ImagePickerActivity.EXTRA_SELECTION_LIMIT, MAX_IMAGES - img_set);
                startActivityForResult(intent, 1);
            }
        });



    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if ( 1 == requestCode) {
                Parcelable[] parcelableUris = data.getParcelableArrayExtra(ImagePickerActivity.EXTRA_IMAGE_URIS);
                Uri[] uris                  = new Uri[parcelableUris.length];
                System.arraycopy(parcelableUris, 0, uris, 0, parcelableUris.length);
                img_set += parcelableUris.length;

                if (uris.length != 0) {
                    for (Uri uri : uris) {
                        mMedia.add(uri);
                    }
                    showMedia();
                }
            }
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    }

    public Uri scaleDown(Bitmap realImage,boolean filter,Uri uri) {
        float actualHeight       = realImage.getHeight();
        float actualWidth        = realImage.getWidth();
        float maxHeight          = (float)600.00;
        float maxWidth           = (float)800.0;
        float imgRatio           = actualWidth/actualHeight;
        float maxRatio           = maxWidth/maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth){
            if(imgRatio < maxRatio){
                imgRatio     = maxHeight / actualHeight;
                actualWidth  = imgRatio * actualWidth;
                actualHeight = maxHeight;
            }
            else if(imgRatio > maxRatio){
                imgRatio     = maxWidth / actualWidth;
                actualHeight = imgRatio * actualHeight;
                actualWidth  = maxWidth;
            }
            else{
                actualHeight = maxHeight;
                actualWidth  = maxWidth;
            }
        }
        int width  = Math.round(actualWidth);
        int height = Math.round(actualHeight);

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,height, filter);
        String imgPath = uri.getPath();
        File newDirectory = new File(Environment.getExternalStorageDirectory()+"/officevikings/");
        if(!newDirectory.exists()){
            if(newDirectory.mkdir()){
                Log.d(getActivity().getClass().getName(), newDirectory.getAbsolutePath()+" directory created");
            }
        }
        File mypath = new File(Environment.getExternalStorageDirectory()+"/officevikings/",imgPath.substring(imgPath.lastIndexOf("/")+1));

        FileOutputStream fos;
        try {
            mypath.createNewFile();
            fos = new FileOutputStream(mypath);
            newBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }

        Uri mImagedUri = Uri.fromFile(mypath);

        return mImagedUri;
    }

    private void showMedia() {
        Iterator<Uri> iterator = mMedia.iterator();
        i = 0;
        j = 0;

        Uri scaledUri = null;

        while(iterator.hasNext()) {
            Uri uri = iterator.next();

            if(null == imageUri) {
                imageUri = uri.toString();

            }

            if(!uri.toString().contains("content://")) {
                uri = Uri.fromFile(new File(uri.toString()));
            }
            try {
                Bitmap bmp= BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(uri));
                scaledUri = scaleDown(bmp, true,  uri);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            thumbnail.setVisibility(View.VISIBLE);
            cancelImage.setVisibility(View.VISIBLE);
            Picasso.with(getActivity()).load(scaledUri)
                    .into(thumbnail);
            break;

        }
        //imageUriLength = imageUri.length;

        cancelImage.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        thumbnail.setImageResource(R.drawable.default_small);
        if( cancelImage  == v ) {
            thumbnail.setImageResource(R.drawable.default_small);
            cancelImage.setVisibility(View.GONE);
            imageUri = null;
            --img_set;
            --imageUriLength;
            img_flag[0] = 0;
        }

    }



    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }


    class CreateCategory extends AsyncTask<String, String, String> {
        boolean failure = false;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(null != getActivity()) {
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
            pDialog = new ProgressDialog(getActivity(), getResources().getString(R.string.prog_msg_Creating));
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            // TODO Auto-generated method stub


            String image = imageUri;
            if(null != image) {
                String newFile = setFileName(image);
                imgSave = uploadFile(newFile);
                if (1 == imgSave)
                    imageUri = newFile;
                else
                    imageUri = null;

            }

            username = prefApp.getString("Username", "UnknownUser");
            try {

                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("username",     username));
                params.add(new BasicNameValuePair("category",     title));
                params.add(new BasicNameValuePair("TAG",          "C"));

                if(null != image)
                    params.add(new BasicNameValuePair("image_path",  imageUri));


                Log.d("request!", "creating");

                json = jsonParser.makeHttpRequest(Urls.CreateCategories, "POST", params);
                if(null != json) {
                    Log.d("create broadcast", json.toString());

                    success = json.getInt("success");
                    if (success == 1) {
                        return "success";
                    } else {
                        return "null";
                    }
                }
                else
                {
                    return "null";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String message) {
            if (null != getActivity()) {
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
            pDialog.dismiss();
            if (success == 1) {

                // Update category object in preference
                try {
                    String str = prefApp.getString("Categories","Empty");
                    if( !str.equalsIgnoreCase("Empty") ){
                        JSONObject categoryObj = new JSONObject(str);
                        JSONObject newObj = json.getJSONObject("category");
                        int catNo = categoryObj.length()+1;
                        categoryObj.put("c_"+catNo, newObj);
                        editApp.putString("Categories",categoryObj.toString());
                        editApp.commit();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //HomeFragment.count              = 1;
                CategoryList fragment           = new CategoryList();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();
            }
        }
    }

    private String setFileName(String text) {
        String filename = text.substring(text.lastIndexOf("/")+1);
        if(filename.contains(" ")) {
            filename = filename.replace(" ", "");
            String path = text.substring(0, text.lastIndexOf("/"));
            path = path + "/" + filename;

            Log.d("Path", path);

            File from = new File(text);
            File to = new File(path);
            from.renameTo(to);

            Log.d("PicturePath", from.toString());

            return to.toString();
        }
        return text;
    }


    public int uploadFile(String sourceFileUri) {
        int successResult = 0;
        if(null != sourceFileUri) {
            String fileName = sourceFileUri;
            String upLoadServerUri = Urls.UploadCategoryImage;
            HttpURLConnection conn;
            DataOutputStream dos;
            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary = "*****";
            File sourceFile = new File(sourceFileUri);

            try {
                URL url = new URL(upLoadServerUri);
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setUseCaches(false);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("uploaded_file", fileName);
                dos = new DataOutputStream(conn.getOutputStream());
                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\"" + fileName + "\"" + lineEnd);
                dos.writeBytes(lineEnd);

                Bitmap myBitmap           = BitmapFactory.decodeFile(sourceFile.getAbsolutePath());
                Bitmap scaleBmp           = resize(myBitmap, true);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                scaleBmp.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                byte[] data = bos.toByteArray();

                dos.write(data, 0, data.length);
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                dos.flush();
                dos.close();
                serverResponseCode           = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();

                // Read response Json from urlConnection
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line = null;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line + '\n');
                }

                if(null != stringBuilder) {
                    JSONObject json = new JSONObject(stringBuilder.toString());
                    successResult = json.getInt("success");
                }

                Log.i("uploadFile", "HTTP Response is : "+ serverResponseMessage + ": " + serverResponseCode);

                myBitmap.recycle();
                scaleBmp.recycle();
                bos.flush();
                bos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return successResult;
    }

    public Bitmap resize(Bitmap realImage,boolean filter) {
        float actualHeight = realImage.getHeight();
        float actualWidth  = realImage.getWidth();
        float maxHeight    = (float) 150.00;
        float maxWidth     = (float) 300.0;
        float imgRatio     = actualWidth / actualHeight;
        float maxRatio     = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio     = maxHeight / actualHeight;
                actualWidth  = imgRatio * actualWidth;
                actualHeight = maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio     = maxWidth / actualWidth;
                actualHeight = imgRatio * actualHeight;
                actualWidth  = maxWidth;
            } else {
                actualHeight = maxHeight;
                actualWidth  = maxWidth;
            }
        }
        int width = Math.round(actualWidth);
        int height = Math.round(actualHeight);
        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,height, filter);
        return newBitmap;
    }
}
