package app.com.scannify.fragments;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;
import app.com.scannify.R;
import app.com.scannify.Utils.ApiCrypter;
import app.com.scannify.Utils.JSONParser;
import app.com.scannify.Utils.Urls;
import app.com.scannify.Utils.Utility;
import app.com.scannify.view.Button;
import app.com.scannify.view.ProgressDialog;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import me.drakeet.materialdialog.MaterialDialog;

public class ChangePassword extends Fragment {

    public ChangePassword(){}

    String                      current,pass,confirm,name;
    View                        rootView;
    MaterialDialog              mMaterialDialog;
    Button                      btnChange;
    ActionBar actionBar;
    EditText                    edCurrent,edNew,edConfirm;
    private ProgressDialog      pDialog;
    SharedPreferences           pref;
    SharedPreferences.Editor    edit;
    JSONObject                  json;
    int                         success;
    JSONParser                  jsonParser;
    private static final String ChangePassword_URL = Urls.ChangePassword;
    private static final String TAG_SUCCESS        = "success";
    private static final String TAG_MESSAGE        = "message";


    @SuppressLint("CommitPrefEdits")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        //ScannifyApplication.getInstance().trackScreenView("ChangePassword");
        rootView = inflater.inflate(R.layout.change_password, container, false);
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && KeyEvent.ACTION_DOWN == event.getAction()) {
                    InviteContact fragment = new InviteContact();
                    if (fragment.isVisible()) {
                    } else {
                        HomeFragment.count = 1;
                        FragmentSettings.back = 1;
                        FragmentManager fragmentManager = getFragmentManager();
                        //fragmentManager.beginTransaction().replace(R.id.frame_container, create).commit();
                        fragmentManager.popBackStack();
                    }
                    return true;
                }
                return false;
            }
        });

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        actionBar = getActivity().getActionBar();
        actionBar.removeAllTabs();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

        pref = getActivity().getSharedPreferences("AppPref", Context.MODE_PRIVATE);
        edit = pref.edit();

        jsonParser = new JSONParser(getActivity());

        name = pref.getString("Username", "UnknownUser");

        edCurrent = (EditText)rootView.findViewById(R.id.edCurrent);
        edNew     = (EditText)rootView.findViewById(R.id.edNewPass);
        edConfirm = (EditText)rootView.findViewById(R.id.edConfirmPass);
        btnChange = (Button)  rootView.findViewById(R.id.btnChangePass);

        btnChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                current = edCurrent.getText().toString();
                pass    = edNew.getText().toString();
                confirm = edConfirm.getText().toString();

                if (current.isEmpty()) {
                    edCurrent.setError(getResources().getString(R.string.prog_msg_Currentpassword_error));
                }else if (pass.isEmpty()) {
                    edNew.setError(getResources().getString(R.string.prog_msg_Newpassword_error));
                }else if (confirm.isEmpty()) {
                    edConfirm.setError(getResources().getString(R.string.prog_msg_Confirmpassword_error));
                }else {
                    if (pass != null && pass.length() >= 6 && pass.length() <= 20) {

                        if (pass.equals(confirm)) {
                            new ChangePass().execute();

                        } else {
                            edConfirm.setError(getResources().getString(R.string.prog_msg_confirm_error));
                        }
                    } else {
                        edNew.setError(getResources().getString(R.string.prog_msg_limit_error));
                    }
                }
            }
        });
        return rootView;
    }

    class ChangePass extends AsyncTask<String, String, String> {

        boolean failure = false;
        String encrypt_new_pwd,encrypt_old_pwd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(null != getActivity()) {
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
            pDialog = new ProgressDialog(getActivity(),getResources().getString(R.string.prog_msg_change_Password));
            pDialog.setCancelable(true);
            pDialog.show();
        }
        @Override
        protected String doInBackground(String... args) {
            // TODO Auto-generated method stub
            // here Check for success tag
            String key = Utility.makeDigest(pref.getString("email",""));

            ApiCrypter crypter = new ApiCrypter(key);
            try {
                encrypt_new_pwd = ApiCrypter.bytesToHex(crypter.encrypt(pass));
                encrypt_old_pwd = ApiCrypter.bytesToHex(crypter.encrypt(current));
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            try {
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("username", name));
                params.add(new BasicNameValuePair("currentPassword", encrypt_old_pwd));
                params.add(new BasicNameValuePair("newPassword", encrypt_new_pwd));

                Log.d("request!", "starting");

                json = jsonParser.makeHttpRequest(ChangePassword_URL, "POST", params);

                if (null != json) {

                    success = json.getInt(TAG_SUCCESS);

                    if (success == 1) {
                        Log.d("Password Changed!", json.toString());
                        edit.putString("Password", json.getString("password"));
                        edit.commit();
                        return json.getString(TAG_MESSAGE);
                    } else {
                        return json.getString(TAG_MESSAGE);
                    }
                }
            }catch(JSONException e){
                e.printStackTrace();
            }

            return "Failed";
        }
        protected void onPostExecute(String message) {
            pDialog.dismiss();
            if (null != getActivity()) {
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
            if ( !message.equalsIgnoreCase("Failed")) {
                Toast.makeText(getActivity(),message,Toast.LENGTH_LONG).show();
                FragmentSettings fragmentSettings = new FragmentSettings();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.frame_container, fragmentSettings).commit();
            }
            else {
                Toast.makeText(getActivity(),message,Toast.LENGTH_LONG).show();
            }
        }
    }
}