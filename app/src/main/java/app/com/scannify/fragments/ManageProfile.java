package app.com.scannify.fragments;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;


import app.com.scannify.R;
import app.com.scannify.Utils.JSONParser;

import app.com.scannify.view.Button;
import app.com.scannify.view.ProgressDialog;
import app.com.scannify.view.RoundedImage;
import app.com.scannify.Utils.Urls;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.drakeet.materialdialog.MaterialDialog;


@SuppressWarnings("ALL")
public class ManageProfile extends Fragment {

    ActionBar actionBar;
    SharedPreferences        pref;
    SharedPreferences.Editor edit;
    private ProgressDialog   pDialog;
    View                     rootview = null;
    RoundedImage             roundedImage;
    ImageView                ivUserImage,ivCancel;
    EditText                 edFullname,edNumber,edCountry,edEmail;
    Button                   btnSave;
    String                   strName;
    String                   picturePath, username, imagePath;
    static int               i = 0;
    Bundle                   bundle;
    JSONObject               json;
    private Spinner          spnGender;
    private String           stredittext, fullname, number,country,email, userGender,strEmail;
    int                      serverResponseCode,success,imgSave,flag = 0;
    MaterialDialog           mMaterialDialog;
    boolean                  okClicked        = true;
    private final int        RESULT_CROP = 400;
    boolean                  matches,isPhone,isName,isCountry;
    String                   isImgUploaded = "0";

    private String[] gender                       = {"Not Set","Male","Female"};
    JSONParser jsonParser;
    private static final String TAG_SUCCESS       = "success";
    private static final String TAG_MESSAGE       = "message";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bitmap bm    = BitmapFactory.decodeResource(getResources(), R.drawable.propic);
        roundedImage = new RoundedImage(bm);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //ScannifyApplication.getInstance().trackScreenView("ManageProfile");
        actionBar = getActivity().getActionBar();
        actionBar.removeAllTabs();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

        rootview = inflater.inflate(R.layout.fragment_manage_profire, container, false);
        rootview.setFocusableInTouchMode(true);
        rootview.requestFocus();
        rootview.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && KeyEvent.ACTION_DOWN == event.getAction()) {
                    ManageProfile fragment = new ManageProfile();
                    if (fragment.isVisible()) {
                    } else {
                        //HomeFragment.count              = 1;
                        HomeFragment fragmenthome       = new HomeFragment();
                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.frame_container, fragmenthome).commit();
                    }
                    return true;
                }
                return false;

            }
        });
        init(rootview);

        return rootview;
    }

    @SuppressLint("CommitPrefEdits")
    private void init(View v) {

        ivUserImage    = (ImageView) v.findViewById(R.id.ivImage);
        btnSave        = (Button)    v.findViewById(R.id.btnSave);
        edFullname     = (EditText)  v.findViewById(R.id.userName);
        edEmail        = (EditText)  v.findViewById(R.id.edEmail);
        edNumber       = (EditText)  v.findViewById(R.id.edContact);
        edCountry      = (EditText)  v.findViewById(R.id.edCountry);
        spnGender      = (Spinner)   v.findViewById(R.id.spnGender);

        pref = getActivity().getSharedPreferences("AppPref", Context.MODE_PRIVATE);
        edit = pref.edit();

        jsonParser = new JSONParser(getActivity());

        v.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideSoftKeyboard(getActivity());
                return false;
            }
        });

        strName          = pref.getString("userFullName","User1");
        strEmail         = pref.getString("email","user@gmail.com");
        number           = pref.getString("number","900000");
        country          = pref.getString("country","900000");
        imagePath        = pref.getString("image","null");
        String genderStr = pref.getString("gender","Male");

        if(number.equalsIgnoreCase("null")){
            edNumber.setText("");
        }
        else{
            edNumber.setText(number);
        }

        if(country.equalsIgnoreCase("null")){
            edCountry.setText("");
        }
        else{
            edCountry.setText(country);
        }
        picturePath = imagePath;

        if(imagePath.equalsIgnoreCase("null")){
            ivUserImage.setImageDrawable(roundedImage);
        }
        else{
            Bitmap bm    = BitmapFactory.decodeFile(imagePath);
            roundedImage = new RoundedImage(bm);
            ivUserImage.setImageDrawable(roundedImage);
            isImgUploaded = "1";
        }

        edFullname.setText(strName);
        edEmail.setText(strEmail);
        edEmail.setFocusable(false);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (0 == flag) {
                    strName = edFullname.getText().toString();
                    country = edCountry.getText().toString();
                    number  = edNumber.getText().toString();
                    email   = edEmail.getText().toString().trim();

                    if (edEmail.getText().length() > 0) {
                        matches = isValidMail(edEmail.getText().toString());
                        if (!matches) {
                            edEmail.setError(getResources().getString(R.string.prog_msg_proper_email));
                        }
                    }

                    if (edNumber.getText().length() > 0) {
                        isPhone = isValidPhone(edNumber.getText().toString());
                        if (!isPhone) {
                            edNumber.setError(getResources().getString(R.string.prog_msg_phone_limit_error));
                        }
                    }

                    if (edCountry.getText().length() > 0) {
                        isCountry = isValidCountry(edCountry.getText().toString());
                        if (!isCountry) {
                            edCountry.setError(getResources().getString(R.string.prog_msg_valid_countryCode_error));
                        }
                    }

                    if (edFullname.getText().length() > 0) {
                        isName = isValidName(edFullname.getText().toString());
                        if (!isName) {
                            edFullname.setError(getResources().getString(R.string.prog_msg_valid_name_error));
                        }
                    }

                    if (strName.isEmpty() || email.isEmpty() || number.isEmpty() || country.isEmpty()) {
                        if (strName.isEmpty()) {
                            edFullname.setError(getResources().getString(R.string.prog_msg_name_error));
                        }
                        if (country.isEmpty()) {
                            edCountry.setError(getResources().getString(R.string.prog_msg_valid_countryCode_error));
                        }
                        if (number.isEmpty()) {
                            edNumber.setError(getResources().getString(R.string.prog_msg_phone_error));
                        }
                        if (email.isEmpty()) {
                            edEmail.setError(getResources().getString(R.string.prog_msg_proper_email));
                        }
                    } else {
                        if (isCountry && isName && isPhone && isCountry) {
                            new AttemptManageProfile().execute(isImgUploaded);
                        }
                    }
                } else
                    new UpdateImage().execute();
            }
        });

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, gender);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnGender.setAdapter(adapter1);
        if(genderStr.equalsIgnoreCase("Not Set") || genderStr.equalsIgnoreCase("null"))
            spnGender.setSelection(0);
        else if(genderStr.equalsIgnoreCase("Male"))
            spnGender.setSelection(1);
        else
            spnGender.setSelection(2);

        spnGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0)
                    userGender = "Not Set";
                else if(i == 1)
                    userGender = "Male";
                else
                    userGender = "Female";
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        ivUserImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 1);
            }
        });

        edFullname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            public void afterTextChanged(Editable edt) {
                if (edFullname.getText().length() > 0) {
                    edFullname.setError(null);
                }
            }
        });

        edFullname.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                    if(edFullname.getText().length()>0) {
                        isName = isValidName(edFullname.getText().toString());
                        if (!isName) {
                            edFullname.setError(getResources().getString(R.string.prog_msg_valid_name_error));
                        }
                    }
                }
            }
        });

        edNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if(edNumber.getText().length()>0) {
                        isPhone = isValidPhone(edNumber.getText().toString());
                        if (!isPhone) {
                            edNumber.setError(getResources().getString(R.string.prog_msg_phone_limit_error));
                        }
                    }
                }
            }
        });

        edCountry.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if(edCountry.getText().length()>0) {
                        isCountry = isValidCountry(edCountry.getText().toString());
                        if (!isCountry) {
                            edCountry.setError(getResources().getString(R.string.prog_msg_valid_countryCode_error));
                        }
                    }
                }
            }
        });

    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && null != data) {
            Uri selectedImage       = data.getData();
            performCrop(selectedImage);
        }

        if (requestCode == RESULT_CROP ) {
            if(resultCode == Activity.RESULT_OK) {
                Bundle extras = data.getExtras();
                Bitmap selectedBitmap = extras.getParcelable("data");
                if (null != selectedBitmap) {
                    roundedImage = new RoundedImage(selectedBitmap);
                    ivUserImage.setImageDrawable(roundedImage);
                    flag = 1;
                }
            }
        }
    }

    private String setFileName(String text) {
        String filename = text.substring(text.lastIndexOf("/")+1);
        filename        = filename.replace(" ", "");
        String path     = text.substring(0,text.lastIndexOf("/"));
        path            = path +"/"+filename;

        Log.d("Path",path);

        File from = new File(text);
        File to   = new File(path);
        from.renameTo(to);

        Log.d("PicturePath",from.toString());

        return to.toString();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    }

    class AttemptManageProfile extends AsyncTask<String, String, String> {

        boolean failure = false;
        String isImgUploaded = "0";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(null != getActivity()) {
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
            pDialog = new ProgressDialog(getActivity(),getResources().getString(R.string.prog_msg_Profile));
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            // TODO Auto-generated method stub
            isImgUploaded = args[0];

            fullname = edFullname.getText().toString();
            email    = edEmail.getText().toString();
            number   = edNumber.getText().toString();
            country  = edCountry.getText().toString();

            username = pref.getString("Username", "UnknownUser");
            try {

                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("username",       username));
                params.add(new BasicNameValuePair("userfullname",   fullname));
                params.add(new BasicNameValuePair("email",          email));
                params.add(new BasicNameValuePair("number",         number));
                params.add(new BasicNameValuePair("country",        country));
                params.add(new BasicNameValuePair("gender",         userGender));

                if(isImgUploaded.equalsIgnoreCase("1"))
                    params.add(new BasicNameValuePair("profimage",  picturePath));

                Log.d("request!", "starting");

                json = jsonParser.makeHttpRequest(Urls.UpdateUser, "POST", params);

                if (null != json) {

                    // checking  log for json response
                    Log.d("Profile Change attempt", json.toString());

                    // success tag for json
                    success = json.getInt(TAG_SUCCESS);

                    if (1 == success) {

                        edit.putString("UserChanged", "True");
                        edit.putString("userFullName", json.getString("fullname"));
                        edit.putString("number", json.getString("number"));
                        edit.putString("country", json.getString("country"));
                        edit.putString("email", json.getString("email"));
                        edit.putString("gender", json.getString("gender"));

                        if(isImgUploaded.equalsIgnoreCase("1"))
                            edit.putString("image", picturePath);
                        edit.commit();

                        return json.getString(TAG_MESSAGE);
                    } else {

                        return json.getString(TAG_MESSAGE);
                    }
                }
            }catch(JSONException e){
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String message) {
            if (message != null) {
                FragmentManager fragmentManager = getFragmentManager();
                HomeFragment fragment = new HomeFragment();
                fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();

                edit.commit();
            }
            pDialog.dismiss();
            if (null != getActivity()) {
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        }
    }

    class UpdateImage extends AsyncTask<String, String, String> {

        boolean failure = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity(),getResources().getString(R.string.prog_msg_UploadingImage));
            pDialog.setCancelable(true);
            pDialog.show();
        }
        @Override
        protected String doInBackground(String... args) {
            // here Check for success tag
            imgSave = uploadFile(picturePath);
            return String.valueOf(imgSave);
        }
        protected void onPostExecute(String message) {
            pDialog.dismiss();
            if (message != null ) {
                //isImgUploaded = imgSave;

                new AttemptManageProfile().execute(message);
                edit.commit();
            }
        }

        public int uploadFile(String sourceFileUri) {
            String fileName        = sourceFileUri;
            String upLoadServerUri = Urls.UploadImage;
            HttpURLConnection conn;
            DataOutputStream dos;
            String lineEnd         = "\r\n";
            String twoHyphens      = "--";
            String boundary        = "*****";
            File sourceFile        = new File(sourceFileUri);
            int  success = 0;

            try {

                URL url = new URL(upLoadServerUri);
                conn    = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setUseCaches(false);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("uploaded_file", fileName);

                dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\"" + fileName + "\"" + lineEnd);
                //-=----------
                dos.writeBytes(lineEnd);

                Bitmap myBitmap           = BitmapFactory.decodeFile(sourceFile.getAbsolutePath());
                Bitmap scaleBmp           = scaleDown(myBitmap, true);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                scaleBmp.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                byte[] data               = bos.toByteArray();

                dos.write(data, 0, data.length);

                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                serverResponseCode           = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();

                // Read response Json from urlConnection
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line = null;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line + '\n');
                }

                if(null != stringBuilder) {
                    JSONObject json = new JSONObject(stringBuilder.toString());
                    success = json.getInt("success");
                }

                Log.i("uploadFile", "HTTP Response is : "+ serverResponseMessage + ": " + serverResponseCode);

                myBitmap.recycle();
                scaleBmp.recycle();
                bos.flush();
                bos.close();
                dos.flush();
                dos.close();
            } catch (MalformedURLException ex) {
                ex.printStackTrace();
                Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return success;
        }
    }

    public Bitmap scaleDown(Bitmap realImage,
                            boolean filter) {
        float actualHeight       = realImage.getHeight();
        float actualWidth        = realImage.getWidth();
        float maxHeight          = (float)600.00;
        float maxWidth           = (float)800.0;
        float imgRatio           = actualWidth/actualHeight;
        float maxRatio           = maxWidth/maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth){
            if(imgRatio < maxRatio){
                //adjust width according to maxHeight
                imgRatio     = maxHeight / actualHeight;
                actualWidth  = imgRatio * actualWidth;
                actualHeight = maxHeight;
            }
            else if(imgRatio > maxRatio){
                //adjust height according to maxWidth
                imgRatio     = maxWidth / actualWidth;
                actualHeight = imgRatio * actualHeight;
                actualWidth  = maxWidth;
            }
            else{
                actualHeight = maxHeight;
                actualWidth  = maxWidth;
            }
        }
        int width  = Math.round(actualWidth);
        int height = Math.round(actualHeight);
        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }

    public  void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    private void performCrop(Uri picUri) {
        try {

            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setDataAndType(picUri, "image/*");

            cropIntent.putExtra("crop", "true");

            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);

            cropIntent.putExtra("outputX", 480);
            cropIntent.putExtra("outputY", 480);


            cropIntent.putExtra("return-data", true);

            File f = createNewFile("CROP_");
            try {
                f.createNewFile();
            } catch (IOException ex) {
                Log.e("io", ex.getMessage());
            }

            Uri mCropImagedUri = Uri.fromFile(f);
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCropImagedUri);

            startActivityForResult(cropIntent, RESULT_CROP);
        }
        catch (ActivityNotFoundException anfe) {

            String errorMessage = "your device doesn't support the crop action!";
            Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private File createNewFile(String prefix){
        if(prefix==null || "".equalsIgnoreCase(prefix)){
            prefix="IMG_";
        }
        File newDirectory = new File(Environment.getExternalStorageDirectory()+"/officevikings/");
        if(!newDirectory.exists()){
            if(newDirectory.mkdir()){
                Log.d(getActivity().getClass().getName(), newDirectory.getAbsolutePath()+" directory created");
            }
        }
        File file = new File(newDirectory,(prefix+System.currentTimeMillis()+".jpg"));
        if(file.exists()){
            //this wont be executed
            file.delete();
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        picturePath = file.getAbsolutePath();
        return file;
    }

    private boolean isValidName(String name)
    {
        boolean check;
        Pattern p;
        Matcher m;

        String NAME_STRING = "[a-zA-Z\\s]+";

        p = Pattern.compile(NAME_STRING);

        m = p.matcher(name);
        check = m.matches();
        return check;
    }

    private boolean isValidPhone(String phone)
    {
        boolean check;
        Pattern p;
        Matcher m;

        String PHONE_STRING = "^[ ()+]*([0-9][ ()+]*){10,13}$";

        p = Pattern.compile(PHONE_STRING);

        m = p.matcher(phone);
        check = m.matches();
        return check;
    }

    private boolean isValidCountry(String country)
    {
        boolean check;
        Pattern p;
        Matcher m;

        String PHONE_STRING = "^[ ()+]*([0-9][ ()+]*){2,4}$";

        p = Pattern.compile(PHONE_STRING);

        m = p.matcher(country);
        check = m.matches();
        return check;
    }

    private boolean isValidHobby(String name)
    {
        boolean check;
        Pattern p;
        Matcher m;

        String NAME_STRING = "[a-zA-Z\\s]+";

        p = Pattern.compile(NAME_STRING);

        m = p.matcher(name);
        check = m.matches();
        return check;
    }

    private boolean isValidMail(String email2)
    {
        boolean check;
        Pattern p;
        Matcher m;

        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        p = Pattern.compile(EMAIL_STRING);

        m = p.matcher(email2);
        check = m.matches();
        return check;
    }
}