package app.com.scannify.fragments;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import app.com.scannify.R;

public class InviteContact extends Fragment {

    public InviteContact(){}
    View                    rootView;
    SharedPreferences        pref;
    SharedPreferences.Editor edit;
    RelativeLayout          rlWhatsapp, rlPhone,rlSMS;
    ActionBar actionBar;
    String                  message;
    String                  username;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       // ScannifyApplication.getInstance().trackScreenView("InviteContact");
        getActivity().setTitle("Invite");
        rootView = inflater.inflate(R.layout.fragment_invite_contact, container, false);
        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && KeyEvent.ACTION_DOWN == event.getAction()) {
                    InviteContact fragment = new InviteContact();
                    if (fragment.isVisible()) {
                    } else {
                       // HomeFragment.count = 1;
                        HomeFragment fragmenthome = new HomeFragment();
                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.frame_container, fragmenthome).commit();
                    }
                    return true;
                }
                return false;
            }
        });

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        message = getResources().getString(R.string.prog_msg_fetching_invite_contacts);

        pref = getActivity().getSharedPreferences("AppPref", Context.MODE_PRIVATE);
        edit = pref.edit();

        username = pref.getString("userFullName","");

        actionBar = getActivity().getActionBar();
        actionBar.removeAllTabs();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

        rlWhatsapp      = (RelativeLayout)rootView.findViewById(R.id.relativeWhatsapp);
        rlPhone         = (RelativeLayout)rootView.findViewById(R.id.relativePhone);
        rlSMS           = (RelativeLayout)rootView.findViewById(R.id.relativeSms);


        rlPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent email = new Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_SUBJECT, "Join RADII");
                email.putExtra(Intent.EXTRA_TEXT, message+username);
                email.setType("message/rfc822");
                startActivity(Intent.createChooser(email, "Choose an Email client :"));
            }
        });

        rlWhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean installed  =   appInstalledOrNot("com.whatsapp");
                if(installed) {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, message+username);
                    sendIntent.setType("text/plain");
                    sendIntent.setPackage("com.whatsapp");
                    startActivity(sendIntent);
                } else {
                    Toast.makeText(getActivity(),"Please install Whatsapp to your phone",Toast.LENGTH_LONG).show();
                }
            }
        });

        rlSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                smsIntent.setType("vnd.android-dir/mms-sms");
                smsIntent.putExtra("sms_body", message+username);
                startActivity(Intent.createChooser(smsIntent, "SMS:"));
            }
        });
        return rootView;
    }

    private boolean appInstalledOrNot(String uri)
    {
        PackageManager pm = getActivity().getPackageManager();
        boolean app_installed = false;
        try
        {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }
        catch (PackageManager.NameNotFoundException e)
        {
            app_installed = false;
        }
        return app_installed ;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
}
