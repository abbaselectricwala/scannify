package app.com.scannify.fragments;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.melnykov.fab.FloatingActionButton;
import com.scanlibrary.ScanActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import app.com.scannify.R;
import app.com.scannify.Utils.JSONParser;
import app.com.scannify.Utils.Urls;
import app.com.scannify.adapter.BookmarkAdapter;
import app.com.scannify.adapter.SpinnerAdapter;
import app.com.scannify.view.ProgressDialog;
import me.drakeet.materialdialog.MaterialDialog;

public class Fragment_Bookmark_List extends Fragment {

    public Fragment_Bookmark_List(){}

    SharedPreferences                   prefApp,pref,prefSettings;
    SharedPreferences.Editor            edit,editApp;
    private Spinner                     spnCat;
    private String[]                    name, email,address, phone,website,isBookmarked;
    ArrayList<JSONObject>               cardsBookmarkedJson = new ArrayList<JSONObject>();
    private  String[][]                 imagePath;
    private TextView                    emptyView;

    //  JSONObject parent;
    CircularProgressView                progressView;
    Thread                              updateThread;
    private RecyclerView                recyclerView;
    private RecyclerView.LayoutManager  layoutManager;
    BookmarkAdapter                     adapterBookmark;
    Bitmap                              bitmap;
    ActionBar actionBar;
    JSONParser                          jsonParserBr;
    JSONObject                          jsonBr,cards;
    ImageView                           imageError;
    static int                          back = 0;
    ProgressDialog                      pDialog         = null;
    MaterialDialog                      mMaterialDialog;
    ImageView                           imageOverlap;
    int                                 flag;
    String                              categories[];
    String                              arr_images[];
    int                                 category_id[];
    SpinnerAdapter                      spnAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

       // ScannifyApplication.getInstance().trackScreenView("Fragment_Bookmark_List");
        getActivity().setTitle("Bookmarks");
        prefApp = getActivity().getSharedPreferences("AppPref", Context.MODE_PRIVATE);
        editApp = prefApp.edit();

        jsonParserBr = new JSONParser(getActivity());

        View rootView = inflater.inflate(R.layout.bookmark_contact_list, container, false);
        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && KeyEvent.ACTION_DOWN == event.getAction()) {
                    Fragment_Bookmark_List fragment = new Fragment_Bookmark_List();
                    if (fragment.isVisible()) {
                    } else {
                        HomeFragment.count              = 1;
                        HomeFragment fragmenthome       = new HomeFragment();
                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.frame_container, fragmenthome).commit();
                    }
                    return true;
                }
                return false;
            }
        });
        emptyView     = (TextView)              rootView.findViewById(R.id.empty_view);
        imageError    = (ImageView)             rootView.findViewById(R.id.imageError);
        progressView  = (CircularProgressView)  rootView.findViewById(R.id.progress_view);
        imageOverlap  = (ImageView)             rootView.findViewById(R.id.overlapImage);
        recyclerView  = (RecyclerView)          rootView.findViewById(R.id.listBrowseBroadcast);
        spnCat        = (Spinner)               rootView.findViewById(R.id.spnCategory);


        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        actionBar     = getActivity().getActionBar();
        actionBar.removeAllTabs();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager         = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                mMaterialDialog = new MaterialDialog(getActivity())
                        .setMessage(getResources().getString(R.string.msg_scan_front))
                        .setPositiveButton(getResources().getString(R.string.bt_yes), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mMaterialDialog.dismiss();
                                Intent i = new Intent(getActivity(),ScanActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                            }
                        })
                        .setNegativeButton(getResources().getString(R.string.bt_no), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mMaterialDialog.dismiss();
                            }
                        });
                mMaterialDialog.show();

            }
        });



        try {
            String str = prefApp.getString("Categories","Empty");
            if( !str.equalsIgnoreCase("Empty") ){
                JSONObject categoryObj = new JSONObject(str);
                categories = new String[categoryObj.length()+1];
                arr_images = new String[categoryObj.length()+1];
                category_id = new int[categoryObj.length()+1];
                JSONObject newObj;
                newObj = categoryObj.getJSONObject("c_1");
                categories[0] = "All";
                arr_images[0] = newObj.getString("image_path");
                for( int cnt = 0;cnt< categoryObj.length(); ++cnt){
                    newObj = categoryObj.getJSONObject("c_"+(cnt+1));

                    categories[cnt+1] = newObj.getString("title");
                    arr_images[cnt+1] = newObj.getString("image_path");
                    category_id[cnt+1] = newObj.getInt("category_id");

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(0 < categories.length){
            spnAdapter = new SpinnerAdapter(getActivity(), R.layout.row, categories,arr_images);
            spnCat.setAdapter(spnAdapter);
        }
        else{
            spnCat.setVisibility(View.GONE);
        }


        spnCat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("CommitPrefEdits")
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int pos = i;
                String cat = spnAdapter.getSelectedCategory(pos);
                new LoadMyCards().execute(cat);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        final GestureDetector mGestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {

            @Override public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

        });



       /* recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                View child = recyclerView.findChildViewUnder(motionEvent.getX(),motionEvent.getY());

                if(child!=null && mGestureDetector.onTouchEvent(motionEvent)){
                    int pos = recyclerView.getChildPosition(child);
                    int k   = 0;
                    try{
                        for (int i = 0; i < cards.length(); i++) {
                            int n = i + 1;
                            JSONObject myCards = cards.getJSONObject("card_" + n);
                            if (k == pos) {
                                editApp.putInt("cardPosition",pos+1);
                                editApp.putString("selectedCardObject",myCards.toString());
                                editApp.putString("allCardsObjects",cards.toString());
                            }k++;
                        }
                    } catch (JSONException x) {
                        x.printStackTrace();
                    }
                    editApp.commit();
                    FragmentManager fragmentManager         = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    Fragment_Detail showContacts             = new Fragment_Detail();
                    Bundle args                             = new Bundle();
                    String finalPageView                  = "BookmarkList";
                    args.putString("finalbroadcast", finalPageView);
                    showContacts.setArguments(args);
                    fragmentTransaction.replace(R.id.frame_container, showContacts);
                    fragmentTransaction.addToBackStack("myFrag");
                    fragmentTransaction.commit();
                    return true;
                }
                return false;
            }
            @Override
            public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
            }
        });*/

        return rootView;
    }



    class LoadMyCards extends AsyncTask<String, String, String> {
        boolean failure = false;
        int success = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(null != getActivity()) {
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
            pDialog = new ProgressDialog(getActivity(), getResources().getString(R.string.prog_msg_loading));
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            // TODO Auto-generated method stub
            String cat          = args[0];
            String username  = prefApp.getString("Username", "noUser");
            List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
            paramsList.add(new BasicNameValuePair("username",  username));
            paramsList.add(new BasicNameValuePair("category",  cat));

            jsonBr = jsonParserBr.makeHttpRequest(Urls.GetBookmarkedCards , "POST", paramsList);

            if(null != jsonBr) {
                try {
                    success = jsonBr.getInt("success");
                    if (1 == success) {

                        cards = jsonBr.getJSONObject("MyCards");

                        int noOfcards = cards.length();

                        name           = new String[noOfcards];
                        address        = new String[noOfcards];
                        phone          = new String[noOfcards];
                        email          = new String[noOfcards];
                        isBookmarked   = new String[noOfcards];
                        website        = new String[noOfcards];
                        imagePath      = new String[noOfcards][2];

                        for (int i = 0; i < noOfcards; i++) {
                            int n = i + 1;
                            JSONObject myCards = cards.getJSONObject("card_" + n);
                            cardsBookmarkedJson.add(i,myCards);
                            name[i]         = myCards.getString("contact_name");
                            address[i]      = myCards.getString("company_address");
                            phone[i]        = myCards.getString("contact_phone");
                            email[i]        = myCards.getString("contact_email");
                            isBookmarked[i] = myCards.getString("isBookmarked");
                            website[i]      = myCards.getString("website");

                            if (!myCards.getJSONArray("image").equals(null)) {
                                JSONArray imageArray = myCards.getJSONArray("image");
                                int arrayLength      = imageArray.length();
                                imagePath[i]         = new String[arrayLength];

                                for (int k = 0; k < arrayLength; k++) {
                                    String path = imageArray.get(k).toString();
                                    try {
                                        imagePath[i][k] = path;
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                        adapterBookmark = new BookmarkAdapter(getActivity(), name, email, imagePath, phone, website,isBookmarked,cardsBookmarkedJson);
                        return "success";
                    }
                } catch (JSONException x) {
                    x.printStackTrace();
                }
            }
            else{
                return "failure";
            }
            return "failure";
        }
        protected void onPostExecute(String message) {
            if(1 == success){
                recyclerView.setAdapter(adapterBookmark);
                recyclerView.setVisibility(View.VISIBLE);
                emptyView.setVisibility(View.GONE);
                imageError.setVisibility(View.GONE);
            }
            else  if (message.equalsIgnoreCase("failure")) {
                recyclerView.setVisibility(View.GONE);
                emptyView.setVisibility(View.VISIBLE);
                imageError.setVisibility(View.VISIBLE);
            } else {
                if (0 == success) {
                    recyclerView.setVisibility(View.GONE);
                    emptyView.setVisibility(View.VISIBLE);
                    imageError.setVisibility(View.VISIBLE);
                }
            }
            pDialog.dismiss();
            if (null != getActivity()) {
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        }

    }


}
