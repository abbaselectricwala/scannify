
package app.com.scannify.fragments;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.com.scannify.R;
import app.com.scannify.Utils.JSONParser;
import app.com.scannify.Utils.Urls;
import app.com.scannify.adapter.ContactAdapter;
import app.com.scannify.adapter.SpinnerAdapter;
import app.com.scannify.view.ButtonRectangle;
import app.com.scannify.view.ProgressDialog;


public class FragmentSearchCard extends Fragment {


    EditText                    edName, edTitle, edCompny;
    Spinner                     spnCat;
    ActionBar actionBar;
    SharedPreferences           prefApp,pref,prefSettings;
    SharedPreferences.Editor    edit,editApp;
    SpinnerAdapter              spnAdapter;
    ButtonRectangle             search;
    String                      categories[],arr_images[];
    int                         category_id[];
    String                      selectedCategory,strName,strTitle,strCmpName;
    ProgressDialog              pDialog         = null;
    ContactAdapter              contactAdapter;



    public FragmentSearchCard() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //ScannifyApplication.getInstance().trackScreenView("FragmentSearchCard");
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_search_card, container, false);
        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && KeyEvent.ACTION_DOWN == event.getAction()) {
                    Fragment_Bookmark_List fragment = new Fragment_Bookmark_List();
                    if (fragment.isVisible()) {
                    } else {
                        HomeFragment.count              = 1;
                        HomeFragment fragmenthome       = new HomeFragment();
                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.frame_container, fragmenthome).commit();
                    }
                    return true;
                }
                return false;
            }
        });

        getActivity().setTitle("Search");
        prefApp = getActivity().getSharedPreferences("AppPref", Context.MODE_PRIVATE);
        editApp = prefApp.edit();

        //edName = (EditText) rootView.findViewById(R.id.edName);
        edTitle = (EditText) rootView.findViewById(R.id.edTitle);
        //edCompny = (EditText) rootView.findViewById(R.id.edCompanyName);
        spnCat = (Spinner) rootView.findViewById(R.id.spnCategory);
        search = (ButtonRectangle)rootView.findViewById(R.id.btsearch);

        actionBar     = getActivity().getActionBar();
        actionBar.removeAllTabs();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

        try {
            String str = prefApp.getString("Categories","Empty");
            if( !str.equalsIgnoreCase("Empty") ){
                JSONObject categoryObj = new JSONObject(str);
                categories = new String[categoryObj.length()+1];
                arr_images = new String[categoryObj.length()+1];
                category_id = new int[categoryObj.length()+1];
                JSONObject newObj;
                newObj = categoryObj.getJSONObject("c_1");
                categories[0] = "All";
                arr_images[0] = newObj.getString("image_path");
                for( int cnt = 0;cnt< categoryObj.length(); ++cnt){
                    newObj = categoryObj.getJSONObject("c_"+(cnt+1));

                    categories[cnt+1] = newObj.getString("title");
                    arr_images[cnt+1] = newObj.getString("image_path");
                    category_id[cnt+1] = newObj.getInt("category_id");

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(0 < categories.length){
            spnAdapter = new SpinnerAdapter(getActivity(), R.layout.row, categories,arr_images);
            spnCat.setAdapter(spnAdapter);
        }
        else{
            spnCat.setVisibility(View.GONE);
        }
        spnCat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("CommitPrefEdits")
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int pos = i;
                selectedCategory = spnAdapter.getSelectedCategory(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //strName    = edName.getText().toString();
                strTitle   = edTitle.getText().toString();
                //strCmpName = edCompny.getText().toString();

                if(strTitle.isEmpty()){
                    edTitle.setError(getResources().getString(R.string.prog_msg_search_string_error));
                }else{
                    strTitle   = strTitle.trim();
                    new SearchCards(selectedCategory).execute();
                }
            }
        });

        return rootView;
    }

    class SearchCards extends AsyncTask<String, String, String>{
        String category,message;
        JSONObject jsonBr;
        JSONParser jsonParser= new JSONParser(getActivity());
        int success = 0;
        String[] name,  phone,email;
        String[][] imagePath = new String[2][];

        public SearchCards(String cat){
            category = cat;

        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(null != getActivity()) {
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
            pDialog = new ProgressDialog(getActivity(), getResources().getString(R.string.prog_msg_searching));
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String username  = prefApp.getString("Username", "noUser");
            List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
            paramsList.add(new BasicNameValuePair("username",  username));
            paramsList.add(new BasicNameValuePair("category",  category));
            //paramsList.add(new BasicNameValuePair("name",  strName));
            paramsList.add(new BasicNameValuePair("searchstr",  strTitle));
            //paramsList.add(new BasicNameValuePair("companyName",  strCmpName));


            jsonBr = jsonParser.makeHttpRequest(Urls.Search , "POST", paramsList);

            if(null != jsonBr) {
                try {
                    success = jsonBr.getInt("success");
                    if (1 == success) {

                        return "success";
                    }

                } catch (JSONException x) {
                    x.printStackTrace();
                }
            }
            else{
                return "failure";
            }
            return "failure";
        }

        protected void onPostExecute(String message){
            if(0 == success){
                Toast.makeText(getActivity(),"No result found",Toast.LENGTH_LONG).show();
            }
            else if(1 == success){

                try {
                    editApp.putString("searchedCardsObject",jsonBr.getJSONObject("MyCards").toString());
                    editApp.commit();
                    FragmentManager fragmentManager         = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    SearchCardList searchCardList             = new SearchCardList();
                    Bundle args                             = new Bundle();
                    String finalPageView                  = "searchcard";
                    args.putString("finalPageView", finalPageView);
                    searchCardList.setArguments(args);
                    fragmentTransaction.replace(R.id.frame_container, searchCardList);
                    fragmentTransaction.addToBackStack("myFrag");
                    fragmentTransaction.commit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            pDialog.dismiss();
            if (null != getActivity()) {
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        }
    }
}
