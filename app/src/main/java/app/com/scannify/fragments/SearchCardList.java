package app.com.scannify.fragments;


import android.app.ActionBar;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.com.scannify.R;
import app.com.scannify.Utils.JSONParser;
import app.com.scannify.adapter.ContactAdapter;
import app.com.scannify.view.ProgressDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchCardList extends Fragment {


    public SearchCardList() {
        // Required empty public constructor
    }
    SharedPreferences                   prefApp,pref,prefSettings;
    SharedPreferences.Editor            edit,editApp;
    private RecyclerView                recyclerView;
    private RecyclerView.LayoutManager  layoutManager;
    ProgressDialog                      pDialog         = null;
    ActionBar actionBar;
    JSONObject                          cards,cardObj;
    ContactAdapter                      contactAdapter   = null;
    int[] id;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       // ScannifyApplication.getInstance().trackScreenView("SearchCardList");
        // Inflate the layout for this fragment
        getActivity().setTitle("Search Result");
        //context = this.getActivity().getApplicationContext();
        prefApp = getActivity().getSharedPreferences("AppPref", Context.MODE_PRIVATE);
        editApp = prefApp.edit();
        View rootView = inflater.inflate(R.layout.fragment_search_card_list, container, false);
        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && KeyEvent.ACTION_DOWN == event.getAction()) {
                    Fragment_Bookmark_List fragment = new Fragment_Bookmark_List();
                    if (fragment.isVisible()) {
                    } else {
                        HomeFragment.count              = 1;
                        HomeFragment fragmenthome       = new HomeFragment();
                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.frame_container, fragmenthome).commit();
                    }
                    return true;
                }
                return false;
            }
        });

        recyclerView  = (RecyclerView)        rootView.findViewById(R.id.search_list);

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        actionBar     = getActivity().getActionBar();
        actionBar.removeAllTabs();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

        String objectStr = prefApp.getString("searchedCardsObject","Empty");
        if(!objectStr.equalsIgnoreCase("Empty")){

            try {
                cardObj  = new JSONObject(objectStr);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        final GestureDetector mGestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {

            @Override public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

        });

        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                View child = recyclerView.findChildViewUnder(motionEvent.getX(),motionEvent.getY());

                if(child!=null && mGestureDetector.onTouchEvent(motionEvent)){

                    int pos = recyclerView.getChildPosition(child);
                    int k   = 0;
                    try{
                        for (int i = 0; i < cardObj.length(); i++) {
                            int n = i + 1;
                            JSONObject myCards = cardObj.getJSONObject("card_" + n);
                            if (k == pos) {
                                editApp.putInt("cardPosition",pos+1);
                                editApp.putString("selectedCardObject",myCards.toString());
                                editApp.putString("allCardsObjects",cardObj.toString());
                            }k++;
                        }
                    } catch (JSONException x) {
                        x.printStackTrace();
                    }
                    editApp.commit();
                    FragmentManager fragmentManager         = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    Fragment_Detail showContacts             = new Fragment_Detail();
                    Bundle args                             = new Bundle();
                    String finalPageView                  = "searchList";
                    args.putString("finalPageView", finalPageView);
                    showContacts.setArguments(args);
                    fragmentTransaction.replace(R.id.frame_container, showContacts);
                    fragmentTransaction.addToBackStack("myFrag");
                    fragmentTransaction.commit();
                    return true;
                }
                return false;
            }
            @Override
            public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
            }

        });

        if(null != cardObj){

            new LoadCardList().execute();
        }

        return rootView;
    }

    class LoadCardList extends AsyncTask<String, String, String> {
        String category,message;
        JSONObject jsonBr,cards;
        JSONParser jsonParser;
        String[] name, address, phone,email,isBookmarked,website;
        String[][] imagePath = new String[2][];
        ArrayList<JSONObject> cardsJson = new ArrayList<>();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(null != getActivity()) {
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
            pDialog = new ProgressDialog(getActivity(), getResources().getString(R.string.prog_msg_searching));
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            if(null != cardObj) {
                try {
                    cards = cardObj;

                        int noOfcrds = cards.length();
                        id              = new int[noOfcrds] ;
                        name            = new String[noOfcrds];
                        address         = new String[noOfcrds];
                        phone           = new String[noOfcrds];
                        email           = new String[noOfcrds];
                        isBookmarked    = new String[noOfcrds];
                        website         = new String[noOfcrds];
                        imagePath       = new String[noOfcrds][2];

                        for (int i = 0; i < noOfcrds; i++) {
                            int n = i + 1;
                            JSONObject myCards = cards.getJSONObject("card_" + n);
                            cardsJson.add(i, myCards);
                            id[i]           = myCards.getInt("id");
                            name[i]         = myCards.getString("contact_name");
                            address[i]      = myCards.getString("company_address");
                            phone[i]        = myCards.getString("contact_phone");
                            email[i]        = myCards.getString("contact_email");
                            isBookmarked[i] = myCards.getString("isBookmarked");
                            website[i]      = myCards.getString("website");

                            if (!myCards.getJSONArray("image").equals(null)) {
                                JSONArray imageArray = myCards.getJSONArray("image");
                                int arrayLength = imageArray.length();
                                imagePath[i] = new String[arrayLength];

                                for (int k = 0; k < arrayLength; k++) {
                                    String path = imageArray.get(k).toString();
                                    try {
                                        imagePath[i][k] = path;
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }


                        contactAdapter = new ContactAdapter(getActivity(), name, email, imagePath, phone, website,isBookmarked,cardsJson,id, isBookmarked);
                        return "success";


                } catch (JSONException x) {
                    x.printStackTrace();
                }
            }
            else{
                return "failure";
            }
            return "failure";
        }

        protected void onPostExecute(String message){

            if(null != contactAdapter){
                recyclerView.setAdapter(contactAdapter);
                recyclerView.setVisibility(View.VISIBLE);

            }
            else{
                recyclerView.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "No result found", Toast.LENGTH_LONG).show();
            }
            pDialog.dismiss();
            if (null != getActivity()) {
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }


        }
    }


}
