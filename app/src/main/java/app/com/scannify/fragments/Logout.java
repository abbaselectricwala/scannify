package app.com.scannify.fragments;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import app.com.scannify.R;
import app.com.scannify.Utils.JSONParser;
import app.com.scannify.Utils.Urls;
import app.com.scannify.activity.LoginActivity;
import app.com.scannify.view.ProgressDialog;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Logout extends Fragment {


    public Logout() {
    }
    View                        rootView;
    SharedPreferences           pref,prefMessage,broadcastPref;
    String                      username,sessionid;
    Integer                     success;
    JSONParser                  jsonParser;
    ProgressDialog              pDialog;
    SharedPreferences.Editor    broadcastEdit,editor,edit;

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //ScannifyApplication.getInstance().trackScreenView("Logout");
        pref             = this.getActivity().getSharedPreferences("AppPref", Context.MODE_PRIVATE);
        prefMessage      = this.getActivity().getSharedPreferences("MessagePreference", Context.MODE_PRIVATE);
        broadcastPref    = getActivity().getSharedPreferences("CreatedBroadcast", Context.MODE_PRIVATE);
        broadcastEdit    = broadcastPref.edit();
        editor           = prefMessage.edit();
        edit             = pref.edit();
        jsonParser = new JSONParser(getActivity());

        new AttemptLogin().execute();

        return rootView;
    }

    class AttemptLogin extends AsyncTask<String, String, String> {

        boolean failure = false;

        @Override
        protected void onPreExecute() {
            if(null != getActivity()) {
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
            pDialog = new ProgressDialog(getActivity(),getResources().getString(R.string.prog_msg_loading));
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            // TODO Auto-generated method stub
            // here Check for success tag
             username = pref.getString("Username",null);
             sessionid = pref.getString("sessionid", null);

            try {

                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("username",  username));
                params.add(new BasicNameValuePair("sessionid", sessionid));

                Log.d("request!", "ending");

                JSONObject json = jsonParser.makeHttpRequest( Urls.Logout , "POST", params);

                // checking  log for json response
                if(null != json) {
                    Log.d("Logout attempt", json.toString());

                    // success tag for json
                    success = json.getInt(TAG_SUCCESS);
                    if (success == 1) {
                        Log.d("Successfully Log Out!", json.toString());

                        Intent ii = new Intent(getActivity(), LoginActivity.class);
                        // set the new task and clear flags
                        ii.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(ii);
                        return json.getString(TAG_MESSAGE);
                    } else {

                        return json.getString(TAG_MESSAGE);

                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }
        @SuppressLint("CommitPrefEdits")
        protected void onPostExecute(String message) {
            if (1 == success) {
                edit.clear();
                edit.commit();
                broadcastEdit.clear();
                broadcastEdit.commit();
            }
            editor.clear();
            editor.commit();
            pDialog.dismiss();
            if(null != getActivity()) {
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        }
    }
}
