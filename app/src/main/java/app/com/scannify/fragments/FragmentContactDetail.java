package app.com.scannify.fragments;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.scanlibrary.ScanActivity;
import com.scanlibrary.Utils;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.com.scannify.R;
import app.com.scannify.Utils.JSONParser;
import app.com.scannify.Utils.Urls;
import app.com.scannify.Utils.Validate;
import app.com.scannify.adapter.SpinnerAdapter;
import app.com.scannify.view.ButtonRectangle;
import app.com.scannify.view.ProgressDialog;
import me.drakeet.materialdialog.MaterialDialog;

public class FragmentContactDetail  extends android.app.Fragment {

    EditText                 name, email, phone, website, address, designation, company, ed1, ed2,edNotes;
    RelativeLayout           main;
    Validate                 validate;
    List<String>      valuePairs;
    public ButtonRectangle   save, add;
    RelativeLayout           rel1, rel2;
    TextView                 tv1, tv2;
    ActionBar actionBar;
    JSONObject               jsonCr;
    JSONParser               jsonParser;
    SharedPreferences        detail,appPref,repeat_sp;
    SharedPreferences.Editor appEdit,editDetail,edit_repeat_sp;
    static int               back = 0;
    MaterialDialog           mMaterialDialog;
    public static int        scan_count = 0;
    String                   image, spnSelectedItem;
    ImageView imgviewfront,imgviewback;
    Bitmap                   bitmap;
    ProgressDialog           pDialog         = null;
    Uri                      imagepathfront,imagepathback;
    private Spinner          spnCat;
    SpinnerAdapter           spnAdapter;
    String                   categories[];
    String                   arr_images[];
    int                      category_id[];
    boolean                  isEmailValid,isPhoneValid;
    boolean                  isClicked = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       // ScannifyApplication.getInstance().trackScreenView("FragmentContactDetail");
        View rootView = inflater.inflate(R.layout.fragment_contact_detail, container, false);
        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();

        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && KeyEvent.ACTION_DOWN == event.getAction()) {
                    FragmentContactDetail fragment = new FragmentContactDetail();
                    if (fragment.isVisible()) {
                    } else {
                        if (back == 0) {
                            // HomeFragment.count              = 1;
                            HomeFragment fragmenthome       = new HomeFragment();
                            FragmentManager fragmentManager = getFragmentManager();
                            fragmentManager.beginTransaction().replace(R.id.frame_container, fragmenthome).commit();

                        } else {
                            back = 0;
                        }
                    }
                    return true;
                }
                return false;
            }
        });

        actionBar     = getActivity().getActionBar();
        actionBar.removeAllTabs();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        //getActivity().setTitle("ContactDetailActivity");

        /****************************************************************************************/

        repeat_sp = getActivity().getSharedPreferences("Repeat_Scan", Context.MODE_PRIVATE);
        edit_repeat_sp = repeat_sp.edit();

        appPref = getActivity().getSharedPreferences("AppPref", Context.MODE_PRIVATE);
        appEdit = appPref.edit();

        jsonParser = new JSONParser(getActivity());

        detail = getActivity().getSharedPreferences("Details", Context.MODE_PRIVATE);
        editDetail = detail.edit();

        String data = getActivity().getIntent().getStringExtra("data");
        System.out.print(data);
        name         = (EditText)        rootView.findViewById(R.id.edName);
        phone        = (EditText)        rootView.findViewById(R.id.edPhone);
        email        = (EditText)        rootView.findViewById(R.id.edEmail);
        website      = (EditText)        rootView.findViewById(R.id.edWebsite);
        address      = (EditText)        rootView.findViewById(R.id.edAddress);
        edNotes      = (EditText)        rootView.findViewById(R.id.edNotes);
        main         = (RelativeLayout)  rootView.findViewById(R.id.main);
        designation        = (EditText)        rootView.findViewById(R.id.edDesignatoin);
        company      = (EditText)        rootView.findViewById(R.id.edCmpName);
        save         = (ButtonRectangle) rootView.findViewById(R.id.btsave);
        add         = (ButtonRectangle) rootView.findViewById(R.id.btAdd);
        rel1         = (RelativeLayout)  rootView.findViewById(R.id.rlTitle1);
        tv1          = (TextView)        rootView.findViewById(R.id.tv1);
        ed1          = (EditText)        rootView.findViewById(R.id.ed1);
        rel2         = (RelativeLayout)  rootView.findViewById(R.id.rlTitle2);
        tv2          = (TextView)        rootView.findViewById(R.id.tv2);
        ed2          = (EditText)        rootView.findViewById(R.id.ed2);
        imgviewfront =(ImageView) rootView.findViewById(R.id.imgviewfront);
        imgviewback  =(ImageView) rootView.findViewById(R.id.imgviewback);
        spnCat       =(Spinner)          rootView.findViewById(R.id.spnCategory);

        valuePairs = new ArrayList<String>();

        try {
            String str = appPref.getString("Categories","Empty");
            if( !str.equalsIgnoreCase("Empty") ){
                JSONObject categoryObj = new JSONObject(str);
                categories  = new String[categoryObj.length()];
                arr_images  = new String[categoryObj.length()];
                category_id = new    int[categoryObj.length()];
                JSONObject newObj;

                for( int cnt = 0;cnt< categoryObj.length(); ++cnt){

                    newObj = categoryObj.getJSONObject("c_"+(cnt+1));
                    categories[cnt]  = newObj.getString("title");
                    arr_images[cnt]  = newObj.getString("image_path");
                    category_id[cnt] = newObj.getInt("category_id");

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(0 < categories.length){
            spnAdapter = new SpinnerAdapter(getActivity(), R.layout.row, categories,arr_images);
            spnCat.setAdapter(spnAdapter);
        }
        else{
            spnCat.setVisibility(View.GONE);
        }

        spnCat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spnSelectedItem = spnAdapter.getSelectedCategory(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        validate = new Validate(getActivity(), data);
        if(scan_count == 0) {
            //String str = appPref.getString("imagepathfront", image);
            Bitmap scanfrontbitmap = decodeBase64(appPref.getString("imagepathfront", image));
            imagepathfront =  Utils.getUri(getActivity(),scanfrontbitmap);
            appEdit.putString("imagepathfrontURI", imagepathfront.toString());
            appEdit.apply();
            add.setEnabled(true);
            add.setBackgroundColor(getResources().getColor(R.color.dark_blue));
            //showDialog();
        }
        else  {
            add.setEnabled(false);
            add.setBackgroundColor(getResources().getColor(R.color.disabled_button_color));
            //imagepathfront =  Uri.parse(appPref.getString("imagepathfront", image));
            Bitmap scanbackbitmap = decodeBase64(appPref.getString("imagepathback", image));
            imagepathback = Utils.getUri(getActivity(),scanbackbitmap);
            appEdit.putString("imagepathbackURI", imagepathback.toString());
            appEdit.apply();
            displayDetails();
        }

      /*  name.setText(validate.getNameStr());
        email.setText(validate.getEmailStr());
        website.setText(validate.getWebsiteStr());
         address.setText(validate.getAddressStr());
        title.setText(validate.getTitleStr());
        company.setText(validate.getCompanyStr());*/
        if(null != appPref.getString("imagepathfrontURI", image)) {
            Bitmap bitmapFront = null;
            if (appPref.getInt("isEdited", 0) == 1) {
                Picasso.with(getActivity()).load(appPref.getString("imagepathfrontURI", image)).into(imgviewfront);

            } else {
                try {
                    bitmapFront = Utils.getBitmap(getActivity(), Uri.parse(appPref.getString("imagepathfrontURI", image)));
                    if (null != bitmapFront) {
                        imgviewfront.setImageBitmap(bitmapFront);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            // Bitmap bitmapFront = BitmapFactory.decodeFile(appPref.getString("imagepathfront", image));
        }
        if(null != appPref.getString("imagepathbackURI", image)) {
            Bitmap bitmapBack = null;
            try {
                bitmapBack = Utils.getBitmap(getActivity(), Uri.parse(appPref.getString("imagepathbackURI", image)));
                if (null != bitmapBack) {
                    imgviewback.setImageBitmap(bitmapBack);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            //Bitmap bitmapBack = BitmapFactory.decodeFile(appPref.getString("imagepathback", image));
            //imgviewback.setImageBitmap(bitmapBack);
        }
        else{
            imgviewback.setImageDrawable(getResources().getDrawable(R.drawable.groupicon));
        }

      /*  valuePairs = validate.getPhoneStr();
        int count = valuePairs.size();
        NameValuePair nm;
        if (count == 1) {
            nm = valuePairs.get(0);
            String value = nm.getValue();
            phone.setText(value);
        }
        if (count == 2) {
            nm = valuePairs.get(0);
            String value = nm.getValue();
            phone.setText(value);

            RelativeLayout rel1 = (RelativeLayout) rootView.findViewById(R.id.rlTitle1);
            rel1.setVisibility(View.VISIBLE);
            TextView tv1 = (TextView) rootView.findViewById(R.id.tv1);
            EditText ed1 = (EditText) rootView.findViewById(R.id.ed1);
            nm = valuePairs.get(1);
            String text = nm.getName();
            tv1.setText(text);
            String val = nm.getValue();
            ed1.setText(val);
        }

        if (count == 3) {
            nm = valuePairs.get(0);
            String value = nm.getValue();
            phone.setText(value);

            RelativeLayout rel1 = (RelativeLayout) rootView.findViewById(R.id.rlTitle1);
            rel1.setVisibility(View.VISIBLE);
            TextView tv1 = (TextView) rootView.findViewById(R.id.tv1);
            EditText ed1 = (EditText) rootView.findViewById(R.id.ed1);
            nm = valuePairs.get(1);
            String text = nm.getName();
            tv1.setText(text);
            String val = nm.getValue();
            ed1.setText(val);

            RelativeLayout rel2 = (RelativeLayout) rootView.findViewById(R.id.rlTitle2);
            rel2.setVisibility(View.VISIBLE);
            TextView tv2 = (TextView) rootView.findViewById(R.id.tv2);
            EditText ed2 = (EditText) rootView.findViewById(R.id.ed2);
            nm = valuePairs.get(2);
            String text1 = nm.getName();
            tv2.setText(text1);
            String val1 = nm.getValue();
            ed2.setText(val1);
        }*/

        displayDetails();

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isClicked == false) {
                    isClicked = true;

                    edit_repeat_sp.putString("repeat_value", "yes");
                    edit_repeat_sp.commit();
                    if (!name.getText().toString().equals("")) {
                        editDetail.putString("name", validate.getNameStr());
                    } else {
                        editDetail.putString("name", "null");
                    }
                    if (!email.getText().toString().equals("")) {
                        editDetail.putString("email", validate.getEmailStr());
                    } else {
                        editDetail.putString("email", "null");
                    }
                    if (!address.getText().toString().equals("")) {
                        editDetail.putString("address", validate.getAddressStr());
                    } else {
                        editDetail.putString("address", "null");
                    }
                    if (!website.getText().toString().equals("")) {
                        editDetail.putString("website", validate.getWebsiteStr());
                    } else {
                        editDetail.putString("website", "null");
                    }
                    if (!designation.getText().toString().equals("")) {
                        editDetail.putString("designation", validate.getDesignationStr());
                    } else {
                        editDetail.putString("designation", "null");
                    }
                    if (!company.getText().toString().equals("")) {
                        editDetail.putString("company", validate.getCompanyStr());
                    } else {
                        editDetail.putString("company", "null");
                    }
                    if (!phone.getText().toString().equals("")) {
                        editDetail.putString("phone", phone.getText().toString());
                    } else {
                        editDetail.putString("phone", "null");
                    }
                    if (!edNotes.getText().toString().equals("")) {
                        editDetail.putString("notes", edNotes.getText().toString());
                    } else {
                        editDetail.putString("notes", "null");
                    }
                    editDetail.apply();
                    scan_count++;
//                    mMaterialDialog.dismiss();
                    Intent i = new Intent(getActivity(), ScanActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nameTxt = name.getText().toString();
                String emailTxt = email.getText().toString();
                String phTxt = phone.getText().toString();
                int emailFlag = 0, nameFlag = 0, phoneFlag = 0;

                if(nameTxt.isEmpty() || emailTxt.isEmpty() || phTxt.isEmpty()) {
                    if (nameTxt.isEmpty()) {
                        name.setError(getResources().getString(R.string.prog_msg_name_error));
                        nameFlag = 1;
                    }
                    if (emailTxt.isEmpty()) {
                        email.setError(getResources().getString(R.string.prog_msg_proper_email));
                        emailFlag = 1;
                    }
                    if (phTxt.isEmpty()) {
                        phone.setError(getResources().getString(R.string.prog_msg_phone_error));
                        phoneFlag = 1;
                    }
                }
                if(emailTxt.length()>0) {
                    isEmailValid = Validate.isEmailValid(emailTxt);
                    if (!isEmailValid) {
                        email.setError(getResources().getString(R.string.prog_msg_proper_email));
                        emailFlag = 1;
                    }
                }
                if(phone.getText().length()>0) {
                    isPhoneValid = isValidPhone(phone.getText().toString());
                    if (!isPhoneValid) {
                        phone.setError(getResources().getString(R.string.prog_msg_phone_limit_error));
                        phoneFlag = 1;
                    }
                }

                if(nameFlag == 0 && emailFlag == 0 && phoneFlag == 0 ) {
                    showConfirmDialog();
                }

            }
        });

        return rootView;
    }
    // method for base64 to bitmap
    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory
                .decodeByteArray(decodedByte, 0, decodedByte.length);
    }
    private void displayDetails() {
         if (scan_count == 0) {
        name.setText(validate.getNameStr());
        //phone.setText(validate.getPhoneStr());
        email.setText(validate.getEmailStr());
        website.setText(validate.getWebsiteStr());
        address.setText(validate.getAddressStr());
        designation.setText(validate.getDesignationStr());
        company.setText(validate.getCompanyStr());

             valuePairs = validate.getPhoneStr();
             int count = valuePairs.size();
             NameValuePair nm;
             if (count == 1) {
                 //nm = valuePairs.get(0);
                 String value = valuePairs.get(0);
                 phone.setText(value);
             }
             if (count == 2) {
                 // nm = valuePairs.get(0);
                 String value = valuePairs.get(0);
                 phone.setText(value);
                 rel1.setVisibility(View.VISIBLE);
                 //nm = valuePairs.get(1);
                 // String text = nm.getName();
                 //tv1.setText(text);
                 String val = valuePairs.get(1);
                 ed1.setText(val);
             }

             if (count == 3) {
                 //nm = valuePairs.get(0);
                 String value = valuePairs.get(0);
                 phone.setText(value);

                 rel1.setVisibility(View.VISIBLE);
                 //nm = valuePairs.get(1);
                 //String text = nm.getName();
                 //tv1.setText(text);
                 String val = valuePairs.get(1);
                 ed1.setText(val);

                 rel2.setVisibility(View.VISIBLE);
                 //nm = valuePairs.get(2);
                 // String text1 = nm.getName();
                 // tv2.setText(text1);
                 String val1 = valuePairs.get(2);
                 ed2.setText(val1);
             }
    }  else {
             if (detail.getString("name", "").equalsIgnoreCase("null")) {
                 //name.setText("No Name Available");
                 name.setText(validate.getNameStr());
             } else {
                 //name.setText(validate.getNameStr());
                 name.setText(detail.getString("name", ""));
             }
             if (detail.getString("address", "").equalsIgnoreCase("null")) {
                 //address.setText("No Address Available");
                 address.setText(validate.getAddressStr());
             } else {
                 //address.setText(validate.getAddressStr());
                 address.setText(detail.getString("address", ""));
             }
             if (detail.getString("email", "").equalsIgnoreCase("null")) {
                 //email.setText("No email Available");
                 email.setText(validate.getEmailStr());
             } else {
                 //email.setText(validate.getEmailStr());
                 email.setText(detail.getString("email", ""));
             }
             if (detail.getString("designation", "").equalsIgnoreCase("null")) {
                 //title.setText("No Title Available");
                 designation.setText(validate.getDesignationStr());
             } else {
                 //title.setText(validate.getTitleStr());
                 designation.setText(detail.getString("designation", ""));
             }
             if (detail.getString("company", "").equalsIgnoreCase("null")) {
                 //company.setText("No company Name Available");
                 company.setText(validate.getCompanyStr());
             } else {
                 //company.setText(validate.getCompanyStr());
                 company.setText(detail.getString("company", ""));
             }
             if (detail.getString("website", "").equalsIgnoreCase("null")) {
                 //website.setText("No Website Available");
                 website.setText(validate.getWebsiteStr());
             } else {
                 //website.setText(validate.getWebsiteStr());
                 website.setText(detail.getString("website", ""));
             }

             if (detail.getString("phone", "").equalsIgnoreCase("null")) {
                 valuePairs = validate.getPhoneStr();
                 int count = valuePairs.size();
                 NameValuePair nm;
                 if (count == 1) {
                     //nm = valuePairs.get(0);
                     String value = valuePairs.get(0);
                     phone.setText(value);
                 }
                 if (count == 2) {
                    // nm = valuePairs.get(0);
                     String value = valuePairs.get(0);
                     phone.setText(value);
                     rel1.setVisibility(View.VISIBLE);
                     //nm = valuePairs.get(1);
                    // String text = nm.getName();
                     //tv1.setText(text);
                     String val = valuePairs.get(1);
                     ed1.setText(val);
                 }

                 if (count == 3) {
                     //nm = valuePairs.get(0);
                     String value = valuePairs.get(0);
                     phone.setText(value);

                     rel1.setVisibility(View.VISIBLE);
                     //nm = valuePairs.get(1);
                     //String text = nm.getName();
                     //tv1.setText(text);
                     String val = valuePairs.get(1);
                     ed1.setText(val);

                     rel2.setVisibility(View.VISIBLE);
                     //nm = valuePairs.get(2);
                    // String text1 = nm.getName();
                    // tv2.setText(text1);
                     String val1 = valuePairs.get(2);
                     ed2.setText(val1);
                 }
             } else {
                 phone.setText(detail.getString("phone", ""));
             }
         }
         }

    private boolean isValidPhone(String phone)
    {
        boolean check;
        Pattern p;
        Matcher m;

        String PHONE_STRING = "^[ ()+]*([0-9][ ()+]*){10,13}$";

        p = Pattern.compile(PHONE_STRING);

        m = p.matcher(phone);
        check = m.matches();
        return check;
    }

    private boolean isValidMail(String email2)
    {
        boolean check;
        Pattern p;
        Matcher m;

        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        p = Pattern.compile(EMAIL_STRING);

        m = p.matcher(email2);
        check = m.matches();
        return check;
    }


    public void showDialog() {

        mMaterialDialog = new MaterialDialog(getActivity())
                .setMessage(getResources().getString(R.string.msg_scan_back))
                .setPositiveButton(getResources().getString(R.string.bt_yes), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        edit_repeat_sp.putString("repeat_value", "yes");
                        edit_repeat_sp.commit();
                        if (!name.getText().toString().equals("")) {
                            editDetail.putString("name", validate.getNameStr());
                        } else {
                            editDetail.putString("name", "null");
                        }
                        if (!email.getText().toString().equals("")) {
                            editDetail.putString("email", validate.getEmailStr());
                        } else {
                            editDetail.putString("email", "null");
                        }
                        if (!address.getText().toString().equals("")) {
                            editDetail.putString("address", validate.getAddressStr());
                        } else {
                            editDetail.putString("address", "null");
                        }
                        if (!website.getText().toString().equals("")) {
                            editDetail.putString("website", validate.getWebsiteStr());
                        } else {
                            editDetail.putString("website", "null");
                        }
                        if (!designation.getText().toString().equals("")) {
                            editDetail.putString("designation", validate.getDesignationStr());
                        } else {
                            editDetail.putString("designation", "null");
                        }
                        if (!company.getText().toString().equals("")) {
                            editDetail.putString("company", validate.getCompanyStr());
                        } else {
                            editDetail.putString("company", "null");
                        }
                        if (!phone.getText().toString().equals("")) {
                            editDetail.putString("phone", phone.getText().toString());
                        } else {
                            editDetail.putString("phone", "null");
                        }
                        editDetail.apply();
                        scan_count++;
                        mMaterialDialog.dismiss();
                        Intent i = new Intent(getActivity(), ScanActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }
                })
                .setNegativeButton(getResources().getString(R.string.bt_no), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                        scan_count = 0;
                    }
                });
        mMaterialDialog.show();
    }

    public void showConfirmDialog() {
        mMaterialDialog = new MaterialDialog(getActivity())
                .setMessage(getResources().getString(R.string.msg_check))
                .setPositiveButton(getResources().getString(R.string.bt_confirm), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();

                        new saveMyCard().execute();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.bt_check), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                    }
                });
        mMaterialDialog.show();
    }
    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
    class saveMyCard extends AsyncTask<String, String, String> {
        boolean failure             = false;
        int     serverResponseCode  = 0;
        int[]   imgSave             = new int[2];

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(null != getActivity()) {
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
            pDialog = new ProgressDialog(getActivity(), getResources().getString(R.string.prog_msg_saving));
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            // TODO Auto-generated method stub

            if(null != appPref.getString("imagepathfrontURI", image)) {
                if (appPref.getInt("isEdited",0) != 1 ) {
                 imgSave[0] = uploadFile(getRealPathFromURI(Uri.parse(appPref.getString("imagepathfrontURI", image))));
                }
            }
            if(null != appPref.getString("imagepathbackURI", image)) {
                imgSave[1] = uploadFile(getRealPathFromURI(Uri.parse(appPref.getString("imagepathbackURI", image))));
            }
            String username  = appPref.getString("Username", "noUser");
            List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
            paramsList.add(new BasicNameValuePair("username",           username));
            paramsList.add(new BasicNameValuePair("category",           spnSelectedItem));
            paramsList.add(new BasicNameValuePair("contact_name",       name.getText().toString()));
            paramsList.add(new BasicNameValuePair("contact_email",      email.getText().toString()));
            paramsList.add(new BasicNameValuePair("contact_phone",      phone.getText().toString()));
            paramsList.add(new BasicNameValuePair("contact_designation",designation.getText().toString()));
            paramsList.add(new BasicNameValuePair("company_name",       company.getText().toString()));
            paramsList.add(new BasicNameValuePair("company_address",    address.getText().toString()));
            paramsList.add(new BasicNameValuePair("website",            website.getText().toString()));
            paramsList.add(new BasicNameValuePair("notes",              edNotes.getText().toString()));
            paramsList.add(new BasicNameValuePair("isEdited",           String.valueOf(appPref.getInt("isEdited",0))));
            if (appPref.getInt("isEdited",0) == 1 ) {
                paramsList.add(new BasicNameValuePair("cr_id",  String.valueOf(appPref.getInt("cr_id",0))));
            }
            JSONArray hList = new JSONArray();

            if (null != appPref.getString("imagepathfrontURI", image)) {
                if (appPref.getInt("isEdited",0) != 1 ) {
                    if (1 == imgSave[0]) {
                        hList.put(getRealPathFromURI(Uri.parse(appPref.getString("imagepathfrontURI", image))));
                    }
                } else {
                    hList.put(appPref.getString("imagepathfrontURI", image));
                }
            }
            if (null != appPref.getString("imagepathbackURI", image)) {
                if (1 == imgSave[1])
                    hList.put(getRealPathFromURI(Uri.parse(appPref.getString("imagepathbackURI", image))));
            }

            if (0 != hList.length())
                paramsList.add(new BasicNameValuePair("imageUri", hList.toString()));

            jsonCr = jsonParser.makeHttpRequest(Urls.CreateCard , "POST", paramsList);

            if(null != jsonCr) {

                try {
                    if (jsonCr.getString("success").equalsIgnoreCase("1")) {
                        scan_count = 0;
                        appEdit.putInt("isEdited",0);
                        appEdit.apply();
                        return "success";

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else{
                return "failure";
            }
            return "failure";
        }
        protected void onPostExecute(String message) {
            if (message.equalsIgnoreCase("success")) {

                Fragment fragment = new HomeFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();
            } else {
                Toast.makeText(getActivity(),"Failed",Toast.LENGTH_LONG).show();
            }
            pDialog.dismiss();
            if (null != getActivity()) {
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        }

        public Bitmap scaleDown(Bitmap realImage,
                                boolean filter) {
            float actualHeight       = realImage.getHeight();
            float actualWidth        = realImage.getWidth();
            float maxHeight          = (float)600.00;
            float maxWidth           = (float)800.0;
            float imgRatio           = actualWidth/actualHeight;
            float maxRatio           = maxWidth/maxHeight;

            if (actualHeight > maxHeight || actualWidth > maxWidth){
                if(imgRatio < maxRatio){
                    //adjust width according to maxHeight
                    imgRatio     = maxHeight / actualHeight;
                    actualWidth  = imgRatio * actualWidth;
                    actualHeight = maxHeight;
                }
                else if(imgRatio > maxRatio){
                    //adjust height according to maxWidth
                    imgRatio     = maxWidth / actualWidth;
                    actualHeight = imgRatio * actualHeight;
                    actualWidth  = maxWidth;
                }
                else{
                    actualHeight = maxHeight;
                    actualWidth  = maxWidth;
                }
            }
            int width  = Math.round(actualWidth);
            int height = Math.round(actualHeight);
            Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                    height, filter);
            return newBitmap;
        }

        public int uploadFile(String sourceFileUri) {
            String fileName        = sourceFileUri;
            String upLoadServerUri = Urls.UploadCardImage;
            HttpURLConnection conn;
            DataOutputStream dos;
            String lineEnd         = "\r\n";
            String twoHyphens      = "--";
            String boundary        = "*****";
            File sourceFile        = new File(sourceFileUri);
            int  success = 0;

            try {

                URL url = new URL(upLoadServerUri);
                conn    = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setUseCaches(false);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("uploaded_file", fileName);

                dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\"" + fileName + "\"" + lineEnd);
                //-=----------
                dos.writeBytes(lineEnd);

                String path = sourceFile.getAbsolutePath();
                Bitmap myBitmap;

                if(!sourceFileUri.contains("content://")) {
                    Uri uri = Uri.fromFile(new File(sourceFileUri.toString()));
                    myBitmap = BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(uri));
                }
                else
                    myBitmap = BitmapFactory.decodeFile(path);

                Bitmap scaleBmp           = scaleDown(myBitmap, true);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                scaleBmp.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                byte[] data               = bos.toByteArray();

                dos.write(data, 0, data.length);

                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                serverResponseCode           = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();

                // Read response Json from urlConnection
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line = null;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line + '\n');
                }

                if(null != stringBuilder) {
                    JSONObject json = new JSONObject(stringBuilder.toString());
                    success = json.getInt("success");
                }

                Log.i("uploadFile", "HTTP Response is : " + serverResponseMessage + ": " + serverResponseCode);

                myBitmap.recycle();
                scaleBmp.recycle();
                bos.flush();
                bos.close();
                dos.flush();
                dos.close();
            } catch (MalformedURLException ex) {
                ex.printStackTrace();
                Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return success;
        }

    }

}