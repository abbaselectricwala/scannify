package app.com.scannify.fragments;

/**
 * Created by user on 4/27/2016.
 */

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.melnykov.fab.FloatingActionButton;
import com.scanlibrary.ScanActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.com.scannify.R;
import app.com.scannify.Utils.JSONParser;
import app.com.scannify.Utils.Urls;
import app.com.scannify.adapter.ContactAdapter;
import app.com.scannify.adapter.SpinnerAdapter;
import app.com.scannify.view.ProgressDialog;
import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by user on 4/22/2016.
 */
public class HomeFragment extends Fragment {

    public HomeFragment(){}


    SharedPreferences                   prefApp,pref,prefSettings;
    SharedPreferences.Editor            edit,editApp;
    private Spinner                     spnCat;
    private String[]                    name, email,address, phone,website,isBookmarked;
    ArrayList<JSONObject>               cardsJson = new ArrayList<JSONObject>();
    private  String[][]                 imagePath;
    private TextView                    emptyView;
    ImageView                           imageOverlap,imageError;
    private RecyclerView                recyclerView;
    private RecyclerView.LayoutManager  layoutManager;
    ProgressDialog                      pDialog         = null;
    ActionBar actionBar;

    //  JSONObject parent;
    CircularProgressView                progressView;
    Thread                              updateThread;
    ContactAdapter                      adapterContact;
    Bitmap                              bitmap;
    JSONParser                          jsonParserBr;
    JSONObject                          jsonBr,cards;
    static int                          back = 0;
    String                              msg;
    ListView                            contact_list;
    MaterialDialog                      mMaterialDialog;
    static int                          count = 0;
    boolean                             exit = false;
    int                                 flag;
    String                              categories[],arr_images[];
    int                                 category_id[];
    SpinnerAdapter spnAdapter;
    int[] id;
    Activity act;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        //getActivity().setTitle("Home");
        //context = this.getActivity().getApplicationContext();

        prefApp = getActivity().getSharedPreferences("AppPref", Context.MODE_PRIVATE);
        editApp = prefApp.edit();

        jsonParserBr = new JSONParser(getActivity());

        View rootView = inflater.inflate(R.layout.contact_list, container, false);

        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && KeyEvent.ACTION_DOWN == event.getAction()) {
                    HomeFragment fragment = new HomeFragment();
                    if (fragment.isVisible()) {
                    } else {
                        if (0 == count) {
                            count = 0;
                            if (true == exit)
                                getActivity().finish();
                            else {
                                Toast.makeText(getActivity(), "Press back again to exit...", Toast.LENGTH_SHORT).show();
                                exit = true;
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        exit = false;
                                    }
                                }, 3 * 1000);
                            }
                        } else {
                            count = 0;
                        }
                    }
                    return true;
                }
                return false;
            }
        });
        //*imgGPS        = (ImageButton)         rootView.findViewById(R.id.imgGPS);
        emptyView     = (TextView)            rootView.findViewById(R.id.empty_view);
        imageError    = (ImageView)           rootView.findViewById(R.id.imageError);
        progressView  = (CircularProgressView)rootView.findViewById(R.id.progress_view);
        imageOverlap  = (ImageView)       rootView.findViewById(R.id.overlapImage);

        recyclerView  = (RecyclerView)        rootView.findViewById(R.id.listBrowseBroadcast);

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        actionBar     = getActivity().getActionBar();
        actionBar.removeAllTabs();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

        //int catPos    = categories.length-1;//pref.getInt("CategoryPosition",90);

        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager         = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Intent i = new Intent(getActivity(),ScanActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);

            }
        });

        spnCat = (Spinner)rootView.findViewById(R.id.spnCategory);

        try {
            String str = prefApp.getString("Categories","Empty");
            if( !str.equalsIgnoreCase("Empty") ){
                JSONObject categoryObj = new JSONObject(str);
                categories = new String[categoryObj.length()+1];
                arr_images = new String[categoryObj.length()+1];
                category_id = new int[categoryObj.length()+1];
                JSONObject newObj;
                newObj = categoryObj.getJSONObject("c_1");
                categories[0] = "All";
                arr_images[0] = newObj.getString("image_path");
                for( int cnt = 0;cnt< categoryObj.length(); ++cnt){
                    newObj = categoryObj.getJSONObject("c_"+(cnt+1));

                    categories[cnt+1] = newObj.getString("title");
                    arr_images[cnt+1] = newObj.getString("image_path");
                    category_id[cnt+1] = newObj.getInt("category_id");

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(0 < categories.length){
            spnAdapter = new SpinnerAdapter(getActivity(), R.layout.row, categories,arr_images);
            spnCat.setAdapter(spnAdapter);
        }
        else{
            spnCat.setVisibility(View.GONE);
        }

        spnCat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("CommitPrefEdits")
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int pos = i;
                String cat = spnAdapter.getSelectedCategory(pos);
                new LoadMyCards().execute(cat);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        final GestureDetector mGestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {

            @Override public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        //ScannifyApplication.getInstance().trackScreenView("HomeFragment");
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.add("Menu 2a").setOnMenuItemClickListener(SearchButtonClickListener).setIcon(R.drawable.search_logo).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

    }
    MenuItem.OnMenuItemClickListener SearchButtonClickListener = new MenuItem.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            FragmentSearchCard fragmentSearch       = new FragmentSearchCard();
            FragmentManager fragmentManager         = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_container, fragmentSearch);
            fragmentTransaction.addToBackStack("myFrag");
            fragmentTransaction.commit();
            return true;
        }


    };

    class LoadMyCards extends AsyncTask<String, String, String> {
        int success = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(null != getActivity()) {
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
            pDialog = new ProgressDialog(getActivity(), getResources().getString(R.string.prog_msg_loading));
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            // TODO Auto-generated method stub
            String cat          = args[0];
            String username  = prefApp.getString("Username", "noUser");
            List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
            paramsList.add(new BasicNameValuePair("username",  username));
            paramsList.add(new BasicNameValuePair("category",  cat));

            jsonBr = jsonParserBr.makeHttpRequest(Urls.GetCardsDetails , "POST", paramsList);

            if(null != jsonBr) {
                try {
                    success = jsonBr.getInt("success");
                    if (1 == success) {

                        cards = jsonBr.getJSONObject("MyCards");

                        int noOfcrds = cards.length();
                        id = new int[noOfcrds] ;
                        name = new String[noOfcrds];
                        address    = new String[noOfcrds];
                        phone   = new String[noOfcrds];
                        email   = new String[noOfcrds];
                        isBookmarked   = new String[noOfcrds];
                        website   = new String[noOfcrds];
                        imagePath      = new String[noOfcrds][2];

                        for (int i = 0; i < noOfcrds; i++) {
                            int n = i + 1;
                            JSONObject myCards = cards.getJSONObject("card_" + n);
                            cardsJson.add(i,myCards);
                            id[i]           = myCards.getInt("id");
                            name[i]         = myCards.getString("contact_name");
                            address[i]      = myCards.getString("company_address");
                            phone[i]        = myCards.getString("contact_phone");
                            email[i]        = myCards.getString("contact_email");
                            isBookmarked[i]  = myCards.getString("isBookmarked");
                            website[i]       = myCards.getString("website");

                            if (myCards.getJSONArray("image").length() != 0) {
                                JSONArray imageArray = myCards.getJSONArray("image");
                                int arrayLength      = imageArray.length();
                                imagePath[i]      = new String[arrayLength];

                                for (int k = 0; k < arrayLength; k++) {
                                    String path = imageArray.get(k).toString();
                                    try {
                                        imagePath[i][k] = path;
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                        adapterContact = new ContactAdapter(getActivity(), name, email, imagePath, phone, website,isBookmarked,cardsJson,id,isBookmarked);
                        return "success";
                    }

                } catch (JSONException x) {
                    x.printStackTrace();
                }
            }
            else{
                return "failure";
            }
            return "failure";
        }
        protected void onPostExecute(String message) {
            if(1 == success){
                recyclerView.setAdapter(adapterContact);
                recyclerView.setVisibility(View.VISIBLE);
                emptyView.setVisibility(View.GONE);
                imageError.setVisibility(View.GONE);

            }
            else  if (message.equalsIgnoreCase("failure")) {
                recyclerView.setVisibility(View.GONE);
                emptyView.setVisibility(View.VISIBLE);
                imageError.setVisibility(View.VISIBLE);
            } else {
                if (0 == success) {
                    recyclerView.setVisibility(View.GONE);
                    emptyView.setVisibility(View.VISIBLE);
                    imageError.setVisibility(View.VISIBLE);
                }
            }
            pDialog.dismiss();
            if (null != getActivity()) {
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        }
    }
}