package app.com.scannify.fragments;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import app.com.scannify.R;
import app.com.scannify.Utils.Utility;
import app.com.scannify.view.Button;

import me.drakeet.materialdialog.MaterialDialog;

public class About extends Fragment {

    public About(){}

    View            rootView;
    MaterialDialog  mMaterialDialog;
    Button          btnTerms;
    ActionBar actionBar;
    TextView        textAbout,tvVersion;
    String          versionName,vCode,finalVersion;
    int             versionCode;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {

        rootView = inflater.inflate(R.layout.fragment_about, container, false);
        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && KeyEvent.ACTION_DOWN == event.getAction()) {
                    About fragment = new About();
                    //Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.frame_container);
                    if (fragment.isVisible()) {
                    } else {
                        HomeFragment.count              = 1;
                        HomeFragment fragmenthome       = new HomeFragment();
                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.frame_container, fragmenthome).commit();
                    }
                    return true;
                }
                return false;
            }
        });

        versionCode = Utility.getVersionCode(getActivity());
        versionName = Utility.getVersionName(getActivity());

        vCode = String.valueOf(versionCode);

        finalVersion = versionName +"."+ vCode;

        actionBar = getActivity().getActionBar();
        actionBar.removeAllTabs();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        tvVersion = (TextView)rootView.findViewById(R.id.tvVersion);
        textAbout =(TextView)rootView.findViewById(R.id.textabout);
        textAbout.setText(Html.fromHtml(getString(R.string.about)));

        tvVersion.setText("Version "+finalVersion);

        btnTerms = (Button)rootView.findViewById(R.id.btnTerms);
        btnTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View v = inflater.inflate(R.layout.terms_of_use,null);
                TextView textView =(TextView)v.findViewById(R.id.textTerms);
                textView.setClickable(true);
                textView.setMovementMethod(LinkMovementMethod.getInstance());
                String text = "<a href='http://www.radii.mobi/Terms%20&%20Condition.html'> Terms of use and Privacy Policy </a>";
                textView.setText(Html.fromHtml(text));
                mMaterialDialog = new MaterialDialog(getActivity())
                        .setContentView(v)
                        .setPositiveButton(getResources().getString(R.string.prog_msg_OK), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mMaterialDialog.dismiss();
                               }
                        });
                mMaterialDialog.show();
            }
        });
        return rootView;
    }
    @Override
    public void onResume() {
        super.onResume();
        //ScannifyApplication.getInstance().trackScreenView("About");
    }
}