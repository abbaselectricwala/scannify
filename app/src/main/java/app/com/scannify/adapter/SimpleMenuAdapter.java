package app.com.scannify.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import app.com.scannify.R;


public class SimpleMenuAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;

    public SimpleMenuAdapter(Context context) {
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        View view = convertView;

        if (view == null) {

            view                 = layoutInflater.inflate(R.layout.simple_list_item, parent, false);
            viewHolder           = new ViewHolder();
            viewHolder.textView  = (TextView) view.findViewById(R.id.text_view);
            viewHolder.imageView = (ImageView) view.findViewById(R.id.image_view);
            view.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        Context context = parent.getContext();

        switch (position) {
            case 0:
                viewHolder.textView.setText(context.getResources().getString(R.string.prog_msg_edit));
                viewHolder.imageView.setImageResource(R.drawable.ic_edit_broadcast);
                break;
            case 1:
                viewHolder.textView.setText(context.getResources().getString(R.string.prog_msg_delete));
                viewHolder.imageView.setImageResource(R.drawable.ic_delete_broadcast);
                break;
        }
        return view;
    }
    static class ViewHolder {
        TextView textView;
        ImageView imageView;
    }
}