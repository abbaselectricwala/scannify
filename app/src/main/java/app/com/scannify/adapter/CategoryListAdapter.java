package app.com.scannify.adapter;

import android.app.Activity;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import app.com.scannify.R;

/**
 * Created by logituit-3 on 5/11/2016.
 */

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.ViewHolder> {
    private Activity context;
    private String[] categoryTitle, image_path;
    private int[] categoryId;

    public CategoryListAdapter(Activity context, String[] categoryTitle, String[] image_path, int[] categoryId) {
        this.context = context;
        this.categoryTitle = categoryTitle;
        this.image_path = image_path;
        this.categoryId=categoryId;

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_list_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        holder.setItem(context,categoryTitle,image_path);
    }

    @Override
    public int getItemCount() {
        return categoryTitle.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        TextView txtTitle, txtDesc, txtReqPending, txtCount;
        private ImageView[] imageView = new ImageView[5];
        private ImageView imgBookmarked;
        ImageView imageView_round;
        private Activity context;
        HorizontalScrollView hrScroll;
        SharedPreferences prefApp;
        SharedPreferences.Editor editApp;
        int myBroadcast = 0;

        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);

            txtTitle = (TextView) itemView.findViewById(R.id.txtContactName);
            imageView_round=(ImageView) itemView.findViewById(R.id.imageView_round);

        }




        public void setItem(Activity con, String[] titles, String[] arr_images) {
            int position = getPosition();
            context = con;


            txtTitle.setText(titles[position]);
            Picasso.with(context).load(arr_images[position]).into(imageView_round);





        }

        @Override
        public void onClick(View view) {
            Log.d("OnClickRecyclerView", "onClick " + getPosition());

        }
    }
}
