package app.com.scannify.adapter;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.com.scannify.R;
import app.com.scannify.Utils.JSONParser;
import app.com.scannify.Utils.Urls;
import app.com.scannify.activity.HomeActivity;
import app.com.scannify.fragments.Fragment_Detail;


public class BookmarkAdapter extends RecyclerView.Adapter<BookmarkAdapter.ViewHolder> {

    public Activity context;
    private  String[]       Name,Email,Phone,Website,Bookmarked;
    private String[][]     imagePath;
    ArrayList<JSONObject> cardsJson = new ArrayList<JSONObject>();
    SharedPreferences prefApp;
    SharedPreferences.Editor editApp;

    public BookmarkAdapter(Activity context,String Name[], String Email[],String imagePath[][], String Phone[], String Website[],String Bookmarked[],ArrayList<JSONObject> cardsJson){
        this.context       = context;
        this.Name = Name;
        this.Email   =  Email;
        this.imagePath     = imagePath;
        this.Phone       = Phone;
        this.Website    = Website;
        this.Bookmarked    = Bookmarked;
        this.cardsJson = cardsJson;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view             = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_bookmark_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(context, Name, Email,  Bookmarked, imagePath,cardsJson);

    }

    @Override
    public int getItemCount() {
        return Name.length;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
             {

        TextView txtTitle,txtDesc;
        private ImageView[] imageView = new ImageView[2];
        private ImageView   imgBookmarked;
        private ImageView icCall, ivEmail;
        //Context con  = itemView.getContext();
        JSONObject card;
                 int flag = 0;
                 int id=0;
        SharedPreferences prefApp;
        SharedPreferences.Editor editApp;
        ArrayList<JSONObject> cardsObj = new ArrayList<JSONObject>();

        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            // con  = itemView.getContext();;

            txtTitle = (TextView) itemView.findViewById(R.id.txtName);
            txtDesc  = (TextView) itemView.findViewById(R.id.txtTitle);

            imageView[0]  = (ImageView) itemView.findViewById(R.id.imgviewfront);
            imageView[1]  = (ImageView) itemView.findViewById(R.id.ivBack);

            imgBookmarked = (ImageView) itemView.findViewById(R.id.bookmark);


            icCall = (ImageView) itemView.findViewById(R.id.icCall);
            ivEmail = (ImageView) itemView.findViewById(R.id.ivEmail);


        }



        public void setItem(Activity context, String Name[], String Email[], String Bookmarked[],String imagePath[][],ArrayList<JSONObject> cardsJson) {
            int position = getPosition();
            context = context;
            cardsObj = cardsJson;
            card = cardsJson.get(position);

            prefApp = context.getSharedPreferences("AppPref", Context.MODE_PRIVATE);
            editApp = prefApp.edit();


            txtTitle.setText( Name[position]);
            txtDesc.setText(Email[position]);

            if(Bookmarked[position].equalsIgnoreCase("0"))
                imgBookmarked.setImageResource(R.drawable.ic_bookmark_inactive_browsebroadcasts);
            else
                imgBookmarked.setImageResource(R.drawable.ic_bookmark_active);



            if(1 == imagePath[position].length){
                imageView[1].setVisibility(View.GONE);
                Picasso.with(context).load(imagePath[position][0]).into(imageView[0]);
            }
            else if(2 == imagePath[position].length){
                imageView[1].setVisibility(View.VISIBLE);
                Picasso.with(context).load(imagePath[position][0]).into(imageView[0]);
                Picasso.with(context).load(imagePath[position][1]).into(imageView[1]);
            }
            icCall.setOnClickListener(this);
            imgBookmarked.setOnClickListener(this);
            ivEmail.setOnClickListener(this);
            imageView[0].setOnClickListener(this);
            imageView[1].setOnClickListener(this);

           /* icCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("call", "call");
                    String phoneNo = null;
                    try {
                        phoneNo = card.getString("contact_phone");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (!phoneNo.equalsIgnoreCase("")) {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:" + phoneNo));
                        con.startActivity(intent);
                    }
                }

            });*/

           /* icMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String latitude = "18.5204";
                    String longitude = " 73.8567";
                    String uri = String.format(Locale.ENGLISH, "geo:%s,%s", latitude, longitude);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    con.startActivity(intent);
                }
            });*/
        }

                 @Override
                 public void onClick(View v) {
                     if(v.getId() == icCall.getId()){
                         Log.d("call", "call");
                         String phoneNo = null;
                         try {
                             phoneNo = card.getString("contact_phone");
                         } catch (JSONException e) {
                             e.printStackTrace();
                         }
                         if (!phoneNo.equalsIgnoreCase("")) {
                             Intent intent = new Intent(Intent.ACTION_DIAL);
                             intent.setData(Uri.parse("tel:" +phoneNo));
                             context.startActivity(intent);
                         }
                     } else if (v.getId() == imgBookmarked.getId()){

                         switch (flag) {
                             case 0:
                                 imgBookmarked.setImageResource(R.drawable.ic_bookmark_active);
                                 flag = 1;
                                 break;
                             case 1:
                                 imgBookmarked.setImageResource(R.drawable.ic_bookmark_inactive_browsebroadcasts);
                                 flag = 0;
                                 break;
                         }
                    new AddBookmarked(flag, id).execute();

                     } else if(v.getId() == ivEmail.getId()){
                         String email = null;
                         try {
                             email = card.getString("contact_email");
                         } catch (JSONException e) {
                             e.printStackTrace();
                         }
                         Intent intent = new Intent(Intent.ACTION_SEND);
                         intent.setType("vnd.android.cursor.item/email");
                         intent.putExtra(Intent.EXTRA_EMAIL, email);
                         context.startActivity(Intent.createChooser(intent, "Send Email"));
                     } else if (v.getId() != ivEmail.getId() || v.getId() != imgBookmarked.getId() || v.getId() != icCall.getId() || v.getId() == imageView[0].getId() || v.getId() == imageView[1].getId() ){

                         int pos = getPosition();
                         int k   = 0;

                         for (int i = 0; i < card.length(); i++) {
                             int n = i + 1;
                             //JSONObject myCards = card.getJSONObject("card_" + n);
                             if (k == pos) {
                                 editApp.putInt("cardPosition",pos+1);
                                 editApp.putString("selectedCardObject",card.toString());
                                 editApp.putString("allCardsObjects",card.toString());
                             }k++;
                         }

                         editApp.commit();
                         FragmentManager fragmentManager         = ((HomeActivity) context).getFragmentManager();
                         FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                         Fragment_Detail showContacts             = new Fragment_Detail();
                         Bundle args                             = new Bundle();
                         String finalPageView                  = "BookmarkList";
                         args.putString("finalbroadcast", finalPageView);
                         showContacts.setArguments(args);
                         fragmentTransaction.replace(R.id.frame_container, showContacts);
                         fragmentTransaction.addToBackStack("myFrag");
                         fragmentTransaction.commit();
                        /* Log.d("OnClickRecyclerView", "onClick " + getPosition());
                         Log.d("OnClickRecyclerView2", "onClick " + position);
                         int pos = position;
                         int k   = 0;

                         for (int i = 0; i < card.length(); i++) {
                             int n = i + 1;
                             //JSONObject myCards = card.getJSONObject("card_" + n);
                             if (k == pos) {
                                 editApp.putInt("cardPosition",pos+1);
                                 editApp.putString("selectedCardObject",card.toString());
                                 editApp.putString("allCardsObjects",card.toString());
                             }k++;
                         }

                         editApp.commit();
                         FragmentManager fragmentManager         = ((HomeActivity) context).getSupportFragmentManager();
                         FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                         Fragment_Detail showContacts             = new Fragment_Detail();
                         Bundle args                             = new Bundle();
                         String finalbroadcasts                  = "CategoryShow";
                         args.putString("finalbroadcast", finalbroadcasts);
                         //args.putString("cardsjsonObject",)
                         showContacts.setArguments(args);
                         fragmentTransaction.replace(R.id.frame_container, showContacts);
                         fragmentTransaction.addToBackStack("myFrag");
                         fragmentTransaction.commit();*/

                     }
                 }


       /* @Override
        public void onClick(View view) {

            editApp.putInt("cardPosition", getPosition() + 1);
            editApp.putString("selectedCardObject", card.toString());
            editApp.putString("allCardsObjects", cardsObj.toString());
            editApp.commit();
            FragmentManager fragmentManager = ((HomeActivity) con).getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            Fragment_Detail showContacts = new Fragment_Detail();
            Bundle args = new Bundle();
            String finalbroadcasts = "CategoryShow";
            args.putString("finalbroadcast", finalbroadcasts);
            //args.putString("cardsjsonObject",)
            showContacts.setArguments(args);
            fragmentTransaction.replace(R.id.frame_container, showContacts);
            fragmentTransaction.addToBackStack("myFrag");
            fragmentTransaction.commit();

        }*/

    }

    public  class AddBookmarked extends AsyncTask<String, String, String> {
        int       flag, crId;
        //  Context    con = context;
        JSONObject json;
        JSONParser jsonParser = new JSONParser(context);

        public AddBookmarked(int flag, int crId){

            if(flag == 0)
                this.flag = 2;
            else
                this.flag = flag;
            this.crId = crId;
            // con = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... args) {
            // TODO Auto-generated method stub
            // here Check for success tag
            String username = prefApp.getString("Username","nouser");
            List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
            paramsList.add(new BasicNameValuePair("username",   username));
            paramsList.add(new BasicNameValuePair("flag",       String.valueOf(flag)));
            paramsList.add(new BasicNameValuePair("crId",      String.valueOf(crId)));

            json = jsonParser.makeHttpRequest(Urls.AddBookmarked, "POST", paramsList);
            String msg = "Failed";

            if(null != json) {
                try {
                    msg = json.getString("message");
                    return msg;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return msg;
        }

        protected void onPostExecute(String message) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }
}

