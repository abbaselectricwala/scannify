package app.com.scannify.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import app.com.scannify.R;


public class SpinnerAdapter extends BaseAdapter {
    Context c;
    int textViewResourceId;
    String[] categoryTitle;
    String[] arr_images;
    public SpinnerAdapter(Context context, int textViewResourceId,   String[] categoryTitle,String[] arr_images) {
        //super(context, textViewResourceId, objects);
        this.c = context;
        this.textViewResourceId = textViewResourceId;
        this.categoryTitle =categoryTitle;
        this.arr_images =arr_images;
    }

    @Override
    public int getCount() {
        return categoryTitle.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (null == view) {
            LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view                    = (RelativeLayout) inflater.inflate(R.layout.row, parent, false);
        }
        TextView label          = (TextView)view.findViewById(R.id.company);
        ImageView icon          = (ImageView)view.findViewById(R.id.image);
        label.setText(categoryTitle[position]);
        Picasso.with(c).load(arr_images[position]).into(icon);
        return view;
    }

    public String getSelectedCategory(int pos){
        return categoryTitle[pos];
    }

    /*@Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View row                = inflater.inflate(R.layout.row, parent, false);
        TextView label          = (TextView)row.findViewById(R.id.company);
        ImageView icon          = (ImageView)row.findViewById(R.id.image);
        label.setText(categories[position]);
        //Picasso.with(c).load(arr_images[position]).into(icon);
        return row;
    }*/
}

