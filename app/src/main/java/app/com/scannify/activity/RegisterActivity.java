package app.com.scannify.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.com.scannify.R;
import app.com.scannify.Utils.ApiCrypter;
import app.com.scannify.Utils.JSONParser;
import app.com.scannify.Utils.NetworkUtil;
import app.com.scannify.Utils.Urls;
import app.com.scannify.Utils.Utility;
import app.com.scannify.view.ButtonRectangle;
import app.com.scannify.view.ProgressDialog;
import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by user on 4/18/2016.
 */
public class RegisterActivity  extends Activity{

    public ButtonRectangle btnSubmit      = null;
    public EditText name         = null;
    public EditText email        = null;
    public EditText countryCode  = null;
    public EditText number       = null;
    public EditText password     = null;
    public EditText location     = null;
    public EditText confPwd      = null;
    private JSONParser jsonParser;
    boolean isName,isPhone,matches;
    String                      status;
    String Phone                 = null;
    String                      Name,Email,Password,Location,confirm,Country,Number;
    MaterialDialog mMaterialDialog;
    private ProgressDialog      pDialog;
    String encrypt_pwd           = null;
    JSONObject json;
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    private static final String IND_REGISTER_URL = Urls.RegisterIndividul;








    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
       // ScannifyApplication.getInstance().trackScreenView("RegisterActivity");
        jsonParser = new JSONParser(getApplicationContext());
        name = (EditText) findViewById(R.id.edName);
        email = (EditText) findViewById(R.id.edEmail);
        countryCode = (EditText) findViewById(R.id.edPhone);
        number = (EditText) findViewById(R.id.edNumber);
        password = (EditText) findViewById(R.id.edPassword);
        confPwd = (EditText) findViewById(R.id.edConfirm);
        btnSubmit = (ButtonRectangle) findViewById(R.id.btnRegister);

        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            public void afterTextChanged(Editable edt) {
                if (name.getText().length() > 0) {
                    name.setError(null);
                }
            }
        });

        name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if(name.getText().length()>0) {
                        isName = isValidName(name.getText().toString());
                        if (!isName) {
                            name.setError(getResources().getString(R.string.prog_msg_valid_name_error));
                        }
                    }
                }
            }
        });

        number.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                    if(number.getText().length()>0) {
                        isPhone = isValidPhone(number.getText().toString());
                        if (!isPhone) {
                            number.setError(getResources().getString(R.string.prog_msg_phone_limit_error));
                        }
                    }
                }
            }
        });

        email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if(email.getText().length()>0) {
                        matches = isValidMail(email.getText().toString());
                        if (!matches) {
                            email.setError(getResources().getString(R.string.prog_msg_proper_email));
                        }
                    }
                }
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                status = NetworkUtil.getConnectivityStatusString(getApplicationContext());

                if ((status.equalsIgnoreCase(getResources().getString(R.string.prog_msg_wifi_enabled)) || status.equalsIgnoreCase(getResources().getString(R.string.prog_msg_network_enabled)))) {

                    String pass = password.getText().toString();
                    Phone = "";
                    Name = name.getText().toString();
                    Country = countryCode.getText().toString();
                    Number = number.getText().toString();
                    Password = password.getText().toString();
                    Email = email.getText().toString().trim();
                    confirm = confPwd.getText().toString();

                    if(email.getText().length()>0) {
                        matches = isValidMail(email.getText().toString());
                        if (!matches) {
                            email.setError(getResources().getString(R.string.prog_msg_proper_email));
                        }
                    }

                    if(number.getText().length()>0) {
                        isPhone = isValidPhone(number.getText().toString());
                        if (!isPhone) {
                            number.setError(getResources().getString(R.string.prog_msg_phone_limit_error));
                        }
                    }

                    if(name.getText().length()>0) {
                        isName = isValidName(name.getText().toString());
                        if (!isName) {
                            name.setError(getResources().getString(R.string.prog_msg_valid_name_error));
                        }
                    }


                    if (Country.isEmpty() || Number.isEmpty()) {
                        if (Country.isEmpty()) {
                            countryCode.setError(getResources().getString(R.string.prog_msg_country_error));
                        }
                        if (Number.isEmpty()) {
                            number.setError(getResources().getString(R.string.prog_msg_phone_error));
                        }
                    } else {
                        Phone = Country + Number;
                    }

                    if (Name.isEmpty() || Email.isEmpty() || Phone.isEmpty() || Password.isEmpty() || confirm.isEmpty()) {
                        if (Name.isEmpty()) {
                            name.setError(getResources().getString(R.string.prog_msg_name_error));
                        }
                        if (Password.isEmpty()) {
                            password.setError(getResources().getString(R.string.prog_msg_password_error));
                        }
                        if (confirm.isEmpty()) {
                            confPwd.setError(getResources().getString(R.string.prog_msg_confirm_password_error));
                        }
                        if (Phone.isEmpty()) {
                            number.setError(getResources().getString(R.string.prog_msg_phone_error));
                        }
                        if (Email.isEmpty()) {
                            email.setError(getResources().getString(R.string.prog_msg_proper_email));
                        }
                    } else {
                        if (matches && isPhone && isName ) {
                            if (pass != null && pass.length() >= 6 && pass.length() <= 20) {
                                if (pass.equals(confirm)) {
                                    LayoutInflater inflater = getLayoutInflater();
                                    View v = inflater.inflate(R.layout.terms_of_use, null);
                                    TextView textView = (TextView) v.findViewById(R.id.textTerms);
                                    textView.setClickable(true);
                                    textView.setMovementMethod(LinkMovementMethod.getInstance());
                                    String text = "<a href='http://www.radii.mobi/Terms%20&%20Condition.html'> Terms of use and Privacy Policy </a>";
                                    textView.setText(Html.fromHtml(text));
                                    mMaterialDialog = new MaterialDialog(RegisterActivity.this)
                                            .setContentView(v)
                                            .setPositiveButton(getResources().getString(R.string.prog_msg_ACCEPT), new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    mMaterialDialog.dismiss();
                                                    //getRegId();
                                                    new AttemptRegister().execute();
                                                }
                                            })
                                            .setNegativeButton(getResources().getString(R.string.prog_msg_DECLINE), new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    mMaterialDialog.dismiss();
                                                }
                                            });

                                    mMaterialDialog.show();
                                } else {
                                    confPwd.setError(getResources().getString(R.string.prog_msg_confirm_error));
                                }
                            } else {
                                password.setError(getResources().getString(R.string.prog_msg_limit_error));
                            }
                        }
                    }
                }
            }
        });






    }

    private boolean isValidName(String name)
    {
        boolean check;
        Pattern p;
        Matcher m;

        String NAME_STRING = "[a-zA-Z\\s]+";

        p = Pattern.compile(NAME_STRING);

        m = p.matcher(name);
        check = m.matches();
        return check;
    }

    private boolean isValidPhone(String phone)
    {
        boolean check;
        Pattern p;
        Matcher m;

        String PHONE_STRING = "^[ ()+]*([0-9][ ()+]*){10,13}$";

        p = Pattern.compile(PHONE_STRING);

        m = p.matcher(phone);
        check = m.matches();
        return check;
    }

    private boolean isValidMail(String email2)
    {
        boolean check;
        Pattern p;
        Matcher m;

        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        p = Pattern.compile(EMAIL_STRING);

        m = p.matcher(email2);
        check = m.matches();
        return check;
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    class AttemptRegister extends AsyncTask<String, String, String> {
        int success;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(RegisterActivity.this,getResources().getString(R.string.prog_msg_Register));
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            // TODO Auto-generated method stub

            String phkey = Utility.makeDigest(Email);

            ApiCrypter crypter = new ApiCrypter(phkey);
            try {
                encrypt_pwd = ApiCrypter.bytesToHex(crypter.encrypt(Password));
            }
            catch (Exception e) {
                e.printStackTrace();
            }

            try {
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("userfullname", Name));
                params.add(new BasicNameValuePair("password", encrypt_pwd));
                params.add(new BasicNameValuePair("email", Email));
                params.add(new BasicNameValuePair("number", Number));
                params.add(new BasicNameValuePair("country", Country));

                Log.d("request!", "starting");

                json = jsonParser.makeHttpRequest(
                        IND_REGISTER_URL, "POST", params);

                if (null != json) {

                    success = json.getInt(TAG_SUCCESS);

                    if (success == 1) {
                        return json.getString(TAG_MESSAGE);
                    }else{
                        return json.getString(TAG_MESSAGE);
                    }
                } else {
                    return "failed";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String message) {
            if(null!=message) {
                if (message.equalsIgnoreCase("failed")) {
                    Toast.makeText(getApplicationContext(), "Network Failure", Toast.LENGTH_LONG).show();
                }
                else if (success == 1) {
                    Log.d("Successfully Register!", json.toString());
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    Intent ii = new Intent(RegisterActivity.this, LoginActivity.class);
                    finish();
                    startActivity(ii);
                }
                else if(success == 0)
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

            }

            pDialog.dismiss();
        }
    }
}
