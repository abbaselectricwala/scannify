package app.com.scannify.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.Settings;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.com.scannify.R;
import app.com.scannify.Utils.JSONParser;
import app.com.scannify.Utils.NetworkUtil;
import app.com.scannify.Utils.Urls;
import app.com.scannify.Utils.Utility;
import me.drakeet.materialdialog.MaterialDialog;

public class SplashScreen extends Activity {

    private Thread              mSplashThread;
    SharedPreferences           pref,prefMessage;
    SharedPreferences.Editor    edit;
    Intent                      startAct,intent;
    String                      username,isFacebookLogin,versionName,vCode,finalVersion;
    JSONObject                  json;
    int                         success;
    MaterialDialog              mMaterialDialog;
    int                         flagStuck = 1,versionCode,recommended_version_flag;

    private static final String LOGIN_URL   = Urls.Login;

    private static final String TAG_SUCCESS = "success";
    JSONParser jsonParser;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash_screen);
        //ScannifyApplication.getInstance().trackScreenView("Splash");
        jsonParser  = new JSONParser(getApplicationContext());

        versionName = Utility.getVersionName(getApplicationContext());
        versionCode = Utility.getVersionCode(getApplicationContext());
        vCode = String.valueOf(versionCode);

        finalVersion = versionName +"."+ vCode;




        String status =  NetworkUtil.getConnectivityStatusString(getApplicationContext());
        pref = getSharedPreferences("AppPref", MODE_PRIVATE);
        edit = pref.edit();
        edit.putString("isOnChat","NO");
        edit.commit();
        prefMessage = getSharedPreferences("MessagePreference", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefMessage.edit();
        editor.clear();
        editor.commit();
        if (status.equalsIgnoreCase(getResources().getString(R.string.prog_msg_wifi_enabled)) || status.equalsIgnoreCase(getResources().getString(R.string.prog_msg_network_enabled))) {

           /* mSplashThread = new Thread() {
                @Override
                public void run() {
                    Looper.prepare();
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // Do Work
                            new PrefetchData().execute();
                            //.removeCallbacks(this);
                           // Looper.myLooper().quit();
                        }
                    }, 2000);

                    Looper.loop();
                    *//*try {
                        synchronized (this) {
                            wait(5000);
                        }
                    } catch (InterruptedException ex) {
                    }
                    new PrefetchData().execute();*//*
                }
            } ;
            mSplashThread.start();*/

           /* SplashScreen.this.runOnUiThread(new Runnable() {
                public void run() {
                    new PrefetchData().execute();
                }
            });*/
            mSplashThread = new Thread() {
                @Override
                public void run() {
                    try {
                        synchronized (this) {
                            wait(2000);
                        }
                    } catch (InterruptedException ex) {
                    }
                    new PrefetchData().execute();
                }
            } ;
            mSplashThread.start();
        }
        else {
            showNetworkDisabledAlertToUser();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        super.onCreateOptionsMenu(menu);
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onTouchEvent(MotionEvent evt) {
        if (evt.getAction() == MotionEvent.ACTION_DOWN) {
            if (null != mSplashThread) {
                synchronized (mSplashThread) {
                    mSplashThread.notifyAll();
                }
            }
        }
        return true;
    }

    private class PrefetchData extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg0) {

            username        = pref.getString("Username", "noUser");
            isFacebookLogin = pref.getString("facebookLogin", "0");


            Log.d("Username", username);

            if(!(username.equals("noUser"))) {
                //sendLoginRequest();

                try {
                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                    params.add(new BasicNameValuePair("username", pref.getString("Username", null)));
                    params.add(new BasicNameValuePair("password",pref.getString("Password", null)));
                    params.add(new BasicNameValuePair("isFacebookLogin", isFacebookLogin));
                    params.add(new BasicNameValuePair("versioncode",     finalVersion));

                    Log.d("request!", "starting");

                    json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);

                    if(null!=json) {

                        Log.d("Login attempt", json.toString());

                        success = json.getInt(TAG_SUCCESS);
                        recommended_version_flag = 0;//json.getInt("recommended_version_flag");

                        if (success == 1) {
                            Log.d("Successfully Login!", json.toString());
                            startAct = new Intent(getApplicationContext(), HomeActivity.class);
                            return "Success";
                        }
                    }
                    else {
                        return "failure";
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else
                startAct =  new Intent(SplashScreen.this, LoginActivity.class);
            return "Success";
        }

        @Override
        protected void onPostExecute(String message) {
            if (message.equalsIgnoreCase("failure")) {
                showNetworkDisabledAlertToUser();
            } else if(recommended_version_flag == 1) {
                showVersionUpdateAlert();
            } else {
                if (null != startAct) {
                    startAct.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(startAct);
                }
                finish();
            }
        }

    }
    private void showVersionUpdateAlert()
    {
        mMaterialDialog = new MaterialDialog(SplashScreen.this)
                .setTitle("Update Required")
                .setMessage("Please update to a newer version to continue using the app")
                .setPositiveButton("UPDATE", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.app.radii"));
                        startActivity(intent);
                        mMaterialDialog.dismiss();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.prog_msg_CANCEL), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                        finish();
                    }
                });

        mMaterialDialog.show();
    }
    private void showNetworkDisabledAlertToUser() {
        mMaterialDialog = new MaterialDialog(SplashScreen.this)
                .setTitle(getResources().getString(R.string.prog_msg_network_connection))
                .setMessage(getResources().getString(R.string.prog_msg_network_on))
                .setPositiveButton(getResources().getString(R.string.prog_msg_OK), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        flagStuck = 0;
                        int conn = NetworkUtil.getConnectivityStatus(getApplicationContext());
                        if (conn == NetworkUtil.TYPE_NOT_CONNECTED) {
                            Intent callSettingIntent = new Intent(Settings.ACTION_SETTINGS);
                            startActivity(callSettingIntent);
                            finish();
                        }
                        mMaterialDialog.dismiss();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.prog_msg_CANCEL), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        flagStuck = 0;
                        mMaterialDialog.dismiss();
                        finish();
                    }
                })
                .setCanceledOnTouchOutside(false)
                        // You can change the message anytime.
                        // mMaterialDialog.setTitle("提示");
                .setOnDismissListener(
                        new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                if(flagStuck == 1) {
                                    finish();
                                    mMaterialDialog.dismiss();
                                }
                            }
                        });
        mMaterialDialog.show();
    }
}
