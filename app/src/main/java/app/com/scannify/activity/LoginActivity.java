package app.com.scannify.activity;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import app.com.scannify.R;
import app.com.scannify.Utils.ConnectionDetector;
import app.com.scannify.Utils.JSONParser;
import app.com.scannify.Utils.NetworkUtil;
import app.com.scannify.Utils.Urls;
import app.com.scannify.Utils.Utility;
import app.com.scannify.view.ButtonRectangle;
import app.com.scannify.view.ProgressDialog;
import me.drakeet.materialdialog.MaterialDialog;

import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends Activity {

    public EditText             edUserName;
    public EditText             edPassword;
    public ButtonRectangle      btnSignin;
    public ButtonRectangle      btnRegister;
    private TextView            tvForgot          = null;
    private String              emailPattern,Message,model,version,vname,ipaddress,versionName,vCode,finalVersion;
    int                         success,versionCode,recommended_version_flag;
    JSONParser                  jsonParser ;
    MaterialDialog              mMaterialDialog;
    ConnectionDetector          cd;
    Boolean                     isInternetPresent = false;
    ProgressDialog              pDialog;
    private String              username          = null;
    private String              password          = null;
    private String              profilePath       = null;
    boolean                     isLoggedIn = false;
    private static final String TAG_SUCCESS  = "success";
    private static final String TAG_MESSAGE  = "message";
    SharedPreferences           pref;
    SharedPreferences.Editor    edit;
    JSONObject json;
    private Bitmap              bitmap            = null;




    public String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        }
        catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
       // ScannifyApplication.getInstance().trackScreenView("Login");
        edUserName = (EditText) findViewById(R.id.edUserName);
        edPassword = (EditText) findViewById(R.id.edPassword);
        tvForgot  = (TextView)      findViewById(R.id.tvForgot);
        btnSignin = (ButtonRectangle) findViewById(R.id.btnSignIn);

        pref  = getSharedPreferences("AppPref", MODE_PRIVATE);
        edit  = pref.edit();

        vname       = Utility.getVersionName();
        model       = Utility.getModel();
        version     = Utility.getVersion();
        ipaddress   = Utility.getLocalIpAddress();
        versionCode = Utility.getVersionCode(getApplicationContext());
        versionName = Utility.getVersionName(getApplicationContext());

        vCode = String.valueOf(versionCode);

        finalVersion = versionName +"."+ vCode;

        jsonParser       = new JSONParser(getApplicationContext());

        edUserName.requestFocus();
        edUserName.setText("");
        edPassword.setText("");


        View v = this.findViewById(android.R.id.content).getRootView();

        v.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideSoftKeyboard(LoginActivity.this);
                return false;
            }
        });

        // Email Validation...
        emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        cd = new ConnectionDetector(getApplicationContext());


        tvForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username = edUserName.getText().toString();
                tvForgot.setClickable(false);
                if(edUserName.getText().toString().equalsIgnoreCase("") || !edUserName.getText().toString().matches(emailPattern)) {
                    mMaterialDialog = new MaterialDialog(LoginActivity.this)
                            .setMessage(getResources().getString(R.string.prog_msg_valid_email))
                            .setPositiveButton(getResources().getString(R.string.prog_msg_OK), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mMaterialDialog.dismiss();
                                    tvForgot.setClickable(true);
                                }
                            })
                            .setNegativeButton(getResources().getString(R.string.prog_msg_CANCEL), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mMaterialDialog.dismiss();
                                    tvForgot.setClickable(true);
                                }
                            });
                    mMaterialDialog.show();
                }
                else {
                    new LoadResetPass().execute();
                }
            }
        });




        btnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isInternetPresent = cd.isConnectingToInternet();
                username = edUserName.getText().toString();
                password = edPassword.getText().toString();
                switch (v.getId()) {
                    case R.id.btnSignIn:
                        if (isInternetPresent) {
                            // Internet Connection is Present
                            if (edUserName.getText().toString().equalsIgnoreCase("")) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.prog_msg_enter_email), Toast.LENGTH_LONG).show();
                            }
                            else if( edPassword.getText().toString().equalsIgnoreCase("")) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.prog_msg_enter_password), Toast.LENGTH_LONG).show();
                            }
                            else {
                                new AttemptLogin().execute();
                            }
                        } else {

                            showNetworkDisabledAlertToUser();
                        }
                    default:
                        break;
                }

                  }
        });

        btnRegister = (ButtonRectangle) findViewById(R.id.btnCreateAccount);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(i);
            }
        });


    }


    public static void hideSoftKeyboard(Activity activity) {
            InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);

    }

    class AttemptLogin extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            pDialog = new ProgressDialog(LoginActivity.this,getResources().getString(R.string.prog_msg_login));
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            try {

                password = Utility.makeDigest(password);

                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("username",       username));
                params.add(new BasicNameValuePair("password",       password));
                params.add(new BasicNameValuePair("isFacebookLogin", "0"));
                params.add(new BasicNameValuePair("model",           model));
                params.add(new BasicNameValuePair("version",         version));
                params.add(new BasicNameValuePair("os",              vname));
                params.add(new BasicNameValuePair("ipaddress",       ipaddress));
                params.add(new BasicNameValuePair("versioncode",     finalVersion));

                Log.d("request!", "starting");

                json = jsonParser.makeHttpRequest(
                        Urls.Login, "POST", params);
                if (null != json) {
                    // success tag for json
                    success = json.getInt(TAG_SUCCESS);
                    if (1 == success) {
                        isLoggedIn = true;
                        recommended_version_flag = json.getInt("recommended_version_flag");
                        Log.d("Successfully Login!", json.toString());

                        try {
                            edit.putString("Username",     json.getString("email"));
                            edit.putString("Password",     json.getString("password"));
                            edit.putString("email",        json.getString("email"));
                            edit.putString("number",       json.getString("number"));
                            edit.putString("country",      json.getString("country"));
                            edit.putString("userFullName", json.getString("fullname"));
                           // edit.putString("about",        json.getString("about"));
                            edit.putString("gender",       json.getString("gender"));
                            edit.putString("facebookLogin",json.getString("isFacebookLogin"));
                            edit.putString("sessionid",    json.getString("sessionid"));
                            edit.putString("AuthToken",    json.getString("token"));
                            edit.putBoolean("isLoggedIn",  isLoggedIn);
                            edit.putString("Categories",  json.getJSONObject("categories").toString());
                            //edit.putString("location",     json.getString("location"));
                            //edit.putString("latitude",     json.getString("latitude"));
                           // edit.putString("longitude",    json.getString("longitude"));
                            edit.apply();

                            profilePath = json.getString("profimage");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        edit.commit();
                        return json.getString(TAG_MESSAGE);
                    } else {
                        return json.getString(TAG_MESSAGE);
                    }
                } else {
                    return "failed";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String message) {
            if(null!=profilePath)
                new LoadImage().execute(profilePath);
            if(1 == success){
                if(recommended_version_flag == 1) {
                    showVersionUpdateAlert();
                } else {
                    Intent ii = new Intent(LoginActivity.this, HomeActivity.class);
                    finish();
                    startActivity(ii);
                }
            }
            if (0 == success || message.equalsIgnoreCase("failed")) {
                if (0 == success) {
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

                } else {
                    Toast.makeText(getApplicationContext(), "Check your internet connection!", Toast.LENGTH_LONG).show();

                }
            }
            pDialog.dismiss();
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        }
    }

    private void showVersionUpdateAlert()
    {
        mMaterialDialog = new MaterialDialog(LoginActivity.this)
                .setTitle("Update Required")
                .setMessage("Please update to a newer version to continue using the app")
                .setPositiveButton("UPDATE", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.app.radii"));
                        startActivity(intent);
                        mMaterialDialog.dismiss();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.prog_msg_CANCEL), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                        finish();
                    }
                });

        mMaterialDialog.show();
    }

    private class LoadImage extends AsyncTask<String, String, Bitmap> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream) new URL(args[0]).getContent());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }
        protected void onPostExecute(Bitmap image) {
            if(image != null){
                saveToInternalSorage(image);
            }
        }
        private String saveToInternalSorage(Bitmap bitmapImage){
            ContextWrapper cw = new ContextWrapper(getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
            // Create imageDir
            File mypath = new File(directory,profilePath.substring(profilePath.lastIndexOf("/")+1));
            FileOutputStream fos;
            try {
                fos = new FileOutputStream(mypath);
                // Use the compress method on the BitMap object to write image to the OutputStream
                bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            edit.putString("image", mypath.getAbsolutePath());
            edit.commit();
            return directory.getAbsolutePath();
        }
    }

    private void showNetworkDisabledAlertToUser()
    {
        mMaterialDialog = new MaterialDialog(LoginActivity.this)
                .setTitle(getResources().getString(R.string.prog_msg_network_connection))
                .setMessage(getResources().getString(R.string.prog_msg_network_on))
                .setPositiveButton(getResources().getString(R.string.prog_msg_OK), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int conn = NetworkUtil.getConnectivityStatus(getApplicationContext());
                        if (conn == NetworkUtil.TYPE_NOT_CONNECTED) {
                            Intent callSettingIntent = new Intent(Settings.ACTION_SETTINGS);
                            startActivity(callSettingIntent);

                        }
                        mMaterialDialog.dismiss();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.prog_msg_CANCEL), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                    }
                });

        mMaterialDialog.show();
    }

    class LoadResetPass extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... args) {

            try {
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("email", username));

                Log.d("request!", "starting");

                json = jsonParser.makeHttpRequest(
                        Urls.ForgotPassword, "POST", params);

                success = json.getInt(TAG_SUCCESS);
                Message = json.getString(TAG_MESSAGE);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String message) {
            if (0 == success) {
                mMaterialDialog = new MaterialDialog(LoginActivity.this)
                        .setMessage(Message)
                        .setPositiveButton(getResources().getString(R.string.prog_msg_OK), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mMaterialDialog.dismiss();
                                Intent register = new Intent(getApplicationContext(),RegisterActivity.class);
                                startActivity(register);
                                tvForgot.setClickable(true);
                            }
                        })
                        .setNegativeButton(getResources().getString(R.string.prog_msg_CANCEL), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mMaterialDialog.dismiss();
                                tvForgot.setClickable(true);

                            }
                        });

            } else if (1 == success){
                mMaterialDialog = new MaterialDialog(LoginActivity.this)
                        .setMessage(Message)
                        .setPositiveButton(getResources().getString(R.string.prog_msg_OK), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mMaterialDialog.dismiss();
                                tvForgot.setClickable(true);
                            }
                        });
            }
            mMaterialDialog.show();
        }

    }
}
