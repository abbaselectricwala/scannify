package app.com.scannify.activity;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import app.com.scannify.R;
import app.com.scannify.Utils.ActionBarDrawerToggle;

import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import app.com.scannify.Utils.DrawerArrowDrawable;
import app.com.scannify.fragments.About;
import app.com.scannify.fragments.Category;
import app.com.scannify.fragments.CategoryList;
import app.com.scannify.fragments.FragmentSettings;
import app.com.scannify.fragments.Fragment_Bookmark_List;
import app.com.scannify.fragments.HomeFragment;
import app.com.scannify.fragments.InviteContact;
import app.com.scannify.fragments.ManageProfile;
import app.com.scannify.view.RoundedImage;


public class HomeActivity  extends Activity implements View.OnClickListener, Animation.AnimationListener{

    RoundedImage roundedImage;
    private DrawerLayout            mDrawerLayout;
    private RelativeLayout          mDrawerList;
    private ActionBarDrawerToggle   mDrawerToggle;
    private CharSequence            mDrawerTitle;
    private CharSequence            mTitle;
    private DrawerArrowDrawable     drawerArrow;
    ImageView                       imageView;
    ImageView                       downArrow;
    TextView                        username;
    TextView                        location;
    SharedPreferences               pref;
    SharedPreferences.Editor        edit;
    String                          flag = "0",userId;
    RelativeLayout                  rlProfile;
    RelativeLayout                  rlHome;
    RelativeLayout                  rlContacts;
    RelativeLayout                  rlCategory,rlBookmarks;
    RelativeLayout                  rlCreateContact;
    RelativeLayout                  rlConnections;
    RelativeLayout                  rlSettings;
    RelativeLayout                  rlAbout;
    //RelativeLayout                  rlInvite;
    ActionBar actionBar;
    Menu main_menu;
    String fragment;
    public static String            CUR_PAGE_TITLE      = "Title";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);
        //ScannifyApplication.getInstance().trackScreenView("HomeActivity");
        Intent i = getIntent();
        fragment = i.getStringExtra("title");

        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.propic);
        roundedImage = new RoundedImage(bm);

        initMenu();

        actionBar = this.getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        Animation animFadein = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.blink);
        // set animation listener
        animFadein.setAnimationListener(this);

        mTitle = mDrawerTitle = getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (RelativeLayout) findViewById(R.id.left_drawer);

        drawerArrow = new DrawerArrowDrawable(this) {
            @Override
            public boolean isLayoutRtl() {
                return false;
            }
        };
        mDrawerToggle = new ActionBarDrawerToggle(this,
                mDrawerLayout, // DrawerLayout object
                drawerArrow, // nav drawer image to replace 'Up' image
                R.string.drawer_open, // "open drawer" description for accessibility
                R.string.drawer_close // "close drawer" description for accessibility
        )
        {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }

            @SuppressLint("CommitPrefEdits")
            public void onDrawerOpened(View drawerView) {

                pref = getApplicationContext().getSharedPreferences("AppPref", Context.MODE_PRIVATE);
                edit = pref.edit();

                flag = pref.getString("Flag", "0");

                String strName = pref.getString("userFullName", "User1");
                String imagePath = pref.getString("image", "null");
                String strLoc = pref.getString("location", "Pune");

                if (imagePath.equalsIgnoreCase("null")) {
                    imageView.setImageDrawable(roundedImage);
                } else {
                    Bitmap bm = BitmapFactory.decodeFile(imagePath);
                    roundedImage = new RoundedImage(bm);
                    imageView.setImageDrawable(roundedImage);
                }
                username.setText(strName);
                location.setText(strLoc);
                invalidateOptionsMenu();
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        if (null == fragment) {
            switchFragment(new HomeFragment());
            setTitle("Home");
            setSelected(rlHome);

        } else if(fragment != null)
        {

            if(fragment.equalsIgnoreCase("Home")) {
                switchFragment(new HomeFragment());
                setTitle("Home");
                setSelected(rlContacts);
            }  if(fragment.equalsIgnoreCase("Category List")) {
                switchFragment(new CategoryList());
                setTitle("Categoy List");
                setSelected(rlCategory);
            }
            else if(fragment.equalsIgnoreCase("Bookmarks")) {
                switchFragment(new Fragment_Bookmark_List());
                setTitle("Bookmarks");
                setSelected(rlBookmarks);
            }
            else{
                switchFragment(new HomeFragment());
                setTitle("Home");
                setSelected(rlHome);
            }
        }


    }

    private void initMenu() {
        imageView     = (ImageView)      findViewById(R.id.imageView);
        username      = (TextView)       findViewById(R.id.title);
        location      = (TextView)       findViewById(R.id.desc);
        rlProfile     = (RelativeLayout) findViewById(R.id.rlProfile);
        rlHome        = (RelativeLayout) findViewById(R.id.rlHome);
        rlSettings    = (RelativeLayout) findViewById(R.id.rlSettings);
        rlAbout       = (RelativeLayout) findViewById(R.id.rlAbout);
        //rlInvite      = (RelativeLayout) findViewById(R.id.rlInvite);
        rlCategory    =(RelativeLayout)  findViewById(R.id.rlCategory);
        rlBookmarks   =(RelativeLayout)  findViewById(R.id.rlBookmarks);

        rlProfile.setOnClickListener(this);
        rlHome.setOnClickListener(this);
        rlSettings.setOnClickListener(this);
        rlAbout.setOnClickListener(this);
       // rlInvite.setOnClickListener(this);
        rlCategory.setOnClickListener(this);
        rlBookmarks.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        Fragment newContent = new Category();

        Bundle   bundle     = new Bundle();

        if (v.getId() == R.id.rlProfile) {
            // PROFILE
            newContent = new ManageProfile();
            bundle.putString(CUR_PAGE_TITLE, getResources().getString(R.string.profile));
            setTitle(getResources().getString(R.string.profile));
            setSelected(rlProfile);
        } else if (v.getId() == R.id.rlHome) {
            // HOME
            newContent = new HomeFragment();
            bundle.putString(CUR_PAGE_TITLE, "Home");
            setTitle("Home");
            setSelected(rlHome);
        }
            else if (v.getId() == R.id.rlCategory) {
            // CONTACT
            if (flag.equalsIgnoreCase("0")) {
                newContent = new CategoryList();
                bundle.putString(CUR_PAGE_TITLE, "Category List");
                setTitle("Category List");
                setSelected(rlCategory);
            }
        }
        else if (v.getId() == R.id.rlBookmarks) {
            // CONTACT
            if (flag.equalsIgnoreCase("0")) {
                newContent = new Fragment_Bookmark_List();
                bundle.putString(CUR_PAGE_TITLE, "Bookmarks");
                setTitle("Bookmarks");
                setSelected(rlBookmarks);
            }
        }
        else if (v.getId() == R.id.rlSettings) {
            // SETTINGS
            newContent = new FragmentSettings();
            bundle.putString(CUR_PAGE_TITLE, getResources().getString(R.string.settings));
            setTitle(getResources().getString(R.string.settings));
            setSelected(rlSettings);
        } else if (v.getId() == R.id.rlAbout) {
            // ABOUT
            newContent = new About();
            bundle.putString(CUR_PAGE_TITLE, getResources().getString(R.string.About));
            setTitle(getResources().getString(R.string.About));
            setSelected(rlAbout);
        }
     /*  else if (v.getId() == R.id.rlInvite){
            // Invite
            newContent = new InviteContact();
            bundle.putString(CUR_PAGE_TITLE, getResources().getString(R.string.Invite));
            setTitle(getResources().getString(R.string.Invite));
            setSelected(rlInvite);
        }*/
        if (newContent != null)
        {
            newContent.setArguments(bundle);
            switchFragment(newContent);
        }
    }


    private void switchFragment(Fragment fragment) {
        mDrawerLayout.closeDrawer(mDrawerList);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }


    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        main_menu = menu;

        return true;
    }



    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                mDrawerLayout.closeDrawer(mDrawerList);
            } else {
                mDrawerLayout.openDrawer(mDrawerList);
            }
        }
        return true;
    }

    private void setSelected(RelativeLayout rl) {
        rlProfile.setSelected(false);
        rlHome.setSelected(false);
        rlSettings.setSelected(false);
        rlAbout.setSelected(false);
        rl.setSelected(true);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


}
