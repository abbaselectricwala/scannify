package app.com.scannify.Utils;

import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;
import android.util.Patterns;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validate extends Activity {

    String addressStr = "";
    String nameStr    = "";
    String emailStr   = "";
    String phoneStr   = "";
    String websiteStr = "";
    String companyStr = "";
    String desigStr   = "";

    static int nameFlag  = 0;
    static int emailflag = 0;
    static int webFlag   = 0;
    static int desgFlag  = 0;
    static int cmpFlag   = 0;

    public String data;
    Context       context;

    List<String>      params = new ArrayList<String>();
    ArrayList<String> designationList = new ArrayList<>();
    ArrayList<String> phoneKeywordList = new ArrayList<>();

    public Validate(Activity act, String text) {
        this.data    = text;
        this.context = act;
        // adding designations in list..
        designationList.add("director");
        designationList.add("vicepresident");
        designationList.add("ceo");
        designationList.add("staff");
        designationList.add("customerrelation");
        designationList.add("service");
        designationList.add("executive");
        designationList.add("md");
        designationList.add("architect");
        designationList.add("advocate");
        designationList.add("builder");
        designationList.add("dealer");
        designationList.add("manager");
        designationList.add("teamleader");
        designationList.add("charteredaccountant");
        designationList.add("incharge");
        designationList.add("hr");
        designationList.add("admin");
        designationList.add("engineer");
        designationList.add("senior");
        designationList.add("m.com");
        designationList.add("llb");
        designationList.add("doctor");
        designationList.add("supplier");
        designationList.add("advisor");
        designationList.add("engineering");

        // validating scanned data..
        decode(data);
    }

    // Checking email is valid or not
    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        if (email.contains(" ")) {
            String[] splitEmail = email.split(" ");
            for (String emailAsset : splitEmail) {
                if (emailAsset.contains("@")) {
                    isValid = true;
                    break;
                } else {
                    isValid = false;
                }
            }
        } else {
            if (email.contains("@")) {
                isValid = true;
            } else {
                isValid = false;
            }
        }

        return isValid;
    }

    // Checking phone number is valid or not
    public static boolean isPhoneNumberValid(String check) {

        check        = check.replaceAll("[() .^:,-]", "");
        System.out.println("String : " + check);
        String regex = "^((\\+|00)(\\d{1,3})[\\s-]?)?(\\d{8,})$";
        Pattern p    = Pattern.compile(regex);
        Matcher m    = p.matcher(check);
        if (m.matches())
            return true;
        return false;
    }

    // Checking name is valid or not
    public static boolean isNameValid(String name) {
        return name.matches("[A-Z][a-zA-Z]*" + "[a-zA-z]+[&]*" + "([ '-][&]*[a-zA-Z]+)*");
        //("[A-Z][a-zA-Z]*" + "[a-zA-z]+([ '-][a-zA-Z]+)*");
    }

    // Checking website is valid or not
    public static boolean isWebsiteValid(String url) {
        //return Patterns.WEB_URL.matcher(url).matches();
        boolean isValid = false;
        if (url.contains(" ")) {
            String[] splitUrl = url.split(" ");
            for (String webAsset : splitUrl) {
                if (webAsset.contains("www")) {
                    if (webFlag == 0) {
                        isValid = true;
                        webFlag = 1;
                        break;
                    }
                } else {
                    isValid = false;
                }
            }
        } else {
            if (url.contains("www")) {
                if (webFlag == 0) {
                    isValid = true;
                    webFlag = 1;
                }
            } else {
                isValid = false;
            }
            if (Patterns.WEB_URL.matcher(url).matches()) {
                if (webFlag == 0) {
                    isValid = true;
                    webFlag = 1;
                }
            }
        }
        return isValid;
    }

    // Checking addr is valid or not
    public String isAddrValid(String str) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());

        String result = null;

        List<Address> addressList = null;
        try {
            addressList = geocoder.getFromLocationName(str, 5);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addressList != null && addressList.size() > 0) {
            Address address  = addressList.get(0);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                if (str.contains(address.getAddressLine(i))) {
                    sb.append(address.getAddressLine(i)).append(",");
                }
                Log.d("addr line" + i, address.getAddressLine(i));
            }

            result = sb.toString();
            Log.d("addr details", result);
        }
        return result;
    }

    public void decode(String data) {
        /*Split the string into multiple lines based on new line character*/
        String[] splits = data.split("\n");
        for (String asset : splits) {
            Log.d("assets", asset);
           /*If line contains blank chracter or space continue the loop*/
            if (asset.equals("") || asset.equals(" ")) {
                continue;
            }

            if (isNameValid(asset)) { // is Name valid.. if yes
                if (nameFlag == 0) { // checking name is already set or not..if No
                    String[] n = asset.split(" ");
                    if (n.length > 1) {
                        setNameStr(asset);
                        nameFlag = 1;
                    }
                } else if (desgFlag == 0) {  // is Name valid.. if No then, is designation valid..if yes
                    for (int i = 0; i < designationList.size(); ++i) {
                        if (desgFlag == 0) {
                            if (asset.toLowerCase().contains(designationList.get(i))) {
                                setDesigStr(asset);
                                desgFlag = 1;
                            }
                        }
                    }
                } else { // is Name valid.. if No then, is designation valid..if No then, setting string as address
                    String result = isAddrValid(asset);
                    if (result != null) {
                        setAddressStr(result);
                    }
                }
            }

            if (isEmailValid(asset)) { // is Email valid.. if yes
                if (emailflag == 0) { // checking email is already set or not..if No
                    setEmailStr(asset);
                    emailflag = 1;
                }
            }

            if (isPhoneNumberValid(asset)) { // is phone valid.. if yes
                setPhoneStr(asset);
            }

            if (isWebsiteValid(asset)) { // is website valid.. if yes
                setWebsiteStr(asset);
            }

            // check for company name..
            if ((asset.toLowerCase().contains("pvt")) || (asset.toLowerCase().contains("private") || (asset.toLowerCase().contains("limited"))
                    || asset.toLowerCase().contains("enterprises") || asset.toLowerCase().contains("services") || asset.toLowerCase().contains("solutions")
                    || asset.toLowerCase().contains("properties"))) {
                if (cmpFlag == 0) { // checking campany name is already set or not..if No
                    setCompanyName(asset);
                    cmpFlag = 1;
                }
            }

            // check for website
            if (asset.contains("www")) {
                if (webFlag == 0) { // checking website is already set or not..if No
                    int index = asset.indexOf("www");
                    setWebsiteStr(asset.substring(index));
                    webFlag = 1;
                }
            }

            // check for email..
            if (asset.contains("@")) { // checking string contains @ or not
                if (emailflag == 0) { // checking email is already set or not..if No

                    if (asset.contains(":")) {
                        String[] str = asset.split(":");
                        setEmailStr(str[1]);
                        emailflag = 1;
                    } else if (asset.toLowerCase().contains("e") || asset.toLowerCase().contains("email") // checking string contains any of these keywords..
                            || asset.toLowerCase().contains("e-mail") || asset.toLowerCase().contains("e mail")) {

                        // if it founds ONLY email/e-mail/e/e mail then return
                        if (asset.equalsIgnoreCase("email") || asset.equalsIgnoreCase("e-mail") || asset.equalsIgnoreCase("e") || asset.equalsIgnoreCase("e mail")) {
                            return;
                        } else { // if it contains "email 45794594504" then splitting that with space..
                            String[] str = asset.split(" ");
                            for (int k = 0; k < str.length; ++k) {
                                if (isEmailValid(str[k])) {
                                    setEmailStr(str[k]);
                                    emailflag = 1;
                                }
                            }
                        }
                    } else { // normal check for email validation..
                        if (isEmailValid(asset)) {
                            setEmailStr(asset);
                            emailflag = 1;
                        }
                    }
                }
            }

            // check for phone number wether string contains any of below's strings..
            if (asset.toLowerCase().contains("tel") || asset.toLowerCase().contains("fax") || asset.toLowerCase().contains("mobile")
                    || asset.toLowerCase().contains("mob") || asset.toLowerCase().contains("ph") || asset.toLowerCase().contains("cell")
                    || asset.toLowerCase().contains("phone") || asset.toLowerCase().contains("contact")) {

                if (asset.contains(",")) { // if string contains , i.e multiple numbers can be there..eg. phone 2323482934982,4587452844
                    String[] a = asset.split(",");
                    for (int j = 0; j < a.length; ++j) {
                        // removing all spaces and specia; characters from string spliited by ","..then splitting strings and numbers..
                        String[] b = a[j].replace(" ", "").replaceAll("[() .^:,-]", "").split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
                        for (int k = 0; k < b.length; ++k) {
                            if (isPhoneNumberValid(b[k])) { // phone number validation..
                                setPhoneStr(b[k]);
                            }
                        }
                    }
                } else {
                    // if no multiple numbers are there... eg. phone 93472472034
                    //for such removing spaces and special characters and then spltiting numbers and strings..
                    String a[] = asset.replace(" ", "").replaceAll("[() .^:,-]", "").split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");

                    for (int j = 0; j < a.length; ++j) {
                        Log.d("a", a[j] + "");
                        if (isPhoneNumberValid(a[j])) {
                            setPhoneStr(a[j]);
                        }
                    }
                }
            }

            // check for string is address or phone number..
            if (asset.contains(",")) { // if contains "," then string  can be addr or phone number..
                if (asset.contains("+")) { // if string contains "+" means its a number..

                    for (int i = 0; i < phoneKeywordList.size(); ++i) {
                        //checking string contains any phone related keyword ..eg.phone,mobile etc.
                        if (asset.toLowerCase().contains(phoneKeywordList.get(i))) {
                            String a[] = asset.replace(" ", "").split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
                            Log.d("a", a + "");
                            for (int j = 0; j < a.length; ++j) {
                                if (isPhoneNumberValid(a[j])) {
                                    setPhoneStr(a[j]);
                                }
                            }
                        }
                        break;
                    }
                } else if (asset.contains(":")) {
                    String a[] = asset.replace(" ", "").split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
                    Log.d("a", a + "");
                    for (int j = 0; j < a.length; ++j) {
                        if (isPhoneNumberValid(a[j])) {
                            setPhoneStr(a[j]);
                        }
                    }
                } else { //  string not contains "+" means it can be a  addr..
                    //checking string contains any below's keywords..eg..."pune 41109  mobile 65428642946"
                    if (asset.toLowerCase().contains("tel") || asset.toLowerCase().contains("fax") || asset.toLowerCase().contains("mobile")
                            || asset.toLowerCase().contains("mob") || asset.toLowerCase().contains("ph") || asset.toLowerCase().contains("cell")
                            || asset.toLowerCase().contains("phone") || asset.toLowerCase().contains("contact")) {
                        String[] abc = asset.split(",");
                        for (int x = 0; x < abc.length; ++x) {
                            //calling addr validation function..
                            String result = isAddrValid(abc[x]);
                            if (result != null) {
                                setAddressStr(result);
                            }
                        }
                    } else { // if string not contains such keywords then its a addr..
                        setAddressStr(asset);
                    }
                }
            }
        }
    }

    public void setNameStr(String name) {
        String[] n = null;
        if (name.contains(" ")) {
            n = name.split(" ");
            if (n.length == 2) {
                this.nameStr = this.nameStr + n[0] + " " + n[1];
            } else if (n.length == 3) {
                this.nameStr = this.nameStr + n[0] + " " + n[1] + " " + n[2];
            } else if (n.length == 4) {
                this.nameStr = this.nameStr + n[0] + " " + n[1] + " " + n[2] + " " + n[3];
            }
        } else {
            this.nameStr = this.nameStr + name;
        }
    }

    public void setAddressStr(String address) {
        this.addressStr = this.addressStr + address;
    }

    public void setWebsiteStr(String website) {
        this.websiteStr = this.websiteStr + website;
    }

    public void setEmailStr(String email) {
        this.emailStr = this.emailStr + email;
    }

    public void setPhoneStr(String phone)//String lable ,
    {
        this.phoneStr = this.phoneStr + phone;
        params.add(phone);
    }

    public void setDesigStr(String desig) {
        this.desigStr = this.desigStr + desig;
    }

    public void setCompanyNameE(String email) {
        if (!email.equals("")) {
            int id1 = email.indexOf("@");
            int id2 = email.lastIndexOf(".");
//            this.companyStr = this.companyStr + email.substring(id1 + 1, id2);
        }
    }

    public void setCompanyName(String cname) {
        this.companyStr = this.companyStr + cname;
    }

    public String getNameStr() {
        return this.nameStr;
    }

    public String getAddressStr() {
        return this.addressStr;
    }

    public String getEmailStr() {
        return this.emailStr;
    }

    public List getPhoneStr() {
        return params;
    }

    public String getWebsiteStr() {
        return this.websiteStr;
    }

    public String getCompanyStr() {
        return this.companyStr;
    }

    public String getDesignationStr() {
        return this.desigStr;
    }

}

