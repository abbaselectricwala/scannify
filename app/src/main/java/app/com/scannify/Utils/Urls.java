package app.com.scannify.Utils;

/**
 * Created by user on 4/18/2016.
 */
public class Urls {


    public static final String SITE_BASE = "http://192.168.0.115/test/";
    public static final String Login = SITE_BASE + "Login.php";

    public static final String FBLogin = SITE_BASE + "Login.php";

    public static final String UpdateUser = SITE_BASE + "changeIndUserProf.php";

    public static final String UpdateAppSettings = SITE_BASE + "UpdateSettings.php";

    public static final String RegisterIndividul = SITE_BASE + "IRegister.php";

    public static final String Logout = SITE_BASE + "logout.php";

    public static final String UploadImage = SITE_BASE + "profileUploadImage.php";
    public static final String UploadCardImage = SITE_BASE + "cardUploadImage.php";
    public static final String UploadCategoryImage = SITE_BASE + "categoryUploadImage.php";
    public static final String Search = SITE_BASE + "searchCard.php";

    public static final String GetCardsDetails = SITE_BASE + "getCards.php";
    public static final String CreateCard = SITE_BASE + "createCard1.php";
    public static final String GetCategories = SITE_BASE + "getCategories.php";
    public static final String CreateCategories = SITE_BASE + "editCategory.php";
    public static final String ForgotPassword = SITE_BASE + "fp/ForgotPassword.php";
    public static final String ChangePassword = SITE_BASE + "ChangePassword.php";
    public static final String AddBookmarked = SITE_BASE + "bookmarkCard.php";
    public static final String GetBookmarkedCards = SITE_BASE + "getBookmarkedCards.php";
    public static final String EditCard = SITE_BASE + "createCard1.php";
    public static final String DeleteAccount = SITE_BASE + "DeleteAccount.php";




}