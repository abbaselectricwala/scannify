package app.com.scannify.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Created by user on 4/18/2016.
 */
public class JSONParser {
    InputStream is    = null;
    String      json  = "";
    JSONObject jsonObj;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    String AuthToken, sessionId;

    /*public JSONParser(){
        AuthToken = "Empty";
        sessionId = "Empty";
    }*/

    public JSONParser(Context context) {
        pref  = context.getSharedPreferences("AppPref", context.MODE_PRIVATE);
        edit  = pref.edit();
        AuthToken = pref.getString("AuthToken", "Empty");
        sessionId = pref.getString("sessionid","Empty");
    }

    public JSONObject getJSONFromUrl(final String url) {
        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity     = httpResponse.getEntity();
            is  = httpEntity.getContent();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            // Create a BufferedReader
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            // Declaring string builder
            StringBuilder str = new StringBuilder();
            //  string to store the JSON object.
            String strLine;
            // Building while we have string !equal null.
            while ((strLine = reader.readLine()) != null) {
                str.append(strLine + "\n");
            }
            // Close inputstream.
            is.close();
            // string builder data conversion  to string.
            json = str.toString();
        } catch (Exception e) {
            Log.e("Error", " something wrong with converting result " + e.toString());
        }

        // Try block used for pasrseing String to a json object
        try {
            jsonObj = new JSONObject(json);
        } catch (JSONException e) {
            Log.e("json Parsering", "" + e.toString());
        }
        // Returning json Object.
        return jsonObj;
    }

    public JSONObject makeHttpRequest(String url, String method, List<NameValuePair> params) {
        try {
            // checking request method
            if ("POST".equals(method)) {
                // now defaultHttpClient object
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost            = new HttpPost(url);

                //httpPost.setEntity(new UrlEncodedFormEntity(params));
                if(!(AuthToken.equalsIgnoreCase("Empty"))) {
                    //AuthToken =  Utility.makeDigest(AuthToken);
                    httpPost.setHeader("AuthenticationToken", AuthToken);
                }

                if(!(sessionId.equalsIgnoreCase("Empty")))
                    httpPost.setHeader("sessionId",sessionId);

                httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8");

                JSONObject jsonObject = new JSONObject();
                for (int i = 0; i < params.size(); ++i) {
                    try {
                        jsonObject.accumulate(params.get(i).getName(), params.get(i).getValue());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                json = jsonObject.toString();

                httpPost.setEntity(new StringEntity(json));
                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity     = httpResponse.getEntity();
                is                        = httpEntity.getContent();
                // Header hr                 = httpEntity.getContentType();
                //Header[] headers          = httpResponse.getAllHeaders();

                Log.d("SWAPNIL Request Json...........", params.toString());
            } else if ("GET".equals(method)) {
                // request method is GET
                DefaultHttpClient httpClient = new DefaultHttpClient();
                String paramString           = URLEncodedUtils.format(params, "utf-8");
                url += "?" + paramString;
                HttpGet httpGet              = new HttpGet(url);
                HttpResponse httpResponse    = httpClient.execute(httpGet);
                HttpEntity httpEntity        = httpResponse.getEntity();
                is                           = httpEntity.getContent();
            }

        } catch (UnsupportedEncodingException e) {
            Log.d("SWAPNIL Request Failed...", "HTTP");
            e.printStackTrace();
            return null;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            Log.d("SWAPNIL Request Failed...", "HTTP");
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("SWAPNIL Request Failed...", "HTTP");
            return null;
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            StringBuilder str = new StringBuilder();
            String strLine;
            while ((strLine = reader.readLine()) != null) {
                str.append(strLine + "\n");
            }
            is.close();
            json = str.toString();
            Log.d("SWAPNIL Response Json...", json.toString());
        } catch (Exception e) {
            Log.d("SWAPNIL Read Data Failed...", json.toString());
            e.printStackTrace();
        }
        // now will try to parse the string into JSON object
        try {

            jsonObj = new JSONObject(json);
        } catch (JSONException e) {
            Log.d("SWAPNIL Parsing Data Failed...", json.toString());
            e.printStackTrace();
        }
        return jsonObj;
    }
}
