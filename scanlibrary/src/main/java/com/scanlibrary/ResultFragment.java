package com.scanlibrary;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.vision.v1.Vision;
import com.google.api.services.vision.v1.VisionRequestInitializer;
import com.google.api.services.vision.v1.model.AnnotateImageRequest;
import com.google.api.services.vision.v1.model.BatchAnnotateImagesRequest;
import com.google.api.services.vision.v1.model.BatchAnnotateImagesResponse;
import com.google.api.services.vision.v1.model.EntityAnnotation;
import com.google.api.services.vision.v1.model.Feature;
import com.google.api.services.vision.v1.model.Image;
import com.scanlibrary.utils.Validate;

import org.apache.http.NameValuePair;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * Created by jhansi on 29/03/15.
 */
public class ResultFragment extends Fragment {

    private View view;
    private ImageView scannedImageView;
    private Button doneButton;
    private Bitmap original;
    private Button originalButton;
    private Button MagicColorButton;
    private Button grayModeButton;
    private Button bwButton;
    private Bitmap transformed;
    EditText edName,edEmail, edPhone, edWebsite, edAddress, edCompanyName, edTitle1, edTitle2, edTitle3, edDesignation, edNotes;//edTitle
    ProgressDialog pDialog;
    TextView tvText;
    Bitmap magicColorBitmap;
    Validate validate;
    List<NameValuePair>      valuePairs;
    RelativeLayout           rel1, rel2;
    TextView                 tv1, tv2;

    public ResultFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.result_layout, null);
        init();
        return view;
    }

    private void init() {
        edName = (EditText) view.findViewById(R.id.edName);
        edEmail = (EditText) view.findViewById(R.id.edEmail);
        edPhone = (EditText) view.findViewById(R.id.edPhone);
        edWebsite = (EditText) view.findViewById(R.id.edWebsite);
        edAddress = (EditText) view.findViewById(R.id.edAddress);
        edCompanyName = (EditText) view.findViewById(R.id.edCmpName);
        edDesignation = (EditText) view.findViewById(R.id.edDesignatoin);
        edNotes = (EditText) view.findViewById(R.id.edNotes);
      //  edTitle = (EditText) view.findViewById(R.id.edtitle);
        edTitle1 = (EditText) view.findViewById(R.id.ed1);
        edTitle2 = (EditText) view.findViewById(R.id.ed2);
        edTitle3 = (EditText) view.findViewById(R.id.ed3);
        tv1          = (TextView)        view.findViewById(R.id.tv1);
        tv2          = (TextView)        view.findViewById(R.id.tv2);
        rel1         = (RelativeLayout)  view.findViewById(R.id.rlTitle1);
        rel2         = (RelativeLayout)  view.findViewById(R.id.rlTitle2);

        scannedImageView = (ImageView) view.findViewById(R.id.scannedImage);
        originalButton = (Button) view.findViewById(R.id.original);
        originalButton.setOnClickListener(new OriginalButtonClickListener());
        MagicColorButton = (Button) view.findViewById(R.id.magicColor);
        MagicColorButton.setOnClickListener(new MagicColorButtonClickListener());
        grayModeButton = (Button) view.findViewById(R.id.grayMode);
        grayModeButton.setOnClickListener(new GrayButtonClickListener());
        bwButton = (Button) view.findViewById(R.id.BWMode);
       // tvText = (TextView) view.findViewById(R.id.tvText);
        bwButton.setOnClickListener(new BWButtonClickListener());

        doneButton = (Button) view.findViewById(R.id.doneButton);
        doneButton.setOnClickListener(new DoneButtonClickListener());
        valuePairs = new ArrayList<NameValuePair>();
        Bitmap bitmap = getBitmap();
        setScannedImage(bitmap);
    }

    private Bitmap getBitmap() {
        Uri uri = getUri();
        try {
            original = Utils.getBitmap(getActivity(), uri);
            getActivity().getContentResolver().delete(uri, null, null);
            return original;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Uri getUri() {
        Uri uri = getArguments().getParcelable(ScanConstants.SCANNED_RESULT);
        return uri;
    }

    public void setScannedImage(Bitmap scannedImage) {
       // magicColorBitmap = ((ScanActivity) getActivity()).getMagicColorBitmap(scannedImage);
        magicColorBitmap  = scannedImage;//brightIt(100);
        Bitmap grayBitmap = ((ScanActivity) getActivity()).getGrayBitmap(scannedImage);
        scannedImageView.setImageBitmap(magicColorBitmap);
        scannedImageView.setColorFilter(brightIt(70));
        new callCloudVision(scaleBitmapDown(grayBitmap,1000)).execute();
       /* valuePairs = validate.getPhoneStr();
        int count = valuePairs.size();
        NameValuePair nm;
        if (count == 1) {
            nm = valuePairs.get(0);
            String value = nm.getValue();
            edPhone.setText(value);
        }
        if (count == 2) {
            nm = valuePairs.get(0);
            String value = nm.getValue();
            edPhone.setText(value);

            RelativeLayout rel1 = (RelativeLayout) view.findViewById(R.id.rlTitle1);
            rel1.setVisibility(View.VISIBLE);
            TextView tv1 = (TextView) view.findViewById(R.id.tv1);
            EditText ed1 = (EditText) view.findViewById(R.id.ed1);
            nm = valuePairs.get(1);
            String text = nm.getName();
            tv1.setText(text);
            String val = nm.getValue();
            ed1.setText(val);
        }

        if (count == 3) {
            nm = valuePairs.get(0);
            String value = nm.getValue();
            edPhone.setText(value);

            RelativeLayout rel1 = (RelativeLayout) view.findViewById(R.id.rlTitle1);
            rel1.setVisibility(View.VISIBLE);
            TextView tv1 = (TextView) view.findViewById(R.id.tv1);
            EditText ed1 = (EditText) view.findViewById(R.id.ed1);
            nm = valuePairs.get(1);
            String text = nm.getName();
            tv1.setText(text);
            String val = nm.getValue();
            ed1.setText(val);

            RelativeLayout rel2 = (RelativeLayout) view.findViewById(R.id.rlTitle2);
            rel2.setVisibility(View.VISIBLE);
            TextView tv2 = (TextView) view.findViewById(R.id.tv2);
            EditText ed2 = (EditText) view.findViewById(R.id.ed2);
            nm = valuePairs.get(2);
            String text1 = nm.getName();
            tv2.setText(text1);
            String val1 = nm.getValue();
            ed2.setText(val1);
        }*/


    }

    private class DoneButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Intent data = new Intent();
            Bitmap bitmap = magicColorBitmap;
            if (bitmap == null) {
                bitmap = original;
            }
            Uri uri = Utils.getUri(getActivity(), bitmap);
            data.putExtra(ScanConstants.SCANNED_RESULT, uri);
            getActivity().setResult(Activity.RESULT_OK, data);
            original.recycle();
            System.gc();
            getActivity().finish();
        }
    }

    private class BWButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            transformed = ((ScanActivity) getActivity()).getBWBitmap(original);
            scannedImageView.setImageBitmap(transformed);
        }
    }

    private class MagicColorButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            transformed = ((ScanActivity) getActivity()).getMagicColorBitmap(original);
            scannedImageView.setImageBitmap(transformed);
        }
    }

    private class OriginalButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            transformed = original;
            scannedImageView.setImageBitmap(original);
        }
    }

    private class GrayButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            transformed = ((ScanActivity) getActivity()).getGrayBitmap(original);
            scannedImageView.setImageBitmap(transformed);
        }
    }

    private String convertResponseToString(BatchAnnotateImagesResponse response) {
        String message = "";
        List<EntityAnnotation> texts = response.getResponses().get(0).getTextAnnotations();
        if (texts != null) {
            for (EntityAnnotation text : texts) {
                message += String.format("%s", text.getDescription());
                message += "\n";
                Log.d("Cloude Msg:", message);
            }
        } else {
            message += "nothing";
        }
        return message;
    }
    // Calling cloud vision api..
    class callCloudVision extends AsyncTask<Object, Void, String> {
        Bitmap bitmap;

        public callCloudVision(Bitmap imageBitmap) {
            super();
            // do stuff
            bitmap = imageBitmap;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage(getResources().getString(R.string.prog_msg_ExtractingDenominationId));
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Object... args) {
            try {
                HttpTransport httpTransport = AndroidHttp.newCompatibleTransport();
                JsonFactory jsonFactory     = GsonFactory.getDefaultInstance();

                Vision.Builder builder      = new Vision.Builder(httpTransport, jsonFactory, null);
                builder.setVisionRequestInitializer(new VisionRequestInitializer(getResources().getString(R.string.CLOUD_VISION_API_KEY)));
                Vision vision               = builder.build();

                BatchAnnotateImagesRequest batchAnnotateImagesRequest = new BatchAnnotateImagesRequest();
                batchAnnotateImagesRequest.setRequests(new ArrayList<AnnotateImageRequest>() {{
                    AnnotateImageRequest annotateImageRequest = new AnnotateImageRequest();
                    // Add the image
                    Image base64EncodedImage = new Image();
                    // Convert the bitmap to a JPEG
                    // Just in case it's a format that Android understands but Cloud Vision
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream);
                    byte[] imageBytes = byteArrayOutputStream.toByteArray();

                    // Base64 encode the JPEG
                    base64EncodedImage.encodeContent(imageBytes);
                    annotateImageRequest.setImage(base64EncodedImage);
                    annotateImageRequest.setFeatures(new ArrayList<Feature>() {{
                        Feature textDetection = new Feature();
                        textDetection.setType("TEXT_DETECTION");
                        textDetection.setMaxResults(10);
                        add(textDetection);
                    }});
                    // Add the list of one thing to the request
                    add(annotateImageRequest);
                }});

                Vision.Images.Annotate annotateRequest = vision.images().annotate(batchAnnotateImagesRequest);
                // Due to a bug: requests to Vision API containing large images fail when GZipped.
                annotateRequest.setDisableGZipContent(true);
                Log.d(TAG, "created Cloud Vision request object, sending request");

                BatchAnnotateImagesResponse response = annotateRequest.execute();
                return convertResponseToString(response);
            } catch (GoogleJsonResponseException e) {
                Log.d(TAG, "failed to make API request because " + e.getContent());
            } catch (IOException e) {
                Log.d(TAG, "failed to make API request because of other IOException " + e.getMessage());
            }
            return "Cloud Vision API request failed. Check logs for details.";
        }

        protected void onPostExecute(String message) {
            if (message != null) {
                String[] str = new String[message.length()];
                String beforeFirstDot = message.split("\n\n")[0];
               // String[] finalStr = beforeFirstDot.split("\n");

               // for(int i = 0; i < finalStr.length; ++i){
                    // edCaptureCurrency.append(System.getProperty("line.separator"));
//                    tvText.append(finalStr[i] +"\n");
                    //validate = new Validate(beforeFirstDot);
                Intent i = null;
                try {
                    i = new Intent(getActivity(), Class.forName("app.com.officevikings.activity.ContactDetailActivity.class"));
                    i.putExtra("data", beforeFirstDot);
                    startActivity(i);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

                    /*edName.setText(validate.getNameStr());
                    edEmail.setText(validate.getEmailStr());
                    edWebsite.setText(validate.getWebsiteStr());
                    edAddress.setText(validate.getAddressStr());
                    edTitle.setText(validate.getTitleStr());
                    edCompanyName.setText(validate.getCompanyStr());*/
                    //  edCaptureCurrency.append(System.getProperty("line.separator"));
               // }
               // displayDetails();
                if (null != pDialog) {
                    pDialog.dismiss();
                    getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }
            }
        }
    }
/*
    public static Bitmap changeBitmapContrastBrightness(Bitmap bmp, float contrast, float brightness)
    {
        ColorMatrix cm = new ColorMatrix(new float[]
                {
                        contrast, 0, 0, 0, brightness,
                        0, contrast, 0, 0, brightness,
                        0, 0, contrast, 0, brightness,
                        0, 0, 0, 1, 0
                });

        Bitmap ret = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), bmp.getConfig());

        Canvas canvas = new Canvas(ret);

        Paint paint = new Paint();
        paint.setColorFilter(new ColorMatrixColorFilter(cm));
        canvas.drawBitmap(bmp, 0, 0, paint);

        return ret;
    }*/
    private void displayDetails() {
       // if (scan_count == 0) {
            edName.setText(validate.getNameStr());
            //phone.setText(validate.getPhoneStr());
            edEmail.setText(validate.getEmailStr());
            edWebsite.setText(validate.getWebsiteStr());
            edAddress.setText(validate.getAddressStr());
            //edTitle.setText(validate.getTitleStr());
            edCompanyName.setText(validate.getCompanyStr());

           valuePairs = validate.getPhoneStr();
            int count = valuePairs.size();
            NameValuePair nm;
            if (count == 1) {
                nm = valuePairs.get(0);
                String value = nm.getValue();
                edPhone.setText(value);
            }
            if (count == 2) {
                nm = valuePairs.get(0);
                String value = nm.getValue();
                edPhone.setText(value);
                rel1.setVisibility(View.VISIBLE);
                nm = valuePairs.get(1);
                String text = nm.getName();
                tv1.setText(text);
                String val = nm.getValue();
                edTitle1.setText(val);
            }

            if (count == 3) {
                nm = valuePairs.get(0);
                String value = nm.getValue();
                edPhone.setText(value);

                rel1.setVisibility(View.VISIBLE);
                nm = valuePairs.get(1);
                String text = nm.getName();
                tv1.setText(text);
                String val = nm.getValue();
                edTitle1.setText(val);

                rel2.setVisibility(View.VISIBLE);
                nm = valuePairs.get(2);
                String text1 = nm.getName();
                tv2.setText(text1);
                String val1 = nm.getValue();
                edTitle2.setText(val1);
            }
       /* } else {
            if (detail.getString("name", "").equalsIgnoreCase("null")) {
                //name.setText("No Name Available");
                name.setText(validate.getNameStr());
            } else {
                //name.setText(validate.getNameStr());
                name.setText(detail.getString("name", ""));
            }
            if (detail.getString("address", "").equalsIgnoreCase("null")) {
                //address.setText("No Address Available");
                address.setText(validate.getAddressStr());
            } else {
                //address.setText(validate.getAddressStr());
                address.setText(detail.getString("address", ""));
            }
            if (detail.getString("email", "").equalsIgnoreCase("null")) {
                //email.setText("No email Available");
                email.setText(validate.getEmailStr());
            } else {
                //email.setText(validate.getEmailStr());
                email.setText(detail.getString("email", ""));
            }
            if (detail.getString("title", "").equalsIgnoreCase("null")) {
                //title.setText("No Title Available");
                title.setText(validate.getTitleStr());
            } else {
                //title.setText(validate.getTitleStr());
                title.setText(detail.getString("title", ""));
            }
            if (detail.getString("company", "").equalsIgnoreCase("null")) {
                //company.setText("No company Name Available");
                company.setText(validate.getCompanyStr());
            } else {
                //company.setText(validate.getCompanyStr());
                company.setText(detail.getString("company", ""));
            }
            if (detail.getString("website", "").equalsIgnoreCase("null")) {
                //website.setText("No Website Available");
                website.setText(validate.getWebsiteStr());
            } else {
                //website.setText(validate.getWebsiteStr());
                website.setText(detail.getString("website", ""));
            }

            if (detail.getString("phone", "").equalsIgnoreCase("null")) {
                valuePairs = validate.getPhoneStr();
                int count = valuePairs.size();
                NameValuePair nm;
                if (count == 1) {
                    nm = valuePairs.get(0);
                    String value = nm.getValue();
                    edPhone.setText(value);
                }
                if (count == 2) {
                    nm = valuePairs.get(0);
                    String value = nm.getValue();
                    edPhone.setText(value);
                    rel1.setVisibility(View.VISIBLE);
                    nm = valuePairs.get(1);
                    String text = nm.getName();
                    tv1.setText(text);
                    String val = nm.getValue();
                    ed1.setText(val);
                }

                if (count == 3) {
                    nm = valuePairs.get(0);
                    String value = nm.getValue();
                    edPhone.setText(value);

                    rel1.setVisibility(View.VISIBLE);
                    nm = valuePairs.get(1);
                    String text = nm.getName();
                    tv1.setText(text);
                    String val = nm.getValue();
                    ed1.setText(val);

                    rel2.setVisibility(View.VISIBLE);
                    nm = valuePairs.get(2);
                    String text1 = nm.getName();
                    tv2.setText(text1);
                    String val1 = nm.getValue();
                    ed2.setText(val1);
                }
            } else {
                edPhone.setText(detail.getString("phone", ""));
            }*/
        }
    public Bitmap scaleBitmapDown(Bitmap bitmap, int maxDimension) {
        int resizedWidth = 0, resizedHeight = 0, originalWidth = 0, originalHeight = 0;

            originalWidth  = bitmap.getWidth();
            originalHeight = bitmap.getHeight();
            resizedWidth   = maxDimension;
            resizedHeight  = maxDimension;

            if (originalHeight > originalWidth) {
                resizedHeight = maxDimension;
                resizedWidth  = (int) (resizedHeight * (float) originalWidth / (float) originalHeight);
            } else if (originalWidth > originalHeight) {
                resizedWidth  = maxDimension;
                resizedHeight = (int) (resizedWidth * (float) originalHeight / (float) originalWidth);
            } else if (originalHeight == originalWidth) {
                resizedHeight = maxDimension;
                resizedWidth  = maxDimension;
            }
        return Bitmap.createScaledBitmap(bitmap, resizedWidth, resizedHeight, false);
    }
    public static ColorMatrixColorFilter brightIt(int fb) {
        ColorMatrix cmB = new ColorMatrix();
        cmB.set(new float[] {
                1, 0, 0, 0, fb,
                0, 1, 0, 0, fb,
                0, 0, 1, 0, fb,
                0, 0, 0, 1, 0   });

        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.set(cmB);
//Canvas c = new Canvas(b2);
//Paint paint = new Paint();
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(colorMatrix);
//paint.setColorFilter(f);
        return f;
    }
    //}
    }