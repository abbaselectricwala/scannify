package com.scanlibrary.utils;

import android.util.Patterns;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validate {

    String addressStr = "";
    String nameStr = "";
    String emailStr = "";
    String phoneStr = "";
    String websiteStr = "";
    String titleStr = "";
    String companyStr = "";

    static int nameFlag = 0;
    //static int phoneFlag = 0;
    static int webFlag = 0;
    static int desgFlag = 0;
    static int cmpFlag = 0;

    public String data;

    List<NameValuePair> params = new ArrayList<NameValuePair>();
    ArrayList<String> designationList = new ArrayList<>();

    public Validate(String text) {
        this.data = text;
        designationList.add("director");
        designationList.add("vice president");
        designationList.add("ceo");
        designationList.add("Staff");
        designationList.add("customer relation");
        designationList.add("service");
        designationList.add("executive");
        designationList.add("md");
        designationList.add("architect");
        designationList.add("advocate");
        designationList.add("builder");
        designationList.add("dealer");
        designationList.add("manager");
        designationList.add("team leader");
        designationList.add("chartered accountant");
        designationList.add("incharge");
        designationList.add("hr");
        designationList.add("admin");
        designationList.add("engineer");

        decode(data);
    }

    public boolean isEmailValid(String email) {
        boolean isValid = false;
       /* int flag = 0;
        if(email.contains(".com") || email.contains(".in") || email.contains(".org") || email.contains(".net"))
        {
            flag = 1;
        }*/

        //String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        //CharSequence inputStr = email;

        //Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        //Matcher matcher = pattern.matcher(inputStr);
        // if (matcher.matches()){
        if(email.contains(" "))
        {
            String [] splitEmail = email.split(" ");
            for(String emailAsset : splitEmail)
            {
                if(emailAsset.contains("@"))
                {
                    isValid=true;
                    break;
                }
                else
                {
                    isValid = false;
                }
            }
        }
        else {
            if(email.contains("@"))
            {
                isValid = true;
            }
            else
            {
                isValid = false;
            }
        }

        return isValid;
    }

    public static boolean isPhoneNumberValid(String check) {

        check = check.replaceAll("[() .^:,-]", "");
        System.out.println("String : " + check);
        String regex = "^((\\+|00)(\\d{1,3})[\\s-]?)?(\\d{8,})$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(check);
        if (m.matches())
            return true;
        return false;

    }

    public static boolean isNameValid(String name) {
        return name.matches("[A-Z][a-zA-Z]*" + "[a-zA-z]+[&]*" + "([ '-][&]*[a-zA-Z]+)*");
        //("[A-Z][a-zA-Z]*" + "[a-zA-z]+([ '-][a-zA-Z]+)*");
    }

    public static boolean isWebsiteValid(String url) {
        //return Patterns.WEB_URL.matcher(url).matches();
        boolean isValid = false;
        if(url.contains(" "))
        {
            String [] splitUrl = url.split(" ");
            for (String webAsset : splitUrl)
                {
                    if (webAsset.contains("www"))
                    {
                        if(webFlag == 0) {
                            isValid = true;
                            webFlag = 1;
                            break;
                        }
                    }
                    else
                    {
                        isValid = false;
                    }
                }
        }
        else {
            if (url.contains("www")) {
                if(webFlag == 0) {
                    isValid = true;
                    webFlag = 1;
                }
            } else {
                isValid = false;
            }
            if (Patterns.WEB_URL.matcher(url).matches())
            {
                if(webFlag == 0) {
                    isValid = true;
                    webFlag = 1;
                }
            }
        }
        return isValid;
    }

    public void decode(String data)
    {
        /*Split the string into multiple lines based on new line character*/
        String[] splits = data.split("\n");

        /*Traversing the lines*/
        for (String asset : splits) {

            //System.out.println(asset);

            /*If line contains blank chracter or space continue the loop*/
            if(asset.equals("") || asset.equals(" "))
            {
                continue;
            }

            /*If line contains colon (:) further split the line*/
            if (asset.contains(":"))
            {
                /*Key holds the key for multiple phone numbers*/
                String key;

                /*Further spliting the string based on colon (:)*/
                String[] colon = asset.split(":");

                /*Traversing the inner loop*/
                for (String newasset : colon)
                {
                    key = colon[0];
                    //System.out.println(newasset);

                    /*Validating the name and setting the name*/
                    if (isNameValid(newasset))
                    {
                        if(nameFlag == 0)
                        {
                            String [] n = newasset.split(" ");
                            if(n.length>1) {
                                setNameStr(newasset);
                                nameFlag = 1;
                            }
                        }
                        else
                        /* Set the value as designation*/
                        {
                            if(desgFlag == 0)
                            {
                                setTitleStr(newasset);
                                desgFlag = 1;
                            }
                        }
                    }

                    /*Validating the email and setting the email*/
                    if (isEmailValid(newasset))
                    {
                        setEmailStr(newasset);
                    }

                    /*Validating the website and setting the website*/
                    if(isWebsiteValid(newasset))
                    {
                        setWebsiteStr(newasset);
                    }

                    /*Validating the phone and setting the phone*/
                    if(isPhoneNumberValid(newasset))
                    {
                        setPhoneStr(key,newasset);
                    }


                    /*Validating the address and setting the address*/
                    if(newasset.contains(","))
                    {
                        /*Validating for phone number*/
                        if(newasset.contains("+"))
                        {
                            String[] commaSplit = asset.split(",");
                            String[] splitAsset = commaSplit[0].split("\\+");
                            if (isPhoneNumberValid(splitAsset[1]))
                            {
                                    setPhoneStr(key,splitAsset[1]);
                            }
                        }

                        /*Validating the address and setting the address*/
                        else {
                            setAddressStr(newasset); 
                        }
                    }
                }
            }

            /*Validating company name and setting the company name*/
            else if((asset.toLowerCase().contains("pvt")) || (asset.toLowerCase().contains("private")))
            {
                if(cmpFlag == 0) {
                    setCompanyName(asset);
                    cmpFlag = 1;
                }
            }

             /*Validating the address and setting the address*/
            else if(asset.contains(","))
            {
                /*Validating for phone number*/
                if(asset.contains("+"))
                {
                    String[] commaSplit = asset.split(",");
                    String[] splitAsset = commaSplit[0].split("\\+");
                    String[] splitAsset1 = commaSplit[1].split("\\+");
                    if (isPhoneNumberValid(splitAsset[1])) ;
                    {
                            setPhoneStr(splitAsset[0],splitAsset[1]);
                    }
                    if(isPhoneNumberValid(splitAsset1[1]))
                    {
                        setPhoneStr(splitAsset1[0],splitAsset1[1]);
                    }
                }

                /*Validating the address and setting the address*/
                else {
                    setAddressStr(asset);
                }
            }

            /*Validating the website and setting the website*/
            else
            {
                if (asset.contains("www"))
                {
                    if(webFlag == 0) {
                        int index = asset.indexOf("www");
                        setWebsiteStr(asset.substring(index));
                        webFlag = 1;
                    }
                }

                /*Validating the website and setting the website*/
                if(isWebsiteValid(asset))
                {
                    setWebsiteStr(asset);
                }

                /*Validating the name and setting the name*/
                if (isNameValid(asset)) {
                    if (nameFlag == 0) {
                        setNameStr(asset);
                        nameFlag = 1;
                    }
                    else
                    /* Set the value as designation*/
                    {
                        if(desgFlag == 0)
                        {
                            setTitleStr(asset);
                            desgFlag = 1;
                        }
                    }
                }

                /*Validating the email and setting the email*/
                if (asset.toLowerCase().contains("e-mail") || asset.toLowerCase().contains("email"))
                {
                    // id1 = asset.indexOf("mail")+1;
                    int id2 = asset.trim().indexOf(' ');
                    setEmailStr(asset.substring(id2));
                }else if(isEmailValid(asset))
                {
                    setEmailStr(asset);
                }

                /*Validating the email and setting the email*/
                if (isPhoneNumberValid(asset))
                {
                    setPhoneStr("",asset);
                }

                /*Validating the email and setting the email*/
                if(asset.contains("+")) {
                    if (asset.contains(",")) {
                        String[] commaSplit = asset.split(",");
                        String[] splitAsset = commaSplit[0].split("\\+");
                        if (isPhoneNumberValid(splitAsset[1])) ;
                        {
                                setPhoneStr(splitAsset[0],splitAsset[1]);
                        }
                    }
                    else
                    {
                        String[] splitAsset = asset.split("\\+");
                        if(splitAsset[1].contains(")")) {
                            int index = splitAsset[1].indexOf(")");

                            String str = splitAsset[1].substring(0,index)+ splitAsset[1].substring(index+1);//splitAsset[1].replace(")","\b");
                            if (isPhoneNumberValid(str)) ;
                            {
                                setPhoneStr(splitAsset[0],str);
                            }
                        }

                    }
                }
            }
        }

        if(cmpFlag ==0)
        {
//            setCompanyNameE(emailStr);
            cmpFlag = 1;
        }
        nameFlag = 0;
       // phoneFlag = 0;
        webFlag = 0;
        cmpFlag = 0;


    }



    public void setNameStr(String name) {
        String[] n = null;
        if (name.contains(" ")) {
            n = name.split(" ");
            if(n.length==2) {
                this.nameStr = this.nameStr + n[0] + " " + n[1];
            } else if (n.length==3){
                this.nameStr = this.nameStr + n[0] + " " + n[1] + " " + n[2];
            } else if (n.length==4) {
                this.nameStr = this.nameStr + n[0] + " " + n[1] + " " + n[2] + " " + n[3];
            }
        } else {
            this.nameStr = this.nameStr + name;
        }
    }

    public void setAddressStr(String address)
    {
        this.addressStr =  this.addressStr + address;
    }

    public void setWebsiteStr(String website)
    {
        this.websiteStr = this.websiteStr + website;
    }

    public void setEmailStr(String email)
    {
        this.emailStr = this.emailStr + email;
    }

    public void setPhoneStr(String lable ,String phone)
    {
        //this.phoneStr = this.phoneStr + phone;
        params.add(new BasicNameValuePair(lable,phone));
    }

    public void setTitleStr(String title)
    {
        this.titleStr =  this.titleStr + title;
    }

    public void setCompanyNameE(String email) {
        if (!email.equals("")) {
            int id1 = email.indexOf("@");
            int id2 = email.lastIndexOf(".");
//            this.companyStr = this.companyStr + email.substring(id1 + 1, id2);
        }
    }

    public void setCompanyName(String cname) {
            this.companyStr = this.companyStr + cname;
    }

    public String getNameStr()
    {
        return this.nameStr;
    }

    public String getAddressStr()
    {
        return this.addressStr;
    }

    public String getEmailStr()
    {
        return this.emailStr;
    }
    public List getPhoneStr()
    {
        return params;
    }

    public String getWebsiteStr()
    {
        return this.websiteStr;
    }

    public String getTitleStr()
    {
        return this.titleStr;
    }

    public String getCompanyStr()
    {
        return this.companyStr;
    }


}

